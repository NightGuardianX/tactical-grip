﻿using UnityEditor;
using UnityEngine;

#if UNITY_EDITOR
namespace Canvas.Editors
{
    [CustomEditor(typeof(ObjectBeingMovedAloneLine))]
    public class CarObjectEditor : Editor
    {
        #region variable
        protected ObjectBeingMovedAloneLine Target;
        private bool dirty;
        #endregion
        private void OnEnable()
        {
            Target = (ObjectBeingMovedAloneLine)target;
        }
        #region Inspector
        public override void OnInspectorGUI()
        {
            using (var check = new EditorGUI.ChangeCheckScope())
            {
                base.OnInspectorGUI();
                if (check.changed)
                {

                }
            }

            info();
            if (dirty)
            {
                dirty = false;
                UnityEditor.SceneManagement.EditorSceneManager.MarkAllScenesDirty();
            }
        }
        private void info()
        {
            GUILayout.BeginVertical("box");

            GUILayout.BeginHorizontal();
            EditorGUILayout.LabelField("Info", EditorStyles.boldLabel);
            GUILayout.EndHorizontal();

            GUILayout.BeginHorizontal();
            EditorGUILayout.LabelField("Path index: " + Target.CurrentIndex.ToString());
            GUILayout.EndHorizontal();

            GUILayout.BeginHorizontal();
            EditorGUILayout.LabelField("State: " + Target.CurrentIndex.ToString());
            GUILayout.EndHorizontal();

            GUILayout.BeginHorizontal();
            EditorGUILayout.LabelField("Bend angle: " + Target.BendAngle.ToString());
            GUILayout.EndHorizontal();

            GUILayout.BeginHorizontal();
            EditorGUILayout.LabelField("Speed: " + Target.Speed.ToString());
            GUILayout.EndHorizontal();

            GUILayout.EndVertical();
        }
        #endregion
    }
}
#endif