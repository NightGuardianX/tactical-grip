﻿namespace Canvas
{
    public enum CarObjectStates
    {
        Start, Draw, Ready, Moving, Finished
    }
}
