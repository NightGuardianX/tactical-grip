﻿using System.Collections.Generic;
using UnityEngine;

namespace Canvas
{
    public class ObjectBeingMovedAloneLine : MonoBehaviour
    {
        #region variable
        #region public
        [Header("Parameter(s)")]
        public float speed = 1;
        public float turnStrength = 1;
        public float height = 1;
        public float targetHitDistance = .001f;
        public bool obstacleSupport = true;
        [Header("Brush")]
        public bool showBrush = true;
        public GameObject brush;
        public float brushSize = .01f;
        public float brushExplanationDistance = .1f;
        public float brushHeight = .1f;
        [Header("Line")]
        public bool showLine = true;
        public float lineWidth = .1f;
        public float lineHeight = .01f;
        public Material material;
        [Header("SteerAngleController")]
        public bool enableSpeedController = true;
        public float steerAngleThreshold = 5;
        public float limitedSpeed = .1f;
        [Header("Extra")]
        public bool showHelpeLines = true;
        [SerializeField]
        private CarObjectStates _currentState = CarObjectStates.Start;
        private float _bendAngle;
        private int _currentIndex = 0;
        #endregion
        #region private
        [SerializeField]
        private Camera _camera;
        [SerializeField]
        private WrapperForObjectBeingMovedLineAlone _wrapper;
        private LineRenderer helperLine;
        [SerializeField]
        private List<PathNode> nodes = new List<PathNode>();
        [SerializeField]
        private List<GameObject> lines = new List<GameObject>();

        private bool isMoving = false;
        private Vector3 targetPosition = Vector3.zero;

        [HideInInspector] public Collider _collider;
        private Ground canvas;
        private bool lockCanvas = false;
        private float deltaTime;
        private CarObjectSituation startSituatuin;
        #region speed
        private Vector3 lastPosition;
        private float deltaSpeed;
        #endregion
        private Vector3[] lastSkidPoint = new Vector3[] { Vector3.zero, Vector3.zero };
        #endregion
        #endregion
        #region Functions
        private void OnValidate()
        {
            _collider = GetComponent<Collider>();
        }
       
        private void OnDrawGizmos()
        {
            if (!showHelpeLines) return;
            if (nodes.Count > 1)
            {
                var p1 = nodes[nodes.Count - 1].Position;
                var p2 = nodes[nodes.Count - 2].Position;
                Gizmos.color = Color.red;
                Gizmos.DrawLine(p2, p1);
            }
        }
        void Update()
        {
            calculateSpeedVector();
            if (Input.GetMouseButton(0))
            {
                switch (_currentState)
                {
                    case CarObjectStates.Draw:
                        draw();
                        break;
                    case CarObjectStates.Start:
                        if (isRayHit())
                            _currentState = CarObjectStates.Draw;
                        break;
                    default:
                        break;
                }
            }
            else
            {
                if (lockCanvas)
                {
                    if (canvas.isLock)
                    {
                        lockCanvas = canvas.isLock = false;
                        helperLine.enabled = false;
                    }
                }
                if (_currentState == CarObjectStates.Draw && nodes.Count > 35)
                {
                    _currentState = CarObjectStates.Ready;
                    _wrapper.ReadyToMove();
                    return;
                }
                else if(_currentState == CarObjectStates.Draw && nodes.Count < 35)
                {
                    Clear();
                }
            }
        }
        private void LateUpdate()
        {
            if (_currentState == CarObjectStates.Moving)
            {
                Moving();
                move(targetPosition, targetHitDistance);
            }
        }
        private void OnDestroy()
        {
            if (helperLine != null)
            {
                Object.Destroy(helperLine.gameObject);
            }
            Clear();
        }
        #endregion
        #region functions
        #region geter and setter
        public float Speed { get; private set; }
        public CarObjectStates CurrentState { get => _currentState; set => _currentState = value; }
        public int CurrentIndex { get => _currentIndex; set => _currentIndex = value; }
        public float BendAngle { get => _bendAngle; set => _bendAngle = value; }

        public void setCanvas(Ground node)
        {
            canvas = node;
        }
        public void setDelteTime(float val)
        {
            deltaTime = val;
        }
        #endregion
        #region draw
        private void draw()
        {
            Ray ray = _camera.ScreenPointToRay(Input.mousePosition);
            RaycastHit hit;
            if (nodes.Count < 2)
            {
                if (isRayHit(ray, out hit))
                {
                    if (!lockCanvas)
                    {
                        if (canvas.isLock) return;
                        lockCanvas = canvas.isLock = true;
                    }
                }
                else return;
            }
            updateHelperLine();
            if (obstacleSupport)
            {
                if (!isRayHitInObstacleSupport(ray, out hit, Mathf.Infinity)) return;
            }
            if (!canvas.isRayHit(ray, out hit)) return;
            if (!hitPointIsValid(hit.point)) return;

            PathNode newNode;
            if (showBrush)
            {
                var node = Instantiate(brush, hit.point, transform.rotation);
                node.SetActive(showBrush);
                node.isStatic = true;
                node.name = nodes.Count.ToString();
                node.transform.localScale = new Vector3(brushSize, brushSize, brushSize);
                node.hideFlags = HideFlags.HideAndDontSave;
                newNode = new PathNode(ref node);
                if (nodes.Count > 0)
                {
                    Vector3 angle = node.transform.position - nodes[nodes.Count - 1].Position;
                    Quaternion rotation = Quaternion.LookRotation(angle, Vector3.up);
                    node.transform.rotation = rotation;
                }
            }
            else
            {
                newNode = new PathNode(hit.point);
            }
            if (showLine && nodes.Count > 0) drawSkid(nodes[nodes.Count - 1], newNode);
            nodes.Add(newNode);
        }
        private bool hitPointIsValid(Vector3 point)
        {
            if (nodes.Count < 1) return true;

            bool result = true;

            Vector3 start = nodes[nodes.Count - 1].Position;
            float dist = Vector3.Distance(point, start);
            if (dist < brushExplanationDistance) return false;

            Vector3 direction = (point - start).normalized;

            IDictionary<Vector3, bool> candidates = new Dictionary<Vector3, bool>();
            Vector3 candidate = start;
            while (true)
            {
                candidate += (direction * brushExplanationDistance);
                if (Vector3.Distance(candidate, start) > dist) break;
                bool hitting = candidatePointIsValid(candidate);
                if (!hitting) result = false;
                if (!showHelpeLines && !result) break;
                if (showHelpeLines) candidates.Add(candidate, hitting);
            }
            //debug lines
            if (showHelpeLines)
            {
                var cameraPosition = _camera.transform.position;
                foreach (KeyValuePair<Vector3, bool> kvp in candidates)
                {
                    Color c = kvp.Value ? Color.green : Color.red;
                    Debug.DrawLine(cameraPosition, kvp.Key, c);
                }
            }
            return result;
        }
        private bool candidatePointIsValid(Vector3 candidate)
        {
            RaycastHit hit;
            Vector3 vector = candidate - _camera.transform.position;
            Ray ray = new Ray(_camera.transform.position, vector.normalized);
            if (obstacleSupport)
            {
                if (!isRayHitInObstacleSupport(ray, out hit, vector.magnitude * 2)) return false;
            }
            return canvas.isRayHit(ray, out hit);
        }
        #region skid pattern
        private void drawSkid(PathNode start, PathNode end)
        {
            if (start == null || end == null) return;
            drawSkid(start.Position, end.Position);
        }
        private void drawSkid(Vector3 start, Vector3 end)
        {
            Vector3 p1 = start.IncreaseY(lineHeight);
            Vector3 p2 = end.IncreaseY(lineHeight);

            GameObject newLine = new GameObject("Line");
            newLine.hideFlags = HideFlags.HideAndDontSave;
            newLine.isStatic = true;
            var meshRender = newLine.AddComponent<MeshRenderer>();
            meshRender.sharedMaterial = material;
            meshRender.shadowCastingMode =  UnityEngine.Rendering.ShadowCastingMode.Off;
            meshRender.receiveShadows = false;
            MeshFilter meshFilter = newLine.AddComponent<MeshFilter>();
            meshFilter.sharedMesh = new Mesh();
            meshFilter.GetComponent<MeshRenderer>().sharedMaterial = material;

            //lineWidth
            Vector3 vector = p2 - p1;
            Vector3 dir = Vector3.Cross(vector, Vector3.up).normalized;

            float halfLineWidth = (lineWidth / 2);
            Vector3 a = p1 + dir * halfLineWidth;
            Vector3 b = p1 - dir * halfLineWidth;
            Vector3 c = p2 + dir * halfLineWidth;
            Vector3 d = p2 - dir * halfLineWidth;
            //
            if (lastSkidPoint[0] != Vector3.zero)
            {
                drawSoftenSkid(vector.normalized, new Vector3[] { lastSkidPoint[0], lastSkidPoint[1], a, b });
            }

            lastSkidPoint[0] = c;
            lastSkidPoint[1] = d;
            //
            Mesh mesh = meshFilter.sharedMesh;
            mesh.Clear();
            mesh.vertices = new Vector3[] { a, b, c, d };

            Vector2[] uvs = new Vector2[mesh.vertices.Length];
            for (int i = 0; i < uvs.Length; i++)
            {
                uvs[i] = mesh.vertices[i].ToXZ();
            }
            mesh.uv = uvs;

            mesh.triangles = new int[] {
                0, 2, 1,
                2, 3, 1
            };
            mesh.RecalculateNormals();
            lines.Add(newLine);
        }
        private void drawSoftenSkid(Vector3 direction, Vector3[] vertices)
        {
            GameObject newLine = new GameObject("SoftenLine");
            newLine.isStatic = true;
            newLine.hideFlags = HideFlags.HideAndDontSave;
            var meshRender = newLine.AddComponent<MeshRenderer>();
            meshRender.sharedMaterial = material;
            meshRender.shadowCastingMode = UnityEngine.Rendering.ShadowCastingMode.Off;
            meshRender.receiveShadows = false;
            MeshFilter meshFilter = newLine.AddComponent<MeshFilter>();
            meshFilter.sharedMesh = new Mesh();
            meshFilter.GetComponent<MeshRenderer>().sharedMaterial = material;
            //
            Mesh mesh = meshFilter.sharedMesh;
            mesh.Clear();
            mesh.vertices = vertices;

            Vector2[] uvs = new Vector2[mesh.vertices.Length];
            for (int i = 0; i < uvs.Length; i++)
            {
                uvs[i] = new Vector2(mesh.vertices[i].x, mesh.vertices[i].z);
            }
            mesh.uv = uvs;

            List<int> triangles = new List<int>();
            Vector3 dir02 = (vertices[2] - vertices[0]).normalized;

            if (Vector3.Dot(dir02, direction) > 0)
            {
                triangles.Add(0);
                triangles.Add(2);
                triangles.Add(1);
            }
            Vector3 dir13 = (vertices[3] - vertices[1]).normalized;
            if (Vector3.Dot(dir13, direction) > 0)
            {
                triangles.Add(3);
                triangles.Add(1);
                triangles.Add(2);
            }

            mesh.triangles = triangles.ToArray();

            mesh.RecalculateNormals();
            lines.Add(newLine);
        }
        #endregion
        private void updateHelperLine()
        {
            Vector3 gizmoPosition;
            if (!canvas.getGizmoPosition(out gizmoPosition) || nodes.Count < 1)
            {
                helperLine.enabled = false;
                return;
            }

            helperLine.enabled = true;
            helperLine.startColor = helperLine.endColor = Color.black;
            helperLine.startWidth = helperLine.endWidth = lineWidth;
            helperLine.positionCount = 2;
            helperLine.useWorldSpace = true;
            helperLine.material = material;

            helperLine.SetPosition(0, nodes[nodes.Count - 1].Position);
            helperLine.SetPosition(1, gizmoPosition);
        }
        public void Clear()
        {
            if (nodes.Count > 0)
            {
                var t = this.transform;
                startSituatuin.setValueFor(ref t);
            }
            foreach (var item in nodes)
            {
                item.destroy();
            }
            foreach (var item in lines)
            {
                Destroy(item);
            }
            nodes.Clear();
            lines.Clear();
        }
        #endregion
        #region Movement
        private void Moving()
        {
            if (isMoving || nodes.Count < 1) return;
            if (_currentIndex == 0)
            {
                setPosition(nodes[0].Position.IncreaseY(height));
            }
            if (_currentIndex < nodes.Count)
            {
                if (targetPosition == Vector3.zero)
                {
                    calculateBendAngle(_currentIndex);
                    targetPosition = nodes[_currentIndex].Position.IncreaseY(height);
                    _currentIndex++;
                }
            }
            else
            {
                _currentIndex = 0;
                _currentState = CarObjectStates.Finished;
                _wrapper.Finished();

            }
        }
        private void move(Vector3 target, float targetRadios)
        {
            if (target == Vector3.zero) return;

            isMoving = true;
            Vector3 relativePos = target - transform.position;
            float movementSpeed = getSpeed();
            if (relativePos != Vector3.zero)
            {
                Quaternion rotation = Quaternion.LookRotation(relativePos, Vector3.up);
                transform.rotation = Quaternion.Lerp(transform.rotation, rotation, movementSpeed * turnStrength);
            }

            setPosition(Vector3.MoveTowards(transform.position, target, movementSpeed));

            if (relativePos.magnitude < targetRadios)
            {
                targetPosition = Vector3.zero;
                isMoving = false;
            }
        }
        private void calculateBendAngle(int index)
        {
            if (index == nodes.Count - 1)
            {
                _bendAngle = 0;
            }
            else
            {
                Vector3 v1 = nodes[index].Position - transform.position;
                Vector3 v2 = nodes[index + 1].Position - nodes[index].Position;
                _bendAngle = Vector3.Angle(v1, v2);
            }
        }
        private void setPosition(Vector3 pos)
        {
            transform.position = pos;
        }
        public void FixeCurrentPosition()
        {
            startSituatuin = new CarObjectSituation(this.transform);
        }
        #endregion
        #region speed
        private float getSpeed()
        {
            if (enableSpeedController)
            {
                if (_bendAngle < steerAngleThreshold)
                {
                    deltaSpeed += Time.deltaTime;
                }
                else
                {
                    deltaSpeed -= Time.deltaTime;
                }
                if (deltaSpeed < limitedSpeed) deltaSpeed = limitedSpeed;
                if (deltaSpeed >= speed) deltaSpeed = speed;
                return deltaTime * deltaSpeed;
            }
            return deltaTime * speed;
        }
        private void calculateSpeedVector()
        {
            float a = deltaTime == 0 ? 1 : deltaTime;
            Speed = Vector3.Distance(lastPosition, transform.position) / a;
            lastPosition = transform.position;
        }
        #endregion
        #region ray
        public bool isRayHit()
        {
            RaycastHit hit;
            return isRayHit(_camera.ScreenPointToRay(Input.mousePosition), out hit);
        }
        public bool isRayHit(Ray ray, out RaycastHit hit)
        {
            float size = Vector3.Distance(ray.origin, this.transform.position) + _collider.bounds.size.magnitude;
            return _collider.Raycast(ray, out hit, size);
        }
        public bool isRayHitInObstacleSupport(Ray ray, out RaycastHit hit, float maxDistance)
        {
            if (Physics.Raycast(ray, out hit, maxDistance))
            {
                var hitGo = hit.collider.gameObject;
                return (hitGo == canvas.gameObject || hitGo == this.gameObject);
            }
            return false;
        }
        #endregion
        public void Init(WrapperForObjectBeingMovedLineAlone wrapperForObjectBeingMovedLineAlone)
        {
            _wrapper = wrapperForObjectBeingMovedLineAlone;
            FixeCurrentPosition();
            if (brush == null)
            {
                brush = new GameObject("brush");
                brush.hideFlags = HideFlags.HideAndDontSave;
            }
            var line = new GameObject("Line");
            //line.hideFlags = HideFlags.HideAndDontSave;
            helperLine = line.AddComponent<LineRenderer>();
            helperLine.enabled = false;
        }
        #endregion
    }
}
