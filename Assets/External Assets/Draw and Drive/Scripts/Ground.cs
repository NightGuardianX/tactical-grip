﻿using System.Collections.Generic;
using UnityEngine;
using SceneManagement.MyCamera;

namespace Canvas
{
    public class Ground : MonoBehaviour
    {
        #region Variables
        #region Public
        [Header("Node(s)")]
        public List<ObjectBeingMovedAloneLine> carObjects;
        public bool syncMode = false;
        [Header("Settings")]
        public bool gizmoEnable = true;
        public GameObject gizmo;
        [System.NonSerialized, HideInInspector]
        public bool isLock = false;
        #endregion
        #region private
        private float deltaTime;
        private CameraOrbit cameraOrbit;
        private bool lastCameraOrbitEnable;
        [HideInInspector]public Collider _collider;
        #endregion
        #endregion
        #region Functions
        private void OnValidate()
        {
            _collider = GetComponent<Collider>();
        }
        private void Start()
        {
            init();
            if (!gizmoEnable || gizmo == null)
            {
                gizmo = new GameObject();
            }
            foreach (ObjectBeingMovedAloneLine node in carObjects)
            {
                if (node == null) continue;
                node.setDelteTime(deltaTime);
                node.setCanvas(this);
            }
        }
        private void Update()
        {
            //updateCarState();
            updateMousePosition();
            setCameraOrbit_enable(!isLock);
        }
        #endregion
        #region functions
        #region getter and setter
        public LayerMask LayerMask
        {
            get { return gameObject.layer; }
        }
        #endregion
        //private void updateCarState()
        //{
        //    if (syncMode)
        //    {
        //        changeStateInSyncMode();
        //    }
        //    else
        //    {
        //        changeStateInAsyncMode();
        //    }
        //}
        //private void changeStateInAsyncMode()
        //{
        //    foreach (ObjectBeingMovedAloneLine node in carObjects)
        //    {
        //        if (node == null) continue;
        //        if (node.CurrentState == CarObjectStates.Ready)
        //        {
        //            node.CurrentState = CarObjectStates.Moving;
        //        }
        //    }
        //}
        //private void changeStateInSyncMode()
        //{
        //    foreach (ObjectBeingMovedAloneLine node in carObjects)
        //    {
        //        if (node == null) continue;
        //        if (node.CurrentState != CarObjectStates.Ready)
        //        {
        //            return;
        //        }
        //    }
        //    changeStateInAsyncMode();
        //}
        private void updateMousePosition()
        {
            Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
            RaycastHit hit;
            if (Physics.Raycast(ray, out hit, Mathf.Infinity))
            {
                if (gizmoEnable)
                    gizmo.SetActive(true);
                gizmo.transform.position = hit.point;
            }
            else
            {
                if (gizmoEnable)
                    gizmo.SetActive(false);
            }
        }
        public bool getGizmoPosition(out Vector3 result)
        {
            if (gizmo.activeInHierarchy) { result = gizmo.transform.position; return true; }
            result = default;
            return false;
        }

        private void init()
        {
            if (carObjects == null || carObjects.Count < 1)
            {
                carObjects = new List<ObjectBeingMovedAloneLine>();
                var list = FindObjectsOfType<ObjectBeingMovedAloneLine>();
                if (list != null)
                {
                    for (int i = 0; i < list.Length; i++)
                    {
                        if (list[i] == null) continue;
                        carObjects.Add(list[i]);
                    }
                }
            }
            if (carObjects.Count < 1)
            {
                Debug.LogError("No car object...");
                Extensions.Quit();
            }
            deltaTime = Time.fixedDeltaTime;
            if (gizmo != null)
            {
                var c = gizmo.GetComponent<Collider>();
                if (c != null) UnityEngine.Object.Destroy(c);
            }
            init_cameraOrbit();
        }
        #region rays
        public bool isRayHit(Ray ray, out RaycastHit hit)
        {
            float size = Vector3.Distance(ray.origin, _collider.transform.position) + _collider.bounds.size.magnitude;
            return _collider.Raycast(ray, out hit, size);
        }
        #endregion
        #endregion
        #region cameraOrbit
        private void init_cameraOrbit()
        {
            if ((cameraOrbit = FindObjectOfType<CameraOrbit>()) != null)
            {
                lastCameraOrbitEnable = isLock;
            }
        }
        public void setCameraOrbit_enable(bool enable)
        {
            if (cameraOrbit == null || lastCameraOrbitEnable == enable) return;
            lastCameraOrbitEnable = cameraOrbit.Enable = enable;
        }
        #endregion
    }
}