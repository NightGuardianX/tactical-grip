﻿using UnityEngine;

namespace Canvas
{
    [System.Serializable]
    public class PathNode
    {
        [SerializeField]
        private Vector3 position = Vector3.zero;
        private GameObject gameObject;

        public PathNode(Vector3 position)
        {
            this.position = position;
        }
        public PathNode(ref GameObject go)
        {
            if ((gameObject = go) != null)
                position = gameObject.transform.position;
        }
        public Vector3 Position
        {
            get { return position; }
        }
        public void destroy()
        {
            if (gameObject == null) return;
            Object.Destroy(gameObject);
        }
        ~PathNode()
        {
            destroy();
        }
    }
}