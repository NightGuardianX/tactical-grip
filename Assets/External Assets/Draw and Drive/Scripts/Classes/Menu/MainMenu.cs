﻿using UnityEngine;
using UnityEditor;
using Canvas;
using System.Collections.Generic;

#if UNITY_EDITOR
public class MainMenu : MonoBehaviour
{
    #region Canvac
    [MenuItem(Globals.PROJECT_NAME +  "/Ground/(Add) and Select")]
    public static void AddGridManager()
    {
        var item = FindObjectOfType<Ground>();
        if (item != null)
        {
            if (item.gameObject.GetComponent<Ground>() == null)
            {
                Undo.AddComponent<Ground>(item.gameObject);
            }
            Selection.objects = new Object[] { item.gameObject };
            return;
        }
        GameObject groundManager = new GameObject("Grid Manager");
        groundManager.transform.SetPositionAndRotation(Vector3.zero, Quaternion.identity);
        groundManager.AddComponent<Ground>();

        Undo.RegisterCreatedObjectUndo(groundManager, "Created Grid Manager");

        Selection.objects = new Object[] { groundManager };
    }
    #endregion
    #region Car object
    [MenuItem(Globals.PROJECT_NAME + "/Car object/Create")]
    public static void AddCarObject()
    {
        AddComponentToObject<ObjectBeingMovedAloneLine>("Car");
    }
    [MenuItem(Globals.PROJECT_NAME + "/Car object/Attach script")]
    public static void AttachCarObjectScript()
    {
        AttachComponentToSelection<ObjectBeingMovedAloneLine>();
    }
    [MenuItem(Globals.PROJECT_NAME + "/Car object/Remove script")]
    public static void RemoveCarObjectScript()
    {
        RemoveComponentFromSelection<ObjectBeingMovedAloneLine>();
    }
    [MenuItem(Globals.PROJECT_NAME + "/Car object/Select All")]
    public static void SelectAllCarObjects()
    {
        SelectAllObjectsByComponent<ObjectBeingMovedAloneLine>();
    }
    #endregion
    #region About us
    [MenuItem(Globals.PROJECT_NAME + "/Publisher Page")]
    public static void PublisherPage()
    {
        Application.OpenURL("https://assetstore.unity.com/publishers/48757");
    }
    #endregion
    #region Component
    public static void AddComponentToObject<T>(string name) where T : Component
    {
        GameObject node = GameObject.CreatePrimitive(PrimitiveType.Cube);
        node.name = name;
        node.transform.SetPositionAndRotation(getPosition(), Quaternion.identity);
        node.AddComponent<T>();
        Undo.RegisterCreatedObjectUndo(node, "Create object");
        Selection.objects = new Object[] { node };
    }
    public static void AttachComponentToSelection<T>() where T : Component
    {
        if (Selection.objects == null) return;
        for (int i = 0; i < Selection.objects.Length; i++)
        {
            var node = (GameObject)Selection.objects[i];
            if (node == null || node.GetComponent<T>() != null) continue;
            Undo.AddComponent<T>(node);
        }
    }
    public static void RemoveComponentFromSelection<T>() where T : Component
    {
        if (Selection.objects == null) return;
        T function;
        for (int i = 0; i < Selection.objects.Length; i++)
        {
            var node = (GameObject)Selection.objects[i];
            if (node == null || (function = node.GetComponent<T>()) == null) continue;
            Undo.DestroyObjectImmediate(function);
            //Object.DestroyImmediate(function);
        }
    }
    public static void SelectAllObjectsByComponent<T>() where T : Component
    {
        var nodes = FindObjectsOfType<T>();
        List<Object> result = new List<Object>();
        if (nodes != null)
        {
            foreach (var node in nodes)
            {
                if (node == null) continue;
                result.Add(node.gameObject);
            }
        }

        Selection.objects = result.ToArray();
    }
    #endregion
    private static Vector3 getPosition()
    {
        var item = FindObjectOfType<Ground>();
        if (item == null) return Vector3.zero;
        return item.transform.position;
    }
}
#endif
