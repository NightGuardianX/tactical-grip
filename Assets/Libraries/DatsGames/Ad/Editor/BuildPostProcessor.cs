// filename BuildPostProcessor.cs
// put it in a folder Assets/Editor/

#if UNITY_IOS
using System.IO;
using UnityEngine;
using UnityEditor;
using UnityEditor.Callbacks;
using UnityEditor.iOS.Xcode;

#if UNITY_2019_3_OR_NEWER
using UnityEditor.iOS.Xcode.Extensions;
#endif
using UnityEngine.Networking;
using System;
using System.Linq;
using System.Collections.Generic;

public class BuildPostProcessor
{
	private static string[] skAdNetworkIds = new string[] {
		"v9wttpbfk9.skadnetwork", // Facebook.
		"n38lu8286q.skadnetwork", // Facebook.
		"4PFYVQ9L8R.skadnetwork", // AdColony
		"cstr6suwn9.skadnetwork", // AdMob
		"ludvb6z3bs.skadnetwork", // AppLovin
		"F38H382JLK.skadnetwork", // Chartboost
		"V4NXQHLYQP.skadnetwork", // Maio
		"22mmun2rn5.skadnetwork", // Pangle (Non CN)
		"238da6jt44.skadnetwork", // Pangle (CN)
		"4DZT52R2T5.skadnetwork", // Unity Ads
		"GTA9LK7P23.skadnetwork"  // Vungle
	};

	[PostProcessBuild]
	public static void ChangeXcodePlist(BuildTarget buildTarget, string path)
	{
		if (buildTarget == BuildTarget.iOS)
		{
			string plistPath = path + "/Info.plist";
			PlistDocument plist = new PlistDocument();
			plist.ReadFromFile(plistPath);

			PlistElementDict rootDict = plist.root;

			Debug.Log(">> Automation, plist ... <<");

			rootDict.SetString("GADApplicationIdentifier", "ca-app-pub-9334727648055753~8236909054");
			rootDict.SetString("NSLocationWhenInUseUsageDescription", "Requires for the actual ADS for you.");
			rootDict.SetString("NSCalendarsUsageDescription", "Requires to add some events for you.");

			// IronSource identifier.
			// ----------------------
			// rootDict.SetString("SKAdNetworkIdentifier", "SU67R6K2V3.skadnetwork");

			// Network items.
			// --------------
            // Check if we have a valid list of SKAdNetworkIds that need to be added.
            if (skAdNetworkIds != null && skAdNetworkIds.Length > 0)
			{
				// @info Add the SKAdNetworkItems to the plist. It should look like following:
				//
				//    <key>SKAdNetworkItems</key>
				//    <array>
				//        <dict>
				//            <key>SKAdNetworkIdentifier</key>
				//            <string>ABC123XYZ.skadnetwork</string>
				//        </dict>
				//        <dict>
				//            <key>SKAdNetworkIdentifier</key>
				//            <string>123QWE456.skadnetwork</string>
				//        </dict>
				//        <dict>
				//            <key>SKAdNetworkIdentifier</key>
				//            <string>987XYZ123.skadnetwork</string>
				//        </dict>
				//    </array>
				//
				PlistElement skAdNetworkItems;
				plist.root.values.TryGetValue("SKAdNetworkItems", out skAdNetworkItems);
				var existingSkAdNetworkIds = new HashSet<string>();
				// Check if SKAdNetworkItems array is already in the Plist document and collect all the IDs that are already present.
				if (skAdNetworkItems != null && skAdNetworkItems.GetType() == typeof(PlistElementArray))
				{
					var plistElementDictionaries = skAdNetworkItems.AsArray().values.Where(plistElement => plistElement.GetType() == typeof(PlistElementDict));
					foreach (var plistElement in plistElementDictionaries)
					{
						PlistElement existingId;
						plistElement.AsDict().values.TryGetValue("SKAdNetworkIdentifier", out existingId);
						if (existingId == null || existingId.GetType() != typeof(PlistElementString) || string.IsNullOrEmpty(existingId.AsString())) continue;

						existingSkAdNetworkIds.Add(existingId.AsString());
					}
				}
				// Else, create an array of SKAdNetworkItems into which we will add our IDs.
				else
				{
					skAdNetworkItems = plist.root.CreateArray("SKAdNetworkItems");
				}

				foreach (var skAdNetworkId in skAdNetworkIds)
				{
					// Skip adding IDs that are already in the array.
					if (existingSkAdNetworkIds.Contains(skAdNetworkId)) continue;

					var skAdNetworkItemDict = skAdNetworkItems.AsArray().AddDict();
					skAdNetworkItemDict.SetString("SKAdNetworkIdentifier", skAdNetworkId);
				}
			}

			// example of changing a value:
			// rootDict.SetString("CFBundleVersion", "6.6.6");

			// example of adding a boolean key...
			// < key > ITSAppUsesNonExemptEncryption </ key > < false />
			rootDict.SetBoolean("ITSAppUsesNonExemptEncryption", false);

			File.WriteAllText(plistPath, plist.WriteToString());
		}
	}

	[PostProcessBuildAttribute(1)]
	public static void OnPostProcessBuild(BuildTarget target, string path)
	{
		if (target == BuildTarget.iOS) {

			PBXProject project = new PBXProject();
			string sPath = PBXProject.GetPBXProjectPath(path);
			project.ReadFromFile(sPath);

#if UNITY_2019_3_OR_NEWER
      		var targetGuid = project.GetUnityFrameworkTargetGuid();
#else
      		var targetGuid = project.TargetGuidByName(PBXProject.GetUnityTargetName());
#endif
			string g = project.TargetGuidByName(targetGuid);

			// todo: Uncomment this if required frameworks settings setup.
			// ModifyFrameworksSettings(project, g);

			// modify frameworks and settings as desired
			File.WriteAllText(sPath, project.WriteToString());
		}
	}

	private static void ModifyFrameworksSettings(PBXProject project, string g)
	{
		// add hella frameworks

		Debug.Log(">> Automation, Frameworks... <<");

		project.AddFrameworkToProject(g, "blah.framework", false);
		project.AddFrameworkToProject(g, "libz.tbd", false);

		// go insane with build settings

		Debug.Log(">> Automation, Settings... <<");

		project.AddBuildProperty(g,
			"LIBRARY_SEARCH_PATHS",
			"../blahblah/lib");

		project.AddBuildProperty(g,
			"OTHER_LDFLAGS",
			"-lsblah -lbz2");

		// note that, due to some Apple shoddyness, you usually need to turn this off
		// to allow the project to ARCHIVE correctly (ie, when sending to testflight):
		project.AddBuildProperty(g,
			"ENABLE_BITCODE",
			"false");
	}

}
#endif