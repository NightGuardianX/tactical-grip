namespace DatsGames
{
	using UnityEngine;
	using UnityEditor;

	[CustomEditor(typeof(AdWrapper))]
	public class AdWrapperEditor : Editor
	{
		private AdWrapper _self;

		#region Unity Calls

		private void OnEnable()
		{
			_self = (AdWrapper) target;
			_self.enableIronSource = DefineHelper.IsExists("IRONSOURCE");
		}

		public override void OnInspectorGUI()
		{
			base.OnInspectorGUI();
			
			EditorGUI.BeginChangeCheck();

			EditorGUI.indentLevel++;
			EditorGUILayout.Space();
			
			GUILayout.BeginHorizontal();
			
			GUILayout.Label("Ad Wrapper", EditorStyles.largeLabel);
			
			// if (GUILayout.Button("Help", GUILayout.MaxWidth(60)))
			// {
			// 	Application.OpenURL("https://gitlab.com/game-delta/wikiguidelines/-/wikis/");
			// }
			
			GUILayout.EndHorizontal();
			
			GUILayout.BeginHorizontal();
			GUILayout.Label("Сomponent for initializing Max SDK.", EditorStyles.miniLabel);
			GUILayout.EndHorizontal();
			EditorGUI.indentLevel--;

			EditorGUILayout.Space();

			bool prevIronSource = _self.enableIronSource;
			EditorGUI.indentLevel++;
			_self.enableIronSource = EditorGUILayout.Toggle("Enable", _self.enableIronSource);
			EditorGUI.indentLevel--;
			if (!_self.enableIronSource)
			{
				EditorGUILayout.HelpBox("AdWrapper is disabled!", MessageType.Warning);
			}
			else
			{
				_self.debugLog = GUILayout.Toggle(_self.debugLog, "Debug Log");
				if (_self.debugLog)
				{
					EditorGUILayout.HelpBox("Don't forget disable the DebugLog option in the production version!", MessageType.Warning);
				}

				_self.debugMode = GUILayout.Toggle(_self.debugMode, "Debug Mode");
				if (_self.debugMode)
				{
					EditorGUILayout.HelpBox("Don't forget disable the Debug Mode option in the production version!", MessageType.Warning);
				}

				GUILayout.Space(6.0f);
				_self.trackStats = GUILayout.Toggle(_self.trackStats, "Track Stats");
				if (_self.trackStats)
				{
					EditorGUILayout.HelpBox("When Track Stats is enabled, AdsWrapper will send default stats to the StatsWrapper.", MessageType.Info);
				}

				GUILayout.Space(6.0f);
				_self.autoRequest = GUILayout.Toggle(_self.autoRequest, "Auto Request");
				if (_self.autoRequest)
				{
					EditorGUILayout.HelpBox("When Auto Request is enabled, AdWrapper will send request for load all AD types automatically.", MessageType.Info);
				}

				GUILayout.Space(10.0f);
				EditorGUILayout.LabelField("IronSource Keys", EditorStyles.boldLabel);
				EditorGUILayout.BeginVertical(EditorStyles.helpBox);
				{
					EditorGUI.indentLevel++;
					_self.iOsKey = EditorGUILayout.TextField("iOS", _self.iOsKey);
					_self.androidKey = EditorGUILayout.TextField("Android", _self.androidKey);
					GUILayout.Space(10.0f);
					_self.debugIosKey = EditorGUILayout.TextField("Debug iOS", _self.debugIosKey);
					_self.debugAndroidKey = EditorGUILayout.TextField("Debug Android", _self.debugAndroidKey);
					EditorGUI.indentLevel--;
					GUILayout.Space(10.0f);
				}
				EditorGUILayout.EndVertical();

				EditorGUILayout.BeginVertical(EditorStyles.helpBox);
				_self.isUseDelayBetweenInterstitials = EditorGUILayout.BeginToggleGroup(
					"Enable delay between interstitials",
					_self.isUseDelayBetweenInterstitials
				);
				EditorGUILayout.EndToggleGroup();

				if (_self.isUseDelayBetweenInterstitials)
				{
					_self.delayBetweenInterstitials = EditorGUILayout.FloatField("Interval (sec)", _self.delayBetweenInterstitials);
				}
				EditorGUILayout.EndVertical();
			}

			if (EditorGUI.EndChangeCheck())
			{
				if (_self.enableIronSource != prevIronSource)
				{
					DefineHelper.SetEnabled("IRONSOURCE", _self.enableIronSource, true);
				}

				EditorUtility.SetDirty(target);
			}
		}

		#endregion
	}
}
