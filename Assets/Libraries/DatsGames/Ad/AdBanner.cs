namespace DatsGames
{
	using UnityEngine;
	using System.Collections;
	using System;

	public class AdBanner
	{
		public delegate void BannerDelegate();

		/// <summary>
		/// Вызывается когда баннер развернут.
		/// </summary>
		public event BannerDelegate EventPresented;

		/// <summary>
		/// Вызывается когда баннер свернут.
		/// </summary>
		public event BannerDelegate EventDismissed;

		/// <summary>
		/// Вызывается когда баннер загружен.
		/// </summary>
#if IRONSOURCE
		public event BannerDelegate EventLoaded;
#endif

		#region Private Methods ----------------------------------------------------------------------------------------
		
		private AdWrapper _wrapper = null;
#if IRONSOURCE
		private bool _isLoaded = false;
#endif
		private int _retryAttempt;

		#endregion
		#region Getters / Setters --------------------------------------------------------------------------------------

		public bool IsVisible { get; private set; }
		
		#endregion
		#region Public Methods -----------------------------------------------------------------------------------------

		public AdBanner(AdWrapper aWrapper)
		{
			_wrapper = aWrapper;
			_wrapper.Log("AdBanner Init");
#if IRONSOURCE
			IronSourceEvents.onBannerAdLoadedEvent += LoadedHandler;
			IronSourceEvents.onBannerAdLoadFailedEvent += LoadFailedHandler;		
			IronSourceEvents.onBannerAdClickedEvent += ClickedHandler; 
			IronSourceEvents.onBannerAdScreenPresentedEvent += PresentedHandler; 
			IronSourceEvents.onBannerAdScreenDismissedEvent += DismissedHandler;
			// IronSourceEvents.onBannerAdLeftApplicationEvent += LeftApplicationHandler;
#endif
		}

		/// <summary>
		/// Отображает баннер.
		/// </summary>
		public AdBanner Show()
		{
#if IRONSOURCE
			_wrapper.Log("Banner Show.");
			IronSource.Agent.displayBanner();
#endif
			return this;
		}

		/// <summary>
		/// Скрывает баннер.
		/// </summary>
		public AdBanner Hide()
		{
#if IRONSOURCE
			_wrapper.Log("Banner Hide.");
			IronSource.Agent.hideBanner();
#endif
			return this;
		}

		/// <summary>
		/// Запрашивает загрузку нового баннера.
		/// </summary>
		/// <param name="aForce">Если TRUE то будет вызвана принудительная загрузка.</param>
		public AdBanner Request(bool aForce = false)
		{
#if IRONSOURCE
			if (!_isLoaded || aForce)
			{
				_wrapper.Log("Request for banner.");
				IronSource.Agent.loadBanner(IronSourceBannerSize.BANNER, IronSourceBannerPosition.BOTTOM);
			}
#endif
#if UNITY_EDITOR
			_wrapper.StartCoroutine(DebugLoadBanner());
#endif
			return this;
		}

		/// <summary>
		/// Уничтожает баннер.
		/// </summary>
		public AdBanner Destroy()
		{
#if IRONSOURCE
			if (_isLoaded)
			{
				_wrapper.Log("Banner is destroyed.");
				IronSource.Agent.destroyBanner();
				_isLoaded = false;
				EventDismissed?.Invoke();
			}
#endif
			return this;
		}

		#endregion
		#region Private Methods ----------------------------------------------------------------------------------------
		
		private IEnumerator DebugLoadBanner()
		{
			yield return new WaitForSeconds(5.0f);
#if IRONSOURCE
			LoadedHandler();
#endif
		}
		
		#endregion
		#region Event Handlers -----------------------------------------------------------------------------------------

		private void DismissedHandler()
		{
			IsVisible = false;
			_wrapper.Log($"Banner Dismissed.");
			EventDismissed?.Invoke();
		}

#if IRONSOURCE
		private void LoadFailedHandler(IronSourceError aError)
		{
			_wrapper.Log($"Banner Load Failed. Code: {aError.getCode()}, Description: {aError.getDescription()}");
			_wrapper.StartCoroutine(WaitReload());
		}
#endif

		private void PresentedHandler()
		{
			IsVisible = true;
			_wrapper.Log($"Banner Presented.");
			EventPresented?.Invoke();
		}

#if IRONSOURCE
		private void LoadedHandler()
		{
			_isLoaded = true;
			_retryAttempt = 0;
			_wrapper.Log($"Banner loaded.");
			EventLoaded?.Invoke();
			EventPresented?.Invoke();
		}
#endif

		private void ClickedHandler()
		{
			_wrapper.Log("Banner is clicked.");
		}

		private void LeftApplicationHandler()
		{
			_wrapper.Log("Banner is Application Left.");
		}

		#endregion
		#region Private Methods ----------------------------------------------------------------------------------------
		
		private IEnumerator WaitReload()
		{
			_retryAttempt++;
			double retryDelay = Math.Pow(2, Math.Min(6, _retryAttempt));
			yield return new WaitForSeconds(_retryAttempt);
			Request();
		}
		
		#endregion
	}
}


