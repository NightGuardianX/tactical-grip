namespace DatsGames
{
	public enum AdId
	{
		AdBannerClick,
		AdBannerError,
		AdRewardShow,
		AdRewardClick,
		AdRewardError,
		AdInterError,
		AdInterShow,
		AdInterClick
	}
}