namespace DatsGames
{
	using UnityEngine;
	
	public class AdWrapper : MonoBehaviour
	{
		[System.Serializable]
		public struct PlatformKey
		{
			public string rewarded;
			public string interstitial;
			public string banner;
		}

		public delegate void WrapperDelegate();
#if IRONSOURCE
		public static event WrapperDelegate EventReady;
#endif

		public delegate void WrapperPauseDelegate(bool aPaused);
		public event WrapperPauseDelegate EventPause;

		#region Public Variables ---------------------------------------------------------------------------------------

		[HideInInspector]
		public bool enableIronSource = false;

		[HideInInspector]
		public bool debugLog = false;

		[HideInInspector]
		public bool debugMode = false;

		[HideInInspector]
		public bool trackStats = true;

		[HideInInspector]
		public bool autoRequest = true;

		[HideInInspector]
		public string iOsKey = "<None>";

		[HideInInspector]
		public string androidKey = "<None>";

		[HideInInspector]
		public string debugIosKey = "8545d445";

		[HideInInspector]
		public string debugAndroidKey = "85460dcd";

		[HideInInspector]
		public bool isUseDelayBetweenInterstitials;

		[HideInInspector]
		public float delayBetweenInterstitials = 25.0f;

		#endregion
		#region Private Variables --------------------------------------------------------------------------------------
		
		private static string _version = "1.3.2 (IronSource)";
		private static bool _isReady = false;

		private static AdWrapper _instance;
		private static AdBanner _banner;
		private static AdRewarded _rewarded;
		private static AdInterstitial _interstitial;

		#endregion
		#region Getters / Setters --------------------------------------------------------------------------------------

		public static AdWrapper Current { get => _instance; }
		public static float LastInterstitialTime { get; set; } = -1.0f;

		public static bool IsUseDelayBetweenInterstitials { get; set; }
		public static float DelayBetweenInterstitials { get; set; }

		public static AdBanner Banner
		{
			get
			{
				Debug.Assert(_banner != null, "[AdWrapper] Banner not initialized!");
				return _banner;
			}
			private set => _banner = value;
		}

		public static AdRewarded Rewarded
		{
			get
			{
				Debug.Assert(_rewarded != null, "[AdWrapper] Rewarded not initialized!");
				return _rewarded;
			}
			private set => _rewarded = value;
		}

		public static AdInterstitial Interstitial
		{
			get
			{
				Debug.Assert(_interstitial != null, "[AdWrapper] Interstitial not initialized!");
				return _interstitial;
			}
			private set => _interstitial = value;
		}

		#endregion
		#region Unity Calls --------------------------------------------------------------------------------------------

		private void Awake()
		{
			_instance = this;

			IsUseDelayBetweenInterstitials = isUseDelayBetweenInterstitials;
			DelayBetweenInterstitials = delayBetweenInterstitials;

			Log($"Version {_version}");

			Interstitial = new AdInterstitial(this);
			Rewarded = new AdRewarded(this);
			Banner = new AdBanner(this);

#if IRONSOURCE
			IronSourceConfig.Instance.setClientSideCallbacks(true);
			
			Log($"Advertiser Id : {IronSource.Agent.getAdvertiserId()}");
			if (debugLog)
			{
				IronSource.Agent.validateIntegration();
			}
			
			var key = GetKey();
			IronSource.Agent.init(key, IronSourceAdUnits.REWARDED_VIDEO);
			IronSource.Agent.init(key, IronSourceAdUnits.INTERSTITIAL);
			IronSource.Agent.init(key, IronSourceAdUnits.BANNER);
#endif

			_isReady = true;

#if IRONSOURCE
			InitializedHandler();
#endif
		}

		private void OnApplicationPause(bool aIsPaused)
		{
#if IRONSOURCE
			IronSource.Agent.onApplicationPause(aIsPaused);
#endif
			EventPause?.Invoke(aIsPaused);
			Log($"Application Pause: {aIsPaused}");
		}

		#endregion
		#region Public Methods -----------------------------------------------------------------------------------------

		public void Track(AdId aId)
		{
			if (trackStats)
			{
				StatsWrapper.Track(aId);
			}
		}

		public void Track(AdId aId, string aKey, string aValue)
		{
			if (trackStats)
			{
				StatsWrapper.Track(aId, new []
				{
					(name: aKey, value: aValue)
				});
			}
		}

		public void Track(AdId aId, int aCode, string aPlaceName)
		{
			if (trackStats)
			{
				StatsWrapper.Track(aId, new []
				{
					(name: "code", value: aCode.ToString()),
					(name: "place", value: aPlaceName)
				});
			}
		}

		public void Log(string aValue)
		{
			if (debugLog)
			{
				Debug.Log($"[AdWrapper] {aValue}");
			}
		}

		public static bool IsReady()
		{
#if !UNITY_EDITOR
			if (!_isReady)
			{
				Debug.LogWarning("[AdWrapper] Not ready yet!");
			}
#endif
			return _isReady;
		}

		private string GetKey()
		{
			string key = string.Empty;
#if UNITY_IOS
			key = (debugMode) ? debugIosKey : iOsKey;
#endif
#if UNITY_ANDROID
			key = (debugMode) ? debugAndroidKey : androidKey;
#endif
			return key;
		}

		#endregion
		#region Event Handlers -----------------------------------------------------------------------------------------

#if IRONSOURCE
		private void InitializedHandler()
		{
			if (autoRequest)
			{
				Log($"Auto request for Ad.");
				Interstitial.Request(true);
				Banner.Request(true);
			}

			EventReady?.Invoke();
		}
#endif
		
		#endregion
	}
}