namespace DatsGames
{
	using UnityEngine;
	using System.Collections;
	using System;

#if GAMEANALYTICS
	using GameAnalyticsSDK;
#endif

	public class AdInterstitial
	{
		public delegate void InterstitialDelegate();

		/// <summary>
		/// Вызывается когда видео загружено и готово к показу.
		/// </summary>
		public event InterstitialDelegate EventReady;

		/// <summary>
		/// Вызывается когда видео загружено и началл проигрываться.
		/// </summary>
		public event InterstitialDelegate EventOpened;

		/// <summary>
		/// Вызывается когда видео закрылось при любом раскладе:
		/// пользователь закрыл видео, возникла ошибка воспроизведение или закрылось
		/// автоматически.
		/// </summary>
		public event InterstitialDelegate EventClosed;

		// Debug Events
		public event InterstitialDelegate EventDebugShow;
		public event InterstitialDelegate EventDebugHide;
		public delegate void InterstitialWatchDelegate(float aCurrentTime, float aTotalTime);
		public event InterstitialWatchDelegate EventDebugWatching;

		#region Private Variables --------------------------------------------------------------------------------------
		
		private int _retryAttempt;
		private AdWrapper _wrapper = null;
		private bool _isReady = false;
		private string _placeName;

		private InterstitialDelegate _readyCallback;
		private InterstitialDelegate _openedCallback;
		private InterstitialDelegate _closedCallback;

		#endregion
		#region Getters / Setters --------------------------------------------------------------------------------------

		public bool IsReady
		{
			get
			{
				bool result = false;
#if IRONSOURCE
				if (_isReady)
				{
					float time = Time.realtimeSinceStartup - AdWrapper.LastInterstitialTime;
					if (AdWrapper.IsUseDelayBetweenInterstitials &&
						(time > AdWrapper.DelayBetweenInterstitials ||
						AdWrapper.LastInterstitialTime < 0.0f))
					{
						result = true;
					}
					else if (!AdWrapper.IsUseDelayBetweenInterstitials)
					{
						result = true;
					}
					else
					{
						_wrapper.Log($"Inter is not available yet. Time since last showing {time.ToString("0.00")} sec.");
					}
				}
#endif
				return result;
			}
			private set => _isReady = value;
		}

		#endregion
		#region Public Methods -----------------------------------------------------------------------------------------

		public AdInterstitial(AdWrapper aWrapper)
		{
			_wrapper = aWrapper;
			_wrapper.Log("Interstitial Initialized.");

#if IRONSOURCE
			IronSourceEvents.onInterstitialAdReadyEvent += ReadyHandler;
			IronSourceEvents.onInterstitialAdLoadFailedEvent += LoadFailedHandler;
			IronSourceEvents.onInterstitialAdShowSucceededEvent += ShowSucceededHandler;
			IronSourceEvents.onInterstitialAdShowFailedEvent += ShowFailedHandler;
			IronSourceEvents.onInterstitialAdClickedEvent += ClickedHandler;
			IronSourceEvents.onInterstitialAdOpenedEvent += OpenedHandler;
			IronSourceEvents.onInterstitialAdClosedEvent += ClosedHandler;
#endif
		}

		/// <summary>
		/// Запрашивает загрузку нового видео.
		/// </summary>
		/// <param name="aForce">Если TRUE то принудительно запрашивает новое видео.</param>
		public AdInterstitial Request(bool aForce = false)
		{
#if IRONSOURCE
			if (AdWrapper.IsReady() && (aForce || !IsReady))
			{
				IronSource.Agent.loadInterstitial();
			}
#endif
#if UNITY_EDITOR
			if (aForce || !IsReady)
			{
				_wrapper.StartCoroutine(DebugLoadInterstitial());
			}
#endif
			return this;
		}

		/// <summary>
		/// Запускает воспроизведение видео.
		/// </summary>
		/// <param name="aPlace">Место отображения видео.</param>
		/// <typeparam name="T">Enum</typeparam>
		/// <returns>AdInterstitial</returns>
		public AdInterstitial Show<T>(T aPlace) where T : System.Enum
		{
			_placeName = aPlace.ToString();
			_wrapper.Log($"Show interstitial in `{_placeName}` place.");

			_wrapper.StartCoroutine(ShowInterstitial());
			return this;
		}

		/// <summary>
		/// Запускает воспроизведение видео.
		/// </summary>
		/// <returns>AdInterstitial</returns>
		public AdInterstitial Show()
		{
			_placeName = "Unnamed";
			_wrapper.Log($"Show interstitial in `{_placeName}` place.");

			_wrapper.StartCoroutine(ShowInterstitial());
			return this;
		}

		/// <summary>
		/// Устанавливает одноразовый вызов на событие загрузки и готовности к показу видео.
		/// </summary>
		/// <param name="aCallback">Обратный вызов.</param>
		/// <returns>AdInterstitial</returns>
		public AdInterstitial OnReady(InterstitialDelegate aCallback)
		{
			_readyCallback = aCallback;
			return this;
		}
		
		/// <summary>
		/// Устанавливает одноразовый вызов на событие когда видео открылось.
		/// </summary>
		/// <param name="aCallback">Обратный вызов.</param>
		/// <returns>AdInterstitial</returns>
		public AdInterstitial OnOpened(InterstitialDelegate aCallback)
		{
			_openedCallback = aCallback;
			return this;
		}

		/// <summary>
		/// Устанавливает одноразовый вызов на событие закрытия видео при любом раскладе:
		/// награда получена, награда не получена, ролик не удалось отобразить,
		/// видео было закрыто пользователем.
		/// </summary>
		/// <param name="aCallback">Обратный вызов.</param>
		/// <returns>AdInterstitial</returns>
		public AdInterstitial OnClosed(InterstitialDelegate aCallback)
		{
			_closedCallback = aCallback;
			return this;
		}

		#endregion
		#region Private Methods ----------------------------------------------------------------------------------------

		private IEnumerator ShowInterstitial()
		{
			yield return new WaitForSeconds(0.1f);
#if IRONSOURCE
			if (IsReady)
			{
				IronSource.Agent.showInterstitial(_placeName);
			}
			else
			{
				_wrapper.Log("Interstitial not available yet.");
			}
#endif
#if UNITY_EDITOR
			EventDebugShow?.Invoke();
			_wrapper.StartCoroutine(DebugWatchInterstitial());
#endif
		}
		
		private IEnumerator WaitReload()
		{
			// We recommend retrying with exponentially higher delays up to a 
			// maximum delay (in this case 64 seconds)
			_retryAttempt++;
			double retryDelay = Math.Pow(2, Math.Min(6, _retryAttempt));
			yield return new WaitForSeconds(_retryAttempt);
			Request();
		}

		private IEnumerator DebugLoadInterstitial()
		{
			yield return new WaitForSeconds(2.0f);
			ReadyHandler();
		}

		private IEnumerator DebugWatchInterstitial()
		{
			EventOpened?.Invoke();
			
			float total = 2.5f;
			float current = 0.0f;

			while (current < total)
			{
				current += Time.deltaTime;
				EventDebugWatching?.Invoke(current, total);
				yield return null;
			}

			EventDebugWatching?.Invoke(total, total);
			EventDebugHide?.Invoke();

			ClosedHandler();
		}
		
		#endregion
		#region Event Handlers -----------------------------------------------------------------------------------------

		private void ReadyHandler()
		{
			IsReady = true;

			_readyCallback?.Invoke();
			_readyCallback = null;
			EventReady?.Invoke();

			_wrapper.Log($"Interstitial is Ready.");
			_retryAttempt = 0;
		}

		private void OpenedHandler()
		{
			_wrapper.Log($"Interstitial is Opened.");
		}

		private void ShowSucceededHandler()
		{
			EventOpened?.Invoke();
			_openedCallback?.Invoke();
			_openedCallback = null;
			IsReady = false;

#if GAMEANALYTICS && IRONSOURCE
			GameAnalytics.NewAdEvent(GAAdAction.Show, GAAdType.Interstitial, IronSource.pluginVersion(), _placeName);
#else
			_wrapper.Track(AdId.AdInterShow, "place", _placeName);
#endif
			_wrapper.Log($"Interstitial is Showed.");

#if GAMEANALYTICS
			GameAnalytics.StartTimer(_placeName);
			_wrapper.EventPause += AppPauseHandler;
#endif
		}

#if IRONSOURCE
		private void LoadFailedHandler(IronSourceError aError)
		{
			_wrapper.Track(AdId.AdInterError, aError.getCode(), aError.getDescription());
			_wrapper.Log($"Interstitial load Failed." +
						 $"Code: {aError.getCode()}, " +
						 $"Desc: {aError.getDescription()}.");
			
			_wrapper.StartCoroutine(WaitReload());
		}
#endif

#if IRONSOURCE
		private void ShowFailedHandler(IronSourceError aError)
		{
	#if GAMEANALYTICS
			GameAnalytics.NewAdEvent(GAAdAction.FailedShow, GAAdType.Interstitial, IronSource.pluginVersion(), _placeName);
	#else
			_wrapper.Track(AdId.AdInterError, aError.getCode(), aError.getDescription());
	#endif
			_wrapper.Log($"Interstitial show Failed. Code: {aError.getCode()}, Desc: {aError.getDescription()}.");

			ClosedHandler();
		}
#endif

		private void ClosedHandler()
		{
			_closedCallback?.Invoke();
			_closedCallback = null;

			EventClosed?.Invoke();

#if GAMEANALYTICS && IRONSOURCE
			_wrapper.EventPause -= AppPauseHandler;
			GameAnalytics.StopTimer(_placeName);
			long elapsedTime = GameAnalytics.StopTimer(_placeName);
			_wrapper.Log($"Elapsed Time: {elapsedTime}");
			GameAnalytics.NewAdEvent(GAAdAction.Show, GAAdType.Interstitial, IronSource.pluginVersion(), _placeName, elapsedTime);
#endif
			AdWrapper.LastInterstitialTime = Time.realtimeSinceStartup;
			_wrapper.Log("Interstitial is Closed.");

			if (_wrapper.autoRequest)
			{
				Request();
			}
		}

		private void ClickedHandler()
		{
#if GAMEANALYTICS && IRONSOURCE
			GameAnalytics.NewAdEvent(GAAdAction.Clicked, GAAdType.Interstitial, IronSource.pluginVersion(), _placeName);
#else
			_wrapper.Track(AdId.AdInterClick, "place", _placeName);
#endif
			_wrapper.Log("InterstitialAd is clicked.");
		}

		private void AppPauseHandler(bool aPaused)
		{
#if GAMEANALYTICS
			if (aPaused)
			{
				GameAnalytics.PauseTimer(_placeName);
			}
			else
			{
				GameAnalytics.ResumeTimer(_placeName);
			}
#endif
		}

		#endregion
	}
}