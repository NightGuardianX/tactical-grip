namespace DatsGames
{
	using UnityEngine;
	using System.Collections;
	using System;

#if GAMEANALYTICS
	using GameAnalyticsSDK;
#endif

	public class AdRewarded
	{
		public delegate void RewardedDelegate();

		/// <summary>
		/// Вызывается когда видео загружено и готово к показу. 
		/// </summary>
		public event RewardedDelegate EventReady;

		/// <summary>
		/// Вызывается когда видео открылось и началось воспроизведение.
		/// </summary>
		public event RewardedDelegate EventOpened;

		/// <summary>
		/// Вызывается когда видео было закрыто при любом раскладе:
		/// получена награда, не получина награда, ошибка воспроизведения.
		/// </summary>
		public event RewardedDelegate EventClosed;

		/// <summary>
		/// Вызывается перед закрытием видео если игрок получил награду.
		/// </summary>
		public event RewardedDelegate EventSuccess;

		/// <summary>
		/// Вызывается перед закрытием видео если игрок не получил награду.
		/// </summary>
		public event RewardedDelegate EventFailure;

		/// <summary>
		/// Вызывается при взаимодействии с рекламой.
		/// </summary>
		public event RewardedDelegate EventClick;

		/// <summary>
		/// Вызывается когда видео отобразилось (только в режиме редактора).
		/// </summary>
		public event RewardedDelegate EventDebugShow;

		/// <summary>
		/// Вызывается когда видео закрылось (только в режиме редактора).
		/// </summary>
		public event RewardedDelegate EventDebugHide;

		public delegate void RewardedWatchDelegate(float aCurrentTime, float aTotalTime);

		/// <summary>
		/// Вызывается постоянно когда видео проигрывается (только в режиме редактора).
		/// </summary>
		public event RewardedWatchDelegate EventDebugWatching;

		#region Private Variables --------------------------------------------------------------------------------------
		
		private RewardedDelegate _readyCallback;
		private RewardedDelegate _openedCallback;
		private RewardedDelegate _closedCallback;
		private RewardedDelegate _successCallback;
		private RewardedDelegate _clickCallback;
		private RewardedDelegate _failureCallback;

		private bool _isRewardedSuccess = false;
		private object _rewardData;
		private AdWrapper _wrapper = null;
		private string _placeName;

		#endregion
		#region Getters / Setters --------------------------------------------------------------------------------------

		public bool IsReady { get; private set; }
		
		#endregion
		#region Public Methods -----------------------------------------------------------------------------------------

		public AdRewarded(AdWrapper aWrapper)
		{
			_wrapper = aWrapper;
			_wrapper.Log("Rewarded Initialized.");

#if IRONSOURCE
			IronSourceEvents.onRewardedVideoAdOpenedEvent += OpenedHandler;
			IronSourceEvents.onRewardedVideoAdClosedEvent += ClosedHandler; 
			IronSourceEvents.onRewardedVideoAvailabilityChangedEvent += AvailabilityChangedHandler;
			IronSourceEvents.onRewardedVideoAdStartedEvent += StartedHandler;
			IronSourceEvents.onRewardedVideoAdEndedEvent += EndedHandler;
			IronSourceEvents.onRewardedVideoAdRewardedEvent += RecievedRewardHandler; 
			IronSourceEvents.onRewardedVideoAdShowFailedEvent += ShowFailedHandler; 
			// IronSourceEvents.onRewardedVideoAdClickedEvent += ClickedHandler;
#endif
#if UNITY_EDITOR
			_wrapper.StartCoroutine(DebugLoadRewarded());
#endif
		}

		/// <summary>
		/// Запускает процесс отображения и воспроизведения видео.
		/// </summary>
		/// <param name="aPlace">Место отображения видо.</param>
		/// <typeparam name="T">Enum</typeparam>
		/// <returns>AdRewarded</returns>
		public AdRewarded Show<T>(T aPlace) where T : System.Enum
		{
			_isRewardedSuccess = false;
			_placeName = aPlace.ToString();
			_wrapper.Log($"Rewarded show on `{_placeName}`.");
			_wrapper.StartCoroutine(ShowRewarded());
			return this;
		}

		/// <summary>
		/// Запускает процесс отображения и воспроизведения видео.
		/// </summary>
		/// <returns>AdRewarded</returns>
		public AdRewarded Show()
		{
			_isRewardedSuccess = false;
			_placeName = "Unnamed";
			_wrapper.Log($"Rewarded show on `{_placeName}`.");
			_wrapper.StartCoroutine(ShowRewarded());
			return this;
		}

		/// <summary>
		/// Устанавливает одноразовый вызов на готовность видео к показу.
		/// </summary>
		/// <param name="aCallback">Обратный вызов.</param>
		/// <returns>AdRewarded</returns>
		public AdRewarded OnReady(RewardedDelegate aCallback)
		{
			_readyCallback = aCallback;
			return this;
		}

		/// <summary>
		/// Устанавливает одноразовый вызов на событие открытия видео.
		/// </summary>
		/// <param name="aCallback">Обратный вызов.</param>
		/// <returns>AdRewarded</returns>
		public AdRewarded OnOpened(RewardedDelegate aCallback)
		{
			_openedCallback = aCallback;
			return this;
		}

		/// <summary>
		/// Устаналивает одноразовый вызов на событие закрытия видео.
		/// Вызывается при любом раскладе: если награда получена, если награда
		/// не получена (видео закрыто пользователем), если возникла ошибка
		/// воспроизведения.
		/// </summary>
		/// <param name="aCallback">Обратный вызов.</param>
		/// <returns>AdRewarded</returns>
		public AdRewarded OnClosed(RewardedDelegate aCallback)
		{
			_closedCallback = aCallback;
			return this;
		}
		
		/// <summary>
		/// Устанавливает одноразовый вызов на событие успешного получения награды.
		/// Вызывается до закрытия видео.
		/// </summary>
		/// <param name="aCallback">Обратный вызов.</param>
		/// <returns>AdRewarded</returns>
		public AdRewarded OnSuccess(RewardedDelegate aCallback)
		{
			_successCallback = aCallback;
			return this;
		}

		/// <summary>
		/// Устанавливает одноразовый вызов на событие не получения награды:
		/// видео было закрыто пользователем, или не удалось воспроизвести.
		/// Вызывается до закрытия видео.
		/// </summary>
		/// <param name="aCallback">Обратный вызов.</param>
		/// <returns>AdRewarded</returns>
		public AdRewarded OnFailure(RewardedDelegate aCallback)
		{
			_failureCallback = aCallback;
			return this;
		}

		/// <summary>
		/// Устанавливает одноразовый вызов на событие взаимодействия с рекламой.
		/// </summary>
		/// <param name="aCallback">Обратный вызов.</param>
		/// <returns>AdRewarded</returns>
		public AdRewarded OnClick(RewardedDelegate aCallback)
		{
			_clickCallback = aCallback;
			return this;
		}
		
		#endregion
		#region Private Methods ----------------------------------------------------------------------------------------

		private IEnumerator ShowRewarded()
		{
			yield return new WaitForSeconds(0.1f);

#if IRONSOURCE
			if (IsReady)
			{
				IronSource.Agent.showRewardedVideo();
			}
			else
			{
				_wrapper.Log("Rewarded not ready yet.");
			}
#endif
#if UNITY_EDITOR
			EventDebugShow?.Invoke();
			_wrapper.StartCoroutine(DebugWatchRewarded());
#endif
		}

		private IEnumerator DebugLoadRewarded()
		{
			yield return new WaitForSeconds(UnityEngine.Random.Range(20.0f, 30.0f));
			AvailabilityChangedHandler(true);
		}

#if UNITY_EDITOR
		private IEnumerator DebugWatchRewarded()
		{
			EventOpened?.Invoke();
			
			float total = 5.0f;
			float current = 0.0f;

			while (current < total)
			{
				current += Time.deltaTime;
				EventDebugWatching?.Invoke(current, total);
				yield return null;
			}

			EventDebugWatching?.Invoke(total, total);
			EventDebugHide?.Invoke();

			RecievedRewardHandler(null);
			ClosedHandler();

			IsReady = false;
			_wrapper.StartCoroutine(DebugLoadRewarded());
		}
#endif
		
		#endregion
		#region Event Handlers -----------------------------------------------------------------------------------------

		private void AvailabilityChangedHandler(bool aCanShowAd)
		{
			IsReady = aCanShowAd;

			if (aCanShowAd)
			{
				EventReady?.Invoke();

				_readyCallback?.Invoke();
				_readyCallback = null;
			}

			_wrapper.Log($"Rewarded Availability is Changed, IsReady = {aCanShowAd}");
		}

		private void RecievedRewardHandler(object aSsp)
		{
			_isRewardedSuccess = true;
			_rewardData = aSsp;
		}

		private void ClosedHandler()
		{
			if (_isRewardedSuccess)
            {
				// Награда получена.
				EventSuccess?.Invoke();
				_successCallback?.Invoke();
				_successCallback = null;

#if GAMEANALYTICS && IRONSOURCE
				GameAnalytics.NewAdEvent(
					GAAdAction.RewardReceived, 
					GAAdType.RewardedVideo, 
					IronSource.pluginVersion(), 
					_placeName
				);
#endif
				_wrapper.Log($"Rewarded is Recieved: {_rewardData}.");
			}
			else
			{
				// Награда не получена.
				EventFailure?.Invoke();
				_failureCallback?.Invoke();
				_failureCallback = null;
			}

			EventClosed?.Invoke();
			_closedCallback?.Invoke();
			_closedCallback = null;

			AdWrapper.LastInterstitialTime = Time.realtimeSinceStartup;
			_wrapper.Log("Rewarded is Closed.");
		}

		private void OpenedHandler()
		{
			EventOpened?.Invoke();
			_openedCallback?.Invoke();
			_openedCallback = null;

			_wrapper.Log("Rewarded is Opened.");
		}

		private void StartedHandler()
		{
#if GAMEANALYTICS
			GameAnalytics.StartTimer(_placeName);
			_wrapper.EventPause += AppPauseHandler;
#endif
			_wrapper.Log("Rewarded is Started.");
		}

		private void EndedHandler()
		{
#if GAMEANALYTICS && IRONSOURCE
			_wrapper.EventPause -= AppPauseHandler;
			GameAnalytics.StopTimer(_placeName);
			long elapsedTime = GameAnalytics.StopTimer(_placeName);
			_wrapper.Log($"Elapsed Time: {elapsedTime}");
			GameAnalytics.NewAdEvent(
				GAAdAction.Show, 
				GAAdType.RewardedVideo, 
				IronSource.pluginVersion(), 
				_placeName, 
				elapsedTime
			);
#endif
			_wrapper.Log("Rewarded is Ended.");
		}

#if IRONSOURCE
		private void ShowFailedHandler(IronSourceError aError)
		{
			var desc = aError.getDescription();
			var str = (desc != null) ? desc.ToString() : "No Description";

	#if GAMEANALYTICS
			GameAnalytics.NewAdEvent(
				GAAdAction.FailedShow, 
				GAAdType.RewardedVideo, 
				IronSource.pluginVersion(), 
				_placeName
			);
	#else
			_wrapper.Track(AdId.AdRewardError, aError.getCode(), str);
	#endif
			_wrapper.Log($"Rewarded show Failed. Code: {aError.getCode()}, Desc: {str}");

			ClosedHandler();
		}
#endif

		private void ClickedHandler(string aUnitId)
		{
			EventClick?.Invoke();
			_clickCallback?.Invoke();
			_clickCallback = null;
			
#if GAMEANALYTICS && IRONSOURCE
			GameAnalytics.NewAdEvent(
				GAAdAction.Clicked, 
				GAAdType.RewardedVideo, 
				IronSource.pluginVersion(), 
				_placeName
			);
#else
			_wrapper.Track(AdId.AdRewardClick, "place", _placeName);
#endif
			_wrapper.Log($"Rewarded is Clicked. Place: {_placeName}.");
		}

		private void AppPauseHandler(bool aPaused)
		{
#if GAMEANALYTICS
			if (aPaused)
			{
				GameAnalytics.PauseTimer(_placeName);
			}
			else
			{
				GameAnalytics.ResumeTimer(_placeName);
			}
#endif
		}
		
		#endregion
	}
}