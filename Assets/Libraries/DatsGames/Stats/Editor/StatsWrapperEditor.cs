namespace DatsGames
{
	using UnityEngine;
	using UnityEditor;

	[CustomEditor(typeof(StatsWrapper))]
	public class StatsWrapperEditor : Editor
	{
		private StatsWrapper _self;

		#region Unity Calls

		private void OnEnable()
		{
			_self = (StatsWrapper) target;
			_self.enableFacebook = DefineHelper.IsExists("FACEBOOK");
			_self.enableAmplitude = DefineHelper.IsExists("AMPLITUDE");
			_self.enableFirebase = DefineHelper.IsExists("FIREBASE");
			_self.enableGameAnalytics = DefineHelper.IsExists("GAMEANALYTICS");
		}

		public override void OnInspectorGUI()
		{
			base.OnInspectorGUI();

			if (_self.debugLog)
			{
				EditorGUILayout.HelpBox("Don't forget to disable `Debug Log` option for production build of the game!", MessageType.Warning);
			}

			EditorGUI.BeginChangeCheck();

			// Facebook
			// ---------
			bool prevFacebook = _self.enableFacebook;
			EditorGUILayout.BeginVertical(EditorStyles.helpBox);
			{
				_self.enableFacebook = EditorGUILayout.BeginToggleGroup("Facebook", _self.enableFacebook);
				{
					if (_self.enableFacebook)
					{
						EditorGUI.indentLevel++;
						EditorGUILayout.LabelField("Please use standart setup window for this SDK.", EditorStyles.miniLabel);
						EditorGUI.indentLevel--;
					}
				}
				EditorGUILayout.EndToggleGroup();
			}
			EditorGUILayout.EndVertical();

			// Amplitude
			// ---------
			bool prevAmplitude = _self.enableAmplitude;
			EditorGUILayout.BeginVertical(EditorStyles.helpBox);
			{
				_self.enableAmplitude = EditorGUILayout.BeginToggleGroup("Amplitude", _self.enableAmplitude);
				{
					if (_self.enableAmplitude)
					{
						EditorGUI.indentLevel++;
						_self.projectId = EditorGUILayout.TextField("Project ID", _self.projectId);
						_self.apiKey = EditorGUILayout.TextField("Api Key", _self.apiKey);
						_self.secretKey = EditorGUILayout.TextField("Secret Key", _self.secretKey);
						EditorGUI.indentLevel--;
					}
				}
				EditorGUILayout.EndToggleGroup();
			}
			EditorGUILayout.EndVertical();
			
			// Firebase
			// --------
			bool prevFirebase = _self.enableFirebase;
			EditorGUILayout.BeginVertical(EditorStyles.helpBox);
			{
				_self.enableFirebase = EditorGUILayout.BeginToggleGroup("Firebase", _self.enableFirebase);
				{
					if (_self.enableFirebase)
					{
						EditorGUI.indentLevel++;
						EditorGUILayout.LabelField("Please use standart setup window for this SDK.", EditorStyles.miniLabel);
						EditorGUI.indentLevel--;
					}
				}
				EditorGUILayout.EndToggleGroup();
			}
			EditorGUILayout.EndVertical();

			// Game Analytics.
			// ---------------
			bool prevGameAnalytics = _self.enableGameAnalytics;
			EditorGUILayout.BeginVertical(EditorStyles.helpBox);
			{
				_self.enableGameAnalytics = EditorGUILayout.BeginToggleGroup("GameAnalytics", _self.enableGameAnalytics);
				{
					if (_self.enableGameAnalytics)
					{
						EditorGUI.indentLevel++;
						EditorGUILayout.LabelField("Please use standart setup window for this SDK.", EditorStyles.miniLabel);
						EditorGUI.indentLevel--;
					}
				}
				EditorGUILayout.EndToggleGroup();
			}
			EditorGUILayout.EndVertical();

			if (EditorGUI.EndChangeCheck())
			{
				if (_self.enableFacebook != prevFacebook)
				{
					DefineHelper.SetEnabled("FACEBOOK", _self.enableFacebook, true);
				}

				if (_self.enableAmplitude != prevAmplitude)
				{
					DefineHelper.SetEnabled("AMPLITUDE", _self.enableAmplitude, true);
				}

				if (_self.enableFirebase != prevFirebase)
				{
					DefineHelper.SetEnabled("FIREBASE", _self.enableFirebase, true);
				}

				if (_self.enableGameAnalytics != prevGameAnalytics)
				{
					DefineHelper.SetEnabled("GAMEANALYTICS", _self.enableGameAnalytics, true);
				}
				EditorUtility.SetDirty(target);
			}
		}

		#endregion
	}
}