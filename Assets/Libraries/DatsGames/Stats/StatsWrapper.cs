namespace DatsGames
{
	using System.Collections.Generic;
	using UnityEngine;

#if FACEBOOK
	using Facebook.Unity;
#endif

#if FIREBASE
	using Firebase;
#endif

#if GAMEANALYTICS
	using GameAnalyticsSDK;
#endif
	
	public class StatsWrapper : MonoBehaviour
	{
		public bool debugLog;

		[HideInInspector]
		public bool enableFacebook;

		[HideInInspector]
		public bool enableAmplitude;

		[HideInInspector]
		public string projectId = string.Empty;

		[HideInInspector]
		public string apiKey = string.Empty;

		[HideInInspector]
		public string secretKey = string.Empty;

		[HideInInspector]
		public bool enableFirebase;

		[HideInInspector]
		public bool enableGameAnalytics;

		private static StatsWrapper _instance;
		private static bool _isReadyAmplitude = false;
		private static bool _isReadyFirebase = false;
		private static bool _isReadyGameAnalytics = false;
		private static bool _isDebug = false;
		
		#region Getters / Setters

		/// <summary>
		/// Returns status of Amplitude SDK.
		/// </summary>
		/// <value>Returns true if Amplitude is initialized.</value>
		public static bool IsReadyAmplitude
		{
			get => _isReadyAmplitude;
		}

		/// <summary>
		/// Returns status of GameAnalytics SDK.
		/// </summary>
		/// <value>Returns true if GameAnalytics is initialized.</value>
		public static bool IsReadyGameAnalytics
		{
			get => _isReadyGameAnalytics;
		}

		/// <summary>
		/// Returns status of Firebase SDK.
		/// </summary>
		/// <value>Returns true if Firebase is initialized.</value>
		public static  bool IsReadyFirebase
		{
			get => _isReadyFirebase;
		}

		/// <summary>
		/// Returns Session ID from Amplitude.
		/// </summary>
		/// <value>Session ID</value>
#if AMPLITUDE
		public static long SessionID
		{
			get => (_isReadyAmplitude)
				? Amplitude.Instance.getSessionId()
				: Random.Range(1000, 9999);
		}
#else
		public static long SessionID
		{
			get => Random.Range(1000, 9999);
		}
#endif

		/// <summary>
		/// Returns device ID from Amplitude.
		/// </summary>
		/// <value>Device ID</value>
#if AMPLITUDE
		public static string DeviceID
		{
			get => (_isReadyAmplitude)
				? Amplitude.Instance.getDeviceId()
				: "<Undifined>";
		}
#else
		public string DeviceID
		{
			get => "<Undefined>";
		}
#endif
		
		#endregion
		#region Unity Calls
	
		private void Awake()
		{
			_instance = this;
			_isDebug = debugLog;
			InitializeFacebook();
			InitializeAmplitude();
			InitializeFirebase();
			InitializeGameAnalytics();
		}

		private void OnDestroy()
		{
			_instance = null;
		}
	
		#endregion
		#region Public Methods

		/// <summary>
		/// Sets unique property for current user. Useful for A/B tests.
		/// </summary>
		/// <param name="aKey">Name of property.</param>
		/// <param name="aValue">Value of property.</param>
		public static void SetOnceUserProperty(string aKey, string aValue)
		{
#if AMPLITUDE
			if (_isReadyAmplitude)
			{
				Amplitude.Instance.setOnceUserProperty(aKey, aValue);
			}
#endif
		}

		public static void SetCustomDimension(string aValue, int aDimensionIndex = 1)
		{
#if GAMEANALYTICS
			if (_isReadyGameAnalytics)
			{
				switch (aDimensionIndex)
				{
					case 1 :
						GameAnalytics.SetCustomDimension01(aValue);
						break;

					case 2 :
						GameAnalytics.SetCustomDimension02(aValue);
						break;

					case 3 :
						GameAnalytics.SetCustomDimension03(aValue);
						break;

					default :
						Debug.LogWarning($"[StatsWrapper] Dimension `{aDimensionIndex}` not available!");
						break;
				}
			}
#endif
			Log($"Set value `{aValue}` to `{aDimensionIndex}` dimension.");
		}

		/// <summary>
		/// Sends simple event with specified identify.
		/// </summary>
		/// <param name="aEnumID">Your enum item.</param>
		/// <typeparam name="T">Type of your enum.</typeparam>
		public static void Track<T>(T aEnumID) where T : System.Enum
		{
#if AMPLITUDE
			if (_isReadyAmplitude)
			{
				Amplitude.Instance.logEvent(aEnumID.ToString());
			}
#endif
#if FIREBASE
			if (_isReadyFirebase)
			{
				Firebase.Analytics.FirebaseAnalytics.LogEvent(aEnumID.ToString());
			}
#endif
#if GAMEANALYTICS
			if (_isReadyGameAnalytics)
			{
				GameAnalytics.NewDesignEvent(aEnumID.ToString());
			}
#endif
			Log($"Event: <b>{aEnumID.ToString()}</b>.");
		}

		public static void Track<T>(T aEnumID, string aKey, object aValue) where T : System.Enum
		{
			Track<T>(aEnumID, new [] { (name: aKey, value: aValue.ToString()) });
		}

		/// <summary>
		/// Sends Event Property to Amplitude or Firebase.
		/// 
		/// StatsWrapper.Current.TrackEventProperty(TrackID.MyEventName, new [] 
		/// {
		///		(name: "code", value: errCode),
		/// 	(name: "place", value: "tryNewWeapon")
		/// });
		/// </summary>
		/// <param name="aEnumID">Enum of the event kind.</param>
		/// <param name="aProperties">List of the properties.</param>
		public static void Track<T>(T aEnumID, (string name, string value)[] aProperties) where T : System.Enum
		{
#if AMPLITUDE
			if (_isReadyAmplitude)
			{
				var dict = new Dictionary<string, object>();
				for (int i = 0, n = aProperties.Length; i < n; i++)
				{
					 dict.Add(aProperties[i].name, aProperties[i].value);
				}

				Amplitude.Instance.logEvent(aEnumID.ToString(), dict);
			}
#endif
#if FIREBASE
			if (_isReadyFirebase)
			{
				var parameters = new Firebase.Analytics.Parameter[aProperties.Length];
				for (int i = 0, n = aProperties.Length; i < n; i++)
				{
					parameters[i] = new Firebase.Analytics.Parameter(aProperties[i].name, aProperties[i].value);
				}

				Firebase.Analytics.FirebaseAnalytics.LogEvent(aEnumID.ToString(), parameters);
			}
#endif
#if GAMEANALYTICS
			if (_isReadyGameAnalytics)
			{
				var arr = new string[aProperties.Length];
				for (int i = 0, n = aProperties.Length; i < n; i++)
				{
					arr[i] = $"{aProperties[i].name}{aProperties[i].value}";
				}

				var str = string.Join(":", arr);
				GameAnalytics.NewDesignEvent($"{aEnumID.ToString()}:{str}");
			}
#endif
			if (_isDebug)
			{
				var arr = new string[aProperties.Length];
				for (int i = 0, n = aProperties.Length; i < n; i++)
				{
					arr[i] = $"{aProperties[i].name}={aProperties[i].value}";
				}

				var str = string.Join(",", arr);
				Log($"TrackEventProperty: <b>{aEnumID.ToString()}</b> ({str})");
			}
		}

		/// <summary>
		/// Tracking of virtual expenses like a coins, gems and e.t.c.
		/// </summary>
		/// <param name="aItemName">Item name.</param>
		/// <param name="aCost">Cost.</param>
		/// <param name="aCurrency">Currency - coins, gems or something else.</param>
		public static void TrackVirtualCurrency(string aItemName, int aCost, string aCurrency = "coin")
		{
#if FIREBASE
			if (!_isReadyFirebase)
			{
				Firebase.Analytics.Parameter[] parameters = {
					new Firebase.Analytics.Parameter(Firebase.Analytics.FirebaseAnalytics.ParameterItemName, aItemName),
					new Firebase.Analytics.Parameter(Firebase.Analytics.FirebaseAnalytics.ParameterValue, aCost),
					new Firebase.Analytics.Parameter(Firebase.Analytics.FirebaseAnalytics.ParameterVirtualCurrencyName, aCurrency),
				};

				Firebase.Analytics.FirebaseAnalytics.LogEvent(
					Firebase.Analytics.FirebaseAnalytics.EventSpendVirtualCurrency, 
					parameters
				);
			}
#endif
#if AMPLITUDE
			Debug.LogWarning("TrackCurrency for Amplitude not implemented yet!");
#endif
#if GAMEANALYTICS
			Debug.LogWarning("TrackCurrence for GameAnalytics not implemented yet!");
#endif
			Log($"TrackCurrency: <b>{aItemName}</b>, cost: {aCost}.");
		}
		
		#endregion
		#region Private Methods

		private void InitializeFacebook()
		{
#if FACEBOOK
			FB.Init(FBInitCompleteHandler, FBHideUnityHandler);
#endif
		}

		private void InitializeAmplitude()
		{
#if AMPLITUDE
			try
			{
				Amplitude amplitude = Amplitude.Instance;
				amplitude.logging = debugLog;
				amplitude.init(apiKey);
				_isReadyAmplitude = true;
				Log("Amplitude Initialized!");
			}
			catch (System.Exception aException)
			{
				Debug.LogWarning(aException);
			}
#endif
		}

		private void InitializeFirebase()
		{
#if FIREBASE
			Firebase.FirebaseApp.CheckAndFixDependenciesAsync()
				.ContinueWith(task =>
				{
					if (task.Result == Firebase.DependencyStatus.Available)
					{
						// _firebase = Firebase.FirebaseApp.DefaultInstance;
						_isReadyFirebase = true;
						Log("Firebase Initialized!");
					}
					else
					{
						Debug.LogError($"Could not resolve all Firebase dependencies: {task.Result}");
					}
				});
#endif
		}

		private void InitializeGameAnalytics()
		{
#if GAMEANALYTICS
			GameAnalytics.Initialize();
			_isReadyGameAnalytics = true;
			Log("GameAnalytics Initialized!");
#endif
		}
		
		private static void Log(string aValue)
		{
			if (_isDebug)
			{
				Debug.Log($"[StatsWrapper] {aValue}");
			}
		}
		
		#endregion
		#region Event Handlers

		private void FBInitCompleteHandler()
		{
#if FACEBOOK
			Log($"IsLoggedIn='{FB.IsLoggedIn}' IsInitialized='{FB.IsInitialized}', AppID='{FB.AppId}'");
#endif
		}

		private void FBHideUnityHandler(bool aIsGameShown)
		{
#if FACEBOOK
			Log($"Success Response: FBHideUnityHandler Called {aIsGameShown}");
#endif
		}

		#endregion
	}
}
