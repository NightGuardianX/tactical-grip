namespace DatsGames
{
    using UnityEngine;

	public abstract class Decision : MonoBehaviour
	{
		public virtual void Execute()
		{
			// ..
		}
	}
}