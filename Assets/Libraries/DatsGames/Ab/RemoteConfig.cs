namespace DatsGames
{
	using UnityEngine;
	using System.Collections.Generic;

#if GAMEANALYTICS
	using GameAnalyticsSDK;
#endif

	public class RemoteConfig : MonoBehaviour
	{
		[HideInInspector]
		public bool debugLog;

		public enum Kind
		{
			ABTest,
			Config
		}

		[System.Serializable]
		public struct DecisionItem
		{
			public string key;
			public Decision decision;
		}

		[System.Serializable]
		public struct ConfigItem
		{
			public Kind kind;
			public string name;
			public string defaultValue;
			public bool isOpened;
			public DecisionItem[] decisions;
		}

		[HideInInspector]
		public ConfigItem[] configs = new ConfigItem[0];

		public delegate void RemoteConfigDelegate();
		public event RemoteConfigDelegate EventConfigUpdate;

		private RemoteConfigDelegate _configUpdateCallback;

		private static Dictionary<string, string> _debugKeys = new Dictionary<string, string>();
		private static RemoteConfig _current;
		private static bool _isDebugLog;

		#region Getters / Setters

		public static bool IsInitialized { get => (Current != null); }

		public static RemoteConfig Current
		{
			get
			{
				if (_current == null)
				{
					Debug.LogError($"[RemoteConfig] Not initialized yet!");
				}
				return _current;
			}
		}

		#endregion
		#region Unity Calls

		private void Awake()
		{
			_isDebugLog = debugLog;
			_current = this;
			Log("Remote Config Initialize!");

#if GAMEANALYTICS
			GameAnalytics.OnRemoteConfigsUpdatedEvent += RemoteConfigUpdatedHandler;
#endif
		}

		private void OnDestroy()
		{
#if GAMEANALYTICS
			GameAnalytics.OnRemoteConfigsUpdatedEvent -= RemoteConfigUpdatedHandler;
#endif
		}

		#endregion
		#region Public Methods

		public static void OnConfigUpdate(RemoteConfigDelegate aCallback)
		{
			if (!IsInitialized)
			{
				return;
			}

			_current._configUpdateCallback = aCallback;
		}

		/// <summary>
		/// Устанавливает тестовый ключ для отладки А/Б теста из кода.
		/// </summary>
		/// <param name="aConfigId">Идентификатор теста.</param>
		/// <param name="aKey">Значение теста.</param>
		/// <typeparam name="T">Enum</typeparam>
		public static void SetDebugKey<T>(T aConfigId, string aKey) where T : System.Enum
		{
			if (_debugKeys.ContainsKey(aConfigId.ToString()))
			{
				_debugKeys[aConfigId.ToString()] = aKey;
			}
			else
			{
				_debugKeys.Add(aConfigId.ToString(), aKey);
			}

			Log($"Set debug key `{aKey}` for `{aConfigId}` test.");
		}

		/// <summary>
		/// Возвращает текущий ключ для указанного А/Б теста.
		/// </summary>
		/// <param name="aConfigId">Идентификатор конфига.</param>
		/// <typeparam name="T">Enum</typeparam>
		/// <returns>Текущий ключ для теста.</returns>
		public static string GetKey<T>(T aConfigId) where T : System.Enum
		{
			var currentTest = GetConfigItem(aConfigId);
			string decisionKey = currentTest.defaultValue;

#if GAMEANALYTICS
			if (GameAnalytics.IsRemoteConfigsReady())
			{
				decisionKey = GameAnalytics.GetRemoteConfigsValueAsString(
					aConfigId.ToString(),
					currentTest.defaultValue
				);
			}
#endif
			return (_debugKeys.ContainsKey(aConfigId.ToString()))
				? _debugKeys[aConfigId.ToString()]
				: decisionKey;
		}

		public static string GetConfig<T>(T aConfigId) where T : System.Enum
		{
			var config = GetConfigItem(aConfigId);
			if (config.kind != Kind.Config)
			{
				Debug.LogWarning($"[RemoteConfig] Requesting `{aConfigId.ToString()}` as config value but it's a `{config.kind}` value.");
			}

			string result = config.defaultValue;

#if GAMEANALYTICS
			if (GameAnalytics.IsRemoteConfigsReady())
			{
				return GameAnalytics.GetRemoteConfigsValueAsString(
					aConfigId.ToString(),
					result
				);
			}
#endif
			return (_debugKeys.ContainsKey(aConfigId.ToString()))
				? _debugKeys[aConfigId.ToString()]
				: result;
		}

		/// <summary>
		/// Возвращает экземпляр конфига из списка.
		/// </summary>
		/// <param name="aConfigId">Идентификатор конфига.</param>
		/// <typeparam name="T">Enum</typeparam>
		/// <returns>Экземпляр теста в списке тестов.</returns>
		public static ConfigItem GetConfigItem<T>(T aConfigId) where T : System.Enum
		{
			if (!IsInitialized)
			{
				return default(ConfigItem);
			}

			var currentTestName = aConfigId.ToString();
			var index = System.Array.FindIndex(_current.configs, x => x.name.Equals(currentTestName));
			if (index >= 0 && index < _current.configs.Length)
			{
				return _current.configs[index];
			}

			Debug.LogError($"[RemoteConfig] Can't find `{aConfigId}` test! ({index})");
			return default(ConfigItem);
		}

		/// <summary>
		/// Возвращает список всех доступных ключей для А/Б теста.
		/// </summary>
		/// <param name="aConfigId">Идентификатор конфига.</param>
		/// <typeparam name="T">Enum</typeparam>
		/// <returns>Массив ключей.</returns>
		public static string[] GetKeys<T>(T aConfigId) where T : System.Enum
		{
			if (!IsInitialized)
			{
				return null;
			}

			var config = GetConfigItem(aConfigId);
			var result = new string[config.decisions.Length];
			for (int i = 0, n = result.Length; i < n; i++)
			{
				result[i] = config.decisions[i].key;
			}

			return result;
		}

		/// <summary>
		/// Возвращает текущее решение для А/Б теста.
		/// </summary>
		/// <param name="aConfigId">Идентификатор конфига.</param>
		/// <typeparam name="T">Enum</typeparam>
		/// <returns>Экземпляр теста в списке тестов.</returns>
		public static Decision GetDecision<T>(T aConfigId) where T : System.Enum
		{
			if (!IsInitialized)
			{
				return null;
			}

			var config = GetConfigItem(aConfigId);
			if (config.kind != Kind.ABTest)
			{
				Debug.LogWarning($"[RemoteConfig] Requesting `{aConfigId.ToString()}` as ABTest value but it's a `{config.kind}` value.");
				return null;
			}

			string decisionKey = config.defaultValue;

#if GAMEANALYTICS
			if (GameAnalytics.IsRemoteConfigsReady())
			{
				decisionKey = GameAnalytics.GetRemoteConfigsValueAsString(
					aConfigId.ToString(),
					config.defaultValue
				);
			}
#endif
			if (_debugKeys.ContainsKey(aConfigId.ToString()))
			{
				decisionKey = _debugKeys[aConfigId.ToString()];
				Log($"Select decision `{decisionKey}` by debug key.");
			}
			else
			{
				Log($"Select decision `{decisionKey}`.");
			}

			// Select decision.
			// ----------------
			var decisionIndex = System.Array.FindIndex(config.decisions, x => x.key.Equals(decisionKey));
			if (decisionIndex >= 0 && decisionIndex < config.decisions.Length)
			{
				return config.decisions[decisionIndex].decision;
			}

			// If previous decision not found, select default decision.
			// --------------------------------------------------------
			Debug.LogWarning($"[RemoteConfig] Can't find `{decisionKey}` decision! Try to select default decision.");
			decisionKey = config.defaultValue;
			decisionIndex = System.Array.FindIndex(config.decisions, x => x.key.Equals(decisionKey));
			if (decisionIndex >= 0 && decisionIndex < config.decisions.Length)
			{
				Log($"[RemoteConfig] Selected default `{decisionKey}` decision.");
				return config.decisions[decisionIndex].decision;
			}

			return null;
		}

		#endregion
		#region Private Methods

		private static void Log(object aValue)
		{
			if (_isDebugLog)
			{
				Debug.Log($"[RemoteConfig] {aValue.ToString()}");
			}
		}

		#endregion
		#region Event Handlers

		private void RemoteConfigUpdatedHandler()
		{
			Log("Got configuration from GA.");

#if GAMEANALYTICS
			string json = GameAnalytics.GetRemoteConfigsContentAsString();
			Log($"JSON: {json}");
#endif
			EventConfigUpdate?.Invoke();
			_configUpdateCallback?.Invoke();
			_configUpdateCallback = null;
		}

		#endregion
	}
}