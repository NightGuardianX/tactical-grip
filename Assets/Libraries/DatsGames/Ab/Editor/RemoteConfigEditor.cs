namespace DatsGames
{
	using UnityEngine;
	using UnityEditor;

	[CustomEditor(typeof(RemoteConfig))]
	public class RemoteConfigEditor : Editor
	{
		private RemoteConfig _self;

		#region Unity Calls

		private void OnEnable()
		{
			_self = (RemoteConfig) target;
		}

		public override void OnInspectorGUI()
		{
			base.OnInspectorGUI();

			DrawHeaderComponent();

#if !GAMEANALYTICS 
			EditorGUILayout.HelpBox("GameAnalytics is not activated yet!", MessageType.Warning);
#endif
			EditorGUI.BeginChangeCheck();

			_self.debugLog = EditorGUILayout.Toggle("Debug Log", _self.debugLog);
			if (_self.debugLog)
			{
				EditorGUILayout.HelpBox("Don't forget disable debugLog in the production version of the game.", MessageType.Warning);
			}

			EditorGUILayout.BeginVertical();
			{
				int delIndex = -1;
				for (int i = 0, n = _self.configs.Length; i < n; i++)
				{
					RemoteConfig.ConfigItem item = _self.configs[i];
					EditorGUILayout.BeginVertical(EditorStyles.helpBox);

					EditorGUI.indentLevel++;
					item.isOpened = EditorGUILayout.Foldout(item.isOpened, $"{item.kind.ToString()}: {item.name}", true);
					if (item.isOpened)
					{
						item.kind = (RemoteConfig.Kind) EditorGUILayout.EnumPopup("Kind", item.kind);

						var c = GUI.color;
						if (item.name.Length > 12)
						{
							GUI.color = Color.red;
						}
						item.name = EditorGUILayout.TextField("Name", item.name);
						GUI.color = c;

						if (item.name.Length > 12)
						{
							EditorGUILayout.HelpBox("The maximum length of Name is 12 characters.", MessageType.Error);
						}

						switch (item.kind)
						{
							case RemoteConfig.Kind.ABTest :
								item = DrawAsABTestItem(item);
								break;
							
							case RemoteConfig.Kind.Config :
								item = DrawAsConfigItem(item);
								break;
						}

						if (GUILayout.Button("Remove Item"))
						{
							delIndex = i;
						}
					}
					EditorGUI.indentLevel--;
					EditorGUILayout.EndVertical();

					_self.configs[i] = item;

					if (delIndex >= 0)
					{
						if (EditorUtility.DisplayDialog(
							"Remove Item?",
							$"Really want to remove `{_self.configs[delIndex].name}`?",
							"Yes",
							"No"))
						{
							ArrayEx.RemoveAt(ref _self.configs, delIndex);
						}
					}
				}
			}
			EditorGUILayout.EndVertical();

			DrawBottomButtons();

			if (EditorGUI.EndChangeCheck())
			{
				EditorUtility.SetDirty(target);
			}
		}

		#endregion
		#region Private Methods

		private void DrawHeaderComponent()
		{
			EditorGUI.indentLevel++;
			EditorGUILayout.Space();
			
			GUILayout.BeginHorizontal();
			
			GUILayout.Label("Remote Config", EditorStyles.largeLabel);
			
			if (GUILayout.Button("Help", GUILayout.MaxWidth(60)))
			{
				Application.OpenURL("https://gitlab.com/game-delta/wikiguidelines/-/wikis/");
			}
			
			GUILayout.EndHorizontal();
			
			GUILayout.BeginHorizontal();
			GUILayout.Label("Implementation of Game Analytics remote config.", EditorStyles.miniLabel);
			GUILayout.EndHorizontal();
			EditorGUI.indentLevel--;

			EditorGUILayout.Space();
		}
		
		private void DrawBottomButtons()
		{
			GUILayout.Space(10.0f);
			EditorGUILayout.BeginHorizontal();
			{
				if (GUILayout.Button("Add Config"))
				{
					ArrayEx.Add(ref _self.configs, new RemoteConfig.ConfigItem
					{
						name = "<Unnamed>",
						defaultValue = "None",
						decisions = new RemoteConfig.DecisionItem[0]
					});
				}

				if (GUILayout.Button("Copy Configs as Enum"))
				{
					var str = "public enum AbTestId\n{\n";
					for (int i = 0, n = _self.configs.Length; i < n; i++)
					{
						str = string.Concat(str, $"	{_self.configs[i].name}");
						str = (i + 1 == _self.configs.Length)
							? string.Concat(str, "\n}")
							: string.Concat(str, ",\n");
					}

					var te = new TextEditor();
					te.text = str;
					te.SelectAll();
					te.Copy();
					Debug.Log("All configs copied to the clipboard as Enum.");
				}
			}
			EditorGUILayout.EndHorizontal();
		}

		private RemoteConfig.ConfigItem DrawAsABTestItem(RemoteConfig.ConfigItem aItem)
		{
			var decisionNames = new string[aItem.decisions.Length];
			for (int j = 0, jn = aItem.decisions.Length; j < jn; j++)
			{
				decisionNames[j] = aItem.decisions[j].key;
			}

			if (decisionNames.Length > 0)
			{
				int decisionIndex = System.Array.FindIndex(decisionNames, x => x.Equals(aItem.defaultValue));
				decisionIndex = (decisionIndex == -1) ? 0 : decisionIndex;
				decisionIndex = EditorGUILayout.Popup("Default Decision", decisionIndex, decisionNames);
				aItem.defaultValue = decisionNames[decisionIndex];
			}

			GUILayout.Space(10.0f);
			EditorGUILayout.BeginHorizontal();
			{
				EditorGUILayout.LabelField("List of Decisions", EditorStyles.boldLabel);
				if (GUILayout.Button("", "OL Plus", GUILayout.MaxWidth(16.0f), GUILayout.MaxHeight(16.0f)))
				{
					ArrayEx.Add(ref aItem.decisions, new RemoteConfig.DecisionItem
					{
						key = "<Unnamed>",
						decision = null
					});
				}
			}
			EditorGUILayout.EndHorizontal();

			if (aItem.decisions.Length == 0)
			{
				EditorGUILayout.HelpBox("Requered minimum one decision per A/B test!", MessageType.Warning);
				return aItem;
			}

			var decisions = _self.GetComponents<Decision>();
			var decisionsNames = new string[decisions.Length + 1];
			decisionsNames[0] = "None";
			for (int i = 0, n = decisions.Length; i < n; i++)
			{
				decisionsNames[i + 1] = decisions[i].GetType().Name;
			}

			bool keyWarning = false;
			int delIndex = -1;
			Color c = GUI.color;
			RemoteConfig.DecisionItem decision;
			for (int j = 0, jn = aItem.decisions.Length; j < jn; j++)
			{
				EditorGUILayout.BeginHorizontal();
				{
					decision = aItem.decisions[j];
					if (decision.key.Length > 16)
					{
						GUI.color = Color.red;
						keyWarning = true;
					}

					decision.key = EditorGUILayout.TextField(decision.key);

					int selIndex = 0;
					if (decision.decision != null)
					{
						selIndex = System.Array.IndexOf(decisionsNames, decision.decision.GetType().Name);
					}

					selIndex = EditorGUILayout.Popup(selIndex, decisionsNames);
					decision.decision = (selIndex != 0)
						? decisions[selIndex - 1]
						: null;

					if (GUILayout.Button("", "OL Minus", GUILayout.MaxWidth(16.0f), GUILayout.MaxHeight(16.0f)))
					{
						delIndex = j;
					}

					aItem.decisions[j] = decision;
					GUI.color = c;
				}
				EditorGUILayout.EndHorizontal();

				if (keyWarning)
				{
					EditorGUILayout.HelpBox("The maximum value name is 16 characters.", MessageType.Error);
					keyWarning = false;
				}
			}

			if (delIndex >= 0)
			{
				if (EditorUtility.DisplayDialog(
					"Remove Decision?",
					$"Really want to remove `{aItem.decisions[delIndex].key}` decision?",
					"Yes",
					"No"))
				{
					ArrayEx.RemoveAt(ref aItem.decisions, delIndex);
				}
			}

			GUILayout.Space(10.0f);
			return aItem;
		}

		private RemoteConfig.ConfigItem DrawAsConfigItem(RemoteConfig.ConfigItem aItem)
		{
			var c = GUI.color;
			if (aItem.defaultValue.Length > 16)
			{
				GUI.color = Color.red;
			}

			aItem.defaultValue = EditorGUILayout.TextField("Default Value", aItem.defaultValue);
			GUI.color = c;
			return aItem;
		}
		
		#endregion
	}
}