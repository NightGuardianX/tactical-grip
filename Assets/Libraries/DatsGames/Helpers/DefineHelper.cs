#if UNITY_EDITOR
namespace DatsGames
{
	using System.Collections.Generic;
	using UnityEditor;

	public static class DefineHelper
	{
		public static bool IsExists(string aDefine)
		{
			var defines = GetDefinesList(mobileBuildTargetGroups[0]);
			return defines.Contains(aDefine);
		}
		
		public static void SetEnabled(string aDefineName, bool aEnable, bool aMobile)
		{
			//A.Log($"Setting {defineName} to {enable}");
			foreach (var group in aMobile ? mobileBuildTargetGroups : buildTargetGroups)
			{
				var defines = GetDefinesList(group);
				if (aEnable)
				{
					if (defines.Contains(aDefineName))
					{
						return;
					}
					defines.Add(aDefineName);
				}
				else
				{
					if (!defines.Contains(aDefineName))
					{
						return;
					}
					while (defines.Contains(aDefineName))
					{
						defines.Remove(aDefineName);
					}
				}
				string definesString = string.Join(";", defines.ToArray());
				PlayerSettings.SetScriptingDefineSymbolsForGroup(group, definesString);
			}
		}

		public static List<string> GetDefinesList(BuildTargetGroup group)
		{
			return new List<string>(PlayerSettings.GetScriptingDefineSymbolsForGroup(group).Split(';'));
		}

		public static BuildTargetGroup[] buildTargetGroups = new BuildTargetGroup[]
		{
			BuildTargetGroup.Standalone,
			BuildTargetGroup.Android,
			BuildTargetGroup.iOS
		};

		public static BuildTargetGroup[] mobileBuildTargetGroups = new BuildTargetGroup[]
		{
			BuildTargetGroup.Android,
			BuildTargetGroup.iOS,
			BuildTargetGroup.WSA 
		};
	}
}
#endif