namespace DatsGames
{
	using System;
	using UnityEngine;

	[CreateAssetMenuAttribute(fileName = "LevelsPreset", menuName = "DatsGames/Levels Preset", order = 0)]
	public class LevelsPreset : ScriptableObject
	{
		public LevelItem[] levels = new LevelItem[0];

		[Serializable]
		public struct LevelItem
		{
			public string sceneName;
			public bool isAllowForRandomSelection;
		}
	}
}