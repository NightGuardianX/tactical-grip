namespace DatsGames
{
	using System;
	using System.Reflection;
	using System.Collections.Generic;
	using Anthill.Core;

	public class Menu : AntScenario
	{
		private readonly List<IController> _controllers;

		public Menu() : base("Menu")
		{
			_controllers = new List<IController>();
		}

		#region Public Methods

		public T Create<T>()
		{
			ConstructorInfo constructor = typeof(T).GetConstructor(Type.EmptyTypes);
			var controller = (IController) constructor.Invoke(null);
			_controllers.Add(controller);

			var system = (ISystem) controller;
			if (system != null)
			{
				Add(system);
			}
			else
			{
				A.Warning($"Controller {controller.GetType().ToString()} have no implemented ISystem interface!");
			}

			return (T) controller;
		}

		public T GetController<T>()
		{
			int index = _controllers.FindIndex(x => x is T);
			if (index >= 0 && index < _controllers.Count)
			{
				return (T) _controllers[index];
			}
			return default(T);
		}

		public bool ContainsController<T>()
		{
			int index = _controllers.FindIndex(x => x is T);
			return (index >= 0 && index < _controllers.Count);
		}

		public void DestroyController<T>()
		{
			int index = _controllers.FindIndex(x => x is T);
			if (index >= 0 && index < _controllers.Count)
			{
				var system = (ISystem) _controllers[index];
				if (system != null)
				{
					Remove(system);
				}

				// _controllers[index].Destroy();
				_controllers.RemoveAt(index);
			}
		}

		#endregion
	}
}