namespace DatsGames
{
	using UnityEngine;
	using Anthill.Prefs;
	
	/// <summary>
	/// Это модель данных для базового сохранения. Модель данных представляет собой набор полей
	/// которые могут быть сохранены или загружены из PlayerPrefs.
	/// 
	/// Для загрузки/сохранения данных используйте класс SaveBoxLoader.
	/// 
	/// Вы можете добавить сюда любое количество необходимых дополнительных полей. Пометьте поля
	/// аттрибутом PlayerPrefs чтобы они сериализовались в PlayerPrefs.
	/// </summary>
	public class SaveBox
	{
		[PlayerPrefs("levelIndex")]
		public int levelIndex = 0;

		[PlayerPrefs("displayLevelIndex")]
		public int displayLevelIndex = 1;

		[PlayerPrefs("coins")]
		public int coins = 0;
	}
}