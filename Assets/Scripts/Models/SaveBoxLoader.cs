namespace DatsGames
{
	using UnityEngine;
	using Anthill.Prefs;

	/// <summary>
	/// Данный класс реализует загрузку и сохранение данных для модели SaveBox.
	/// </summary>
	public static class SaveBoxLoader
	{
		/// <summary>
		/// Создает модель данных для сохранения и загружает данные в нее.
		/// </summary>
		/// <param name="aSaveBox">Модель для загрузки, если не указано, то будет создана новая.</param>
		/// <returns>Загруженная или созданная модель данных.</returns>
		public static SaveBox Load(SaveBox aSaveBox = null)
		{
			if (aSaveBox == null)
			{
				aSaveBox = new SaveBox();
			}

			AntPlayerPrefs.Deserialize<SaveBox>(aSaveBox);
			return aSaveBox;
		}

		/// <summary>
		/// Записывает модель данных в сохранение.
		/// </summary>
		/// <param name="aSaveBox"></param>
		public static void Write(SaveBox aSaveBox)
		{
			AntPlayerPrefs.Serialize<SaveBox>(aSaveBox);
			PlayerPrefs.Save();
		}
	}
}