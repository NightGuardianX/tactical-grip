using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ServiceOfReturnShots 
{
    private ShootService _shootService;

    public ServiceOfReturnShots(ShootService shootService)
    {
        _shootService = shootService;
    }

    public void SetTargetThatShootsAtMe(Character targetWhoShootInMe)
    {
        _shootService.ShootInTarget(targetWhoShootInMe);
    }
}
