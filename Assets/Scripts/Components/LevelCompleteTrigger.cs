namespace DatsGames
{
	using UnityEngine;
	using Anthill.Core;
	
	public class LevelCompleteTrigger : MonoBehaviour
	{
	#region Unity Calls
	
		private void OnTriggerEnter(Collider aOther)
		{
			// Выключаем инпут.
			AntEngine.Get<Menu>()
				.GetController<TouchStickController>()
				.SetEnable(false)
				.SetVisible(false)
				.Reset();

			// Скрываем внутриигровой UI.
			AntEngine.Get<Menu>()
				.GetController<InGameController>()
				.ShowRestartButton(false)
				.ShowProgressBar(false);

			AntEngine.Get<Menu>()
				.GetController<HealthBarController>()
				.Hide();

			// Отображаем окно дебрифинга.
			// @info окно отображаем с задержкой чтобы анимации не накладывались друг на друга.
			AntDelayed.Call(0.25f, () =>
			{
				AntEngine.Get<Menu>()
					.GetController<DebriefingController>()
					.Show();
			});
		}
	
	#endregion
	}
}