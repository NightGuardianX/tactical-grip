using System.Collections;
using UnityEngine;
public class Bullet : MonoBehaviour 
{
    public delegate void BulletDelegate();
    public event BulletDelegate HitEvent;
    private float _speedMoveBullet;

    public void Init(float speedMoveBullet)
    {
        _speedMoveBullet = speedMoveBullet;
    }

    public void MoveToTarget(Transform target)
    {
        StartCoroutine(Move(target));
    }

    private IEnumerator Move(Transform target)
    {
        while (Vector3.Distance(transform.position, target.position) > 0f)
        {
            transform.position = Vector3.MoveTowards(transform.position, 
                target.position, _speedMoveBullet * Time.fixedDeltaTime);
            yield return null;
        }
        HitEvent?.Invoke();
        Destroy(gameObject);
    }
}
