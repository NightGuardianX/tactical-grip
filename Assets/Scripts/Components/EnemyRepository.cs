using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyRepository
{
    private static List<Enemy> _enemies = new List<Enemy>();

    public void Add(Enemy enemy)
    {
        if (!_enemies.Contains(enemy))
        {
            _enemies.Add(enemy);
        }
    }
    public void Remove(Enemy enemy)
    {
        if (_enemies.Contains(enemy))
        {
            _enemies.Remove(enemy);
        }
    }
    public void Clear()
    {
        _enemies.Clear();
    }
    public void NotificateAllAboutStartGame()
    {
        foreach (var item in _enemies)
        {
            item.StartGame();
        }
    }
}
