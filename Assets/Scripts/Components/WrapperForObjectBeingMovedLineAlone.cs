using Canvas;
using UnityEngine;

[RequireComponent(typeof(ObjectBeingMovedAloneLine))]
public class WrapperForObjectBeingMovedLineAlone : MonoBehaviour
{
    private ObjectBeingMovedAloneLine _objectBeingMovedAloneLine;
    private WrapperRepository _wrapperRepository = new WrapperRepository();
    private AnimationHandler _animationHandler;
    private LossOrVictoryVerificationService _lossOrVictoryVerificationService = new LossOrVictoryVerificationService();
    public void Init(AnimationHandler animationHandler)
    {
        _animationHandler = animationHandler;
    }

    private void Start()
    {
        _objectBeingMovedAloneLine = GetComponent<ObjectBeingMovedAloneLine>();
        _objectBeingMovedAloneLine.Init(this);
        _wrapperRepository.Add(this);
    }

    public void StartMove() 
    {
        _objectBeingMovedAloneLine.CurrentState = CarObjectStates.Moving;
        gameObject.layer = 7;
        _animationHandler.PlayWalkAnimation();
    }

    public void StopMove() 
    {
        _objectBeingMovedAloneLine.CurrentState = CarObjectStates.Finished;
    }

    public void ReadyToMove()
    {
        _objectBeingMovedAloneLine.CurrentState = CarObjectStates.Ready;
        _wrapperRepository.CheckEveryoneReadyStartMoving();
        gameObject.layer = 2;
        
    }
    public void LockToTarget(Transform target)
    {
        transform.LookAt(target);
    }
    public CarObjectStates GetCurrentState()
    {
        return _objectBeingMovedAloneLine.CurrentState;
    }
    public void Finished()
    {
        _animationHandler.PlayIdleAnimation();
        _objectBeingMovedAloneLine.CurrentState = CarObjectStates.Finished;
    }
    
}
