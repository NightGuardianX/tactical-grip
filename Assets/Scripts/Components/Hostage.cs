using UnityEngine;

public class Hostage : MonoBehaviour
{
    public delegate void HostageDelegate();
    public event HostageDelegate FindEvent;
    private SpecialForces _specialForces;
    private float _radius;
    private LayerMask _targetMask;
    private HostageRepository _hostageRepository;
    public SpecialForces SpecialForces { get => _specialForces; private set => _specialForces = value; }
    public void CheckSpecialForces()
    {
        var range = Physics.OverlapSphere(transform.position, _radius, _targetMask);
        if (range.Length != 0)
        {
            _specialForces = range[0].gameObject.GetComponent<SpecialForces>();
            if (_specialForces != null)
            {
                FindEvent?.Invoke();
            }
        }
    }
    private void OnDrawGizmos()
    {
        Gizmos.color = new Color(1f, 0f, 0, 0.5f);
        Gizmos.DrawSphere(transform.position, _radius);
    }

    public void Init(float radius, LayerMask targetMask)
    {
        _radius = radius;
        _targetMask = targetMask;
    }
    private void OnDisable()
    {
        if (_hostageRepository != null)
        {
            _hostageRepository.Remove(this);
        }
    }

}
