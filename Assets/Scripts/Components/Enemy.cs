using DatsGames;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Enemy : Character 
{
    private EnemyRepository _enemyRepository = new EnemyRepository();
    [SerializeField] private Material _whiteMaterial;
    [SerializeField] private SkinnedMeshRenderer _skinnedMeshRendererBody;
    [SerializeField] private SkinnedMeshRenderer _skinnedMeshRendererClothes;
    [SerializeField] private SkinnedMeshRenderer _skinnedMeshRendererShoes;
    [SerializeField] private Material[] _originalBodyMaterials;
    [SerializeField] private Material _originalClothesMaterial;
    [SerializeField] private Material _originalShoesMaterial;
    public void LockTarget(Transform target)
    {
        transform.LookAt(target);
        _animationHandler.PlayFiringAnimation();
    }
    protected override void Init()
    {
        base.Init();
        _healthBarIndex = _healthBarController.RequestBar(HealthBarKind.Enemy);
        _healthBarController.SetMaxValue(_healthBarIndex, _healthPoints);
        _lossOrVictoryVerificationService.Add(this);
        DieEvent += () => _lossOrVictoryVerificationService.Remove(this);
        gameObject.layer = 2;
        _enemyRepository.Add(this);
    }
    public void StartGame()
    {
        gameObject.layer = 1;
    }
    public override void TakeDamage(Weapon weapon)
    {
        base.TakeDamage(weapon);
        StartCoroutine(ChangeMaterial());
    }

    private IEnumerator ChangeMaterial()
    {
        _skinnedMeshRendererBody.materials = new Material[2]{ _whiteMaterial,_whiteMaterial};
        _skinnedMeshRendererClothes.material = new Material(_whiteMaterial);
        _skinnedMeshRendererShoes.material = new Material(_whiteMaterial);
        yield return new WaitForSeconds(0.2f);
        _skinnedMeshRendererBody.materials = new Material[2] { _originalBodyMaterials[0], _originalBodyMaterials[1] };
        _skinnedMeshRendererClothes.material = new Material(_originalClothesMaterial);
        _skinnedMeshRendererShoes.material = new Material(_originalShoesMaterial);
    }
    public void ActionAfterTargetDie()
    {
        _animationHandler.PlayIdleAnimation();
    }
}
