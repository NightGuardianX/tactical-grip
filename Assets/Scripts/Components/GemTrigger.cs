namespace DatsGames
{
	using UnityEngine;
	using Anthill.Core;
	
	public class GemTrigger : MonoBehaviour
	{
	#region Unity Calls
	
		private void OnTriggerEnter(Collider aOther)
		{
			AntEngine.Get<Menu>()
				.GetController<FlyingLabelsController>()
				.SpawnLabel(1, aOther.transform.position, LabelKind.Coin, FlyingLabelsController.ValueMode.Add);

			gameObject.SetActive(false);
		}
	
	#endregion
	}
}