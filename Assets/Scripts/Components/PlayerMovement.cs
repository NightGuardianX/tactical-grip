namespace DatsGames
{
	using UnityEngine;
	using Anthill.Core;
	using Anthill.Utils;
	
	public class PlayerMovement : MonoBehaviour
	{
		public float movementSpeed = 2.8f;
		public Vector3 healthBarOffset = new Vector3(0.0f, 0.75f, 0.0f);
		public Transform target;

		private Transform _t;
		private TouchStickController _controller;
		private Rigidbody _body;

		private float _totalDist;
		private InGameController _ingame;

		private HealthBarController _healthBarController;
		private int _healthBarIndex;

		public float Health { get; set; } = 100.0f;
	
	#region Unity Calls
	
		private void Awake()
		{
			_t = GetComponent<Transform>();
			_body = GetComponent<Rigidbody>();
		}
	
		private void Start()
		{
			// Получаем референс на инпут.
			_controller = AntEngine.Get<Menu>()
				.GetController<TouchStickController>();

			_totalDist = Vector3.Distance(_t.position, target.position);

			_ingame = AntEngine.Get<Menu>()
				.GetController<InGameController>();

			// Получаем референс на HealthBarController.
			_healthBarController = AntEngine.Get<Menu>()
				.GetController<HealthBarController>();
			
			// Добавляем для себя HealthBar в контроллере.
			_healthBarIndex = _healthBarController.RequestBar(HealthBarKind.Player);
		}

		private void OnDestroy()
		{
			// Освобождаем HealthBar если он более не нужен.
			_healthBarController.ReleaseBar(_healthBarIndex);
		}
	
		private void Update()
		{
			// Обновление HealthBar.
			_healthBarController.UpdateBarWorld(_healthBarIndex, Health / 100.0f, _t.position + healthBarOffset);

			// Двигаем сферу согласно пользовательскому импуту.
			var move = Camera.main.transform.rotation * new Vector3(_controller.Direction.x, 0.0f, _controller.Direction.y);
			move.y = 0.0f;
			_body.velocity = move * movementSpeed;

			// Прогресс уровня.
			_ingame.SetProgress(
				AntMath.TrimToRange(
					1.0f - Vector3.Distance(_t.position, target.position) / _totalDist,
					0.0f, 1.0f
				)
			);
		}
	
	#endregion
	}
}