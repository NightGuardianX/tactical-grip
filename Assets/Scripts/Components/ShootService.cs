using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Weapon))]
public class ShootService : MonoBehaviour
{
    private Weapon _weapon;
    private SpecialForces _specialForces;
    private Enemy _enemy;
    private ITargetForShoot _targetForShoot;
    private ShootServiceState _shootServiceState = ShootServiceState.StopService;
    private Character _myType;
    public void Init(Character character)
    {
        _myType = character;
        _weapon = GetComponent<Weapon>();
    }
    public void ShootInTarget(ITargetForShoot target)
    {
        if (_shootServiceState == ShootServiceState.StartService)
        {
            _targetForShoot = target;
            _targetForShoot.DieEvent += TargetDie;
            if (_targetForShoot is Enemy && _myType is SpecialForces)
            {
                _specialForces = GetComponent<SpecialForces>();
                _weapon.Shoot(_targetForShoot, _specialForces); 
                _specialForces.ActionAfterStartShootInTarget(_targetForShoot.GetTarget());
            }
            else if (_targetForShoot is SpecialForces && _myType is Enemy)
            {
                _enemy = GetComponent<Enemy>();
                _enemy.LockTarget(_targetForShoot.GetTarget());
                _weapon.Shoot(_targetForShoot , _enemy);
            }
        }
        else return;
    }

    public void TargetDie()
    {
        _weapon.StopShoot();
        if (_specialForces != null)
        {
            if (!_specialForces.IsDie)
            {
                _specialForces.WrapperForObjectBeingMovedLineAlone.StartMove();
                _specialForces = null;
            }
            else
            {
                _specialForces.WrapperForObjectBeingMovedLineAlone.StopMove();
            }
        }
        else if(_enemy!=null)
        {
            _enemy.ActionAfterTargetDie();
        }
    }

    public void StartService()
    {
        _shootServiceState = ShootServiceState.StartService;
    }

    public void StopService()
    {
        _shootServiceState = ShootServiceState.StopService;
        _weapon.StopShoot();
    }

}
public enum ShootServiceState
{
    StopService,
    StartService
}