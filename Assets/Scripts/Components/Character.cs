using Anthill.Core;
using DatsGames;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(FieldOfView), typeof(ShootService) , typeof(AnimationHandler))]
public abstract class Character : MonoBehaviour, ITargetForShoot
{
    [SerializeField] protected int _healthPoints;
    [SerializeField] private Vector3 _healthBarOffset;

    private FieldOfView _fieldOfView;
    private ShootService _shootService;
    protected AnimationHandler _animationHandler;
    protected HealthBarController _healthBarController;
    protected int _healthBarIndex;
    private Character _targetWhoShootInMe;
    private ServiceOfReturnShots _serviceOfReturnShots;
    protected LossOrVictoryVerificationService _lossOrVictoryVerificationService = new LossOrVictoryVerificationService();
    public ShootService ShootService { get => _shootService; private set => _shootService = value; }
    public event ITargetForShoot.TargetForShootDelegate DieEvent;
    public Transform GetTarget()
    {
        return transform;
    }
    public bool IsDie
    {
        get
        {
            if (_healthPoints > 0)
            {
                return false;
            }
            else
            {
                return true;
            }
        }
    }
    public virtual void TakeDamage(Weapon weapon)
    {
        _healthPoints -= weapon.Damage;
        if (_healthPoints <= 0)
        {
            DieEvent?.Invoke();
            DieEvent =null;
            _fieldOfView.StopFieldOfView();
            ShootService.StopService();
            _animationHandler.PlayDieAnimation();
            gameObject.layer = 2;
            _lossOrVictoryVerificationService.LossOrVictoryVerification();
        }
        if (_targetWhoShootInMe == null)
        {
            _targetWhoShootInMe = weapon.GetComponent<Character>();
            
            if (_targetWhoShootInMe.IsDie)
            {
                _targetWhoShootInMe = null;
                return;
            }
            else
            {
                _targetWhoShootInMe.DieEvent += () => _targetWhoShootInMe = null;
                _serviceOfReturnShots.SetTargetThatShootsAtMe(_targetWhoShootInMe);
            }
        }
    }

    protected virtual void Init()
    {
        _shootService = GetComponent<ShootService>();
        _fieldOfView = GetComponent<FieldOfView>();
        _animationHandler = GetComponent<AnimationHandler>();
        _fieldOfView.Init(this);
        _shootService.Init(this);
        _shootService.StartService();
        _serviceOfReturnShots = new ServiceOfReturnShots(_shootService);
        _healthBarController = AntEngine.Get<Menu>()
                    .GetController<HealthBarController>();
        _healthBarController.
            InitCamera(Camera.main);
    }

    private void OnDestroy()
    {
        _healthBarController.ReleaseBar(_healthBarIndex);
    }

    private void Update()
    {
        _healthBarController.UpdateBarWorld(_healthBarIndex, _healthPoints, transform.position + _healthBarOffset);
    }

    private void Start()
    {
        Init();
    }
    public void SetWhoShootInMe(Character character)
    {
        _targetWhoShootInMe = character;

    }
}

