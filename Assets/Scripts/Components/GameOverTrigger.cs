namespace DatsGames
{
	using UnityEngine;
	using Anthill.Core;
	
	public class GameOverTrigger : MonoBehaviour
	{
		#region Unity Calls
	
		private void OnTriggerEnter(Collider aOther)
		{
			var player = aOther.GetComponent<PlayerMovement>();
			if (player != null)
			{
				player.Health -= 25.0f;
				if (player.Health <= 0.0f)
				{
					AntEngine.Get<Menu>()
						.GetController<PopupNotifyController>()
						.SetDescription("Oops!")
						.Show();

					// Выключаем инпут.
					AntEngine.Get<Menu>()
						.GetController<TouchStickController>()
						.SetEnable(false)
						.SetVisible(false)
						.Reset();

					// Скрываем внутриигровой UI.
					AntEngine.Get<Menu>()
						.GetController<InGameController>()
						.ShowRestartButton(false)
						.ShowProgressBar(false);

					AntEngine.Get<Menu>()
						.GetController<HealthBarController>()
						.Hide();

					// Отображаем окно фейла.
					// @info окно отображаем с задержкой чтобы анимации не накладывались друг на друга.
					AntDelayed.Call(1.25f, () =>
					{
						AntEngine.Get<Menu>()
							.GetController<GameOverController>()
							.Show();
					});
				}
			}
		}
	
		#endregion
	}
}