public enum TrackId
{
    GameStarted,
    LevelStart,
    LevelCompleted,
    LevelFailed
}

