using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Animator))]
public class AnimationHandler : MonoBehaviour
{
    private Animator _animator;
    [SerializeField] private string _dyingAnimationKey;
    [SerializeField] private string _idleAnimationKey;
    [SerializeField] private string _walkAnimationKey;
    [SerializeField] private string _firingAnimationKey;
    [SerializeField] private int _countDieAnimations;
    void Start()
    {
        _animator = GetComponent<Animator>();
    }
    public void PlayDieAnimation() 
    { 
        if (_walkAnimationKey != null && _walkAnimationKey != "")
        {
            _animator.SetBool(_walkAnimationKey, false);
        }
        _animator.SetBool(_firingAnimationKey, false);
        _animator.SetInteger(_dyingAnimationKey, Random.Range(1, _countDieAnimations));
    }
    public void PlayIdleAnimation() 
    {
        if (_walkAnimationKey != null && _walkAnimationKey != "")
        {
            _animator.SetBool(_walkAnimationKey, false);
        }
        _animator.SetBool(_firingAnimationKey, false);
    }
    public void PlayWalkAnimation() 
    {
        if (_walkAnimationKey != null && _walkAnimationKey != "")
        {
            _animator.SetBool(_walkAnimationKey, true);
        }
        _animator.SetBool(_firingAnimationKey, false);
    }
    public void PlayFiringAnimation() 
    {
        _animator.SetBool(_firingAnimationKey , true);
        if (_walkAnimationKey != null && _walkAnimationKey != "")
        {
            _animator.SetBool(_walkAnimationKey, false);
        }
    }
}
