namespace DatsGames
{
	using UnityEngine;
	using Anthill.Utils;
	
	public class GemRotator : MonoBehaviour
	{
		[System.Serializable]
		public struct AnimItem
		{
			public float speed;
			public float amplitude;
			public float offset;
		}

		public AnimItem levitation = new AnimItem
		{
			speed = 0.0f,
			amplitude = 0.0f,
			offset = 1.0f
		};

		public AnimItem rotation = new AnimItem
		{
			speed = 0.0f,
			amplitude = 0.0f,
			offset = 1.0f
		};

		private Transform _t;
		private float _time;
		
	#region Unity Calls

		private void Awake()
		{
			_t = GetComponent<Transform>();
			_time = AntRandom.Range(0.0f, 5.5f);
		}
	
		private void Update()
		{
			_time += Time.deltaTime;

			var rot = _t.localRotation.eulerAngles;
			rot.y = rotation.offset + Mathf.Cos(_time * rotation.speed) * rotation.amplitude;
			_t.localRotation = Quaternion.Euler(rot.x, rot.y, rot.z);

			var pos = _t.localPosition;
			pos.y = levitation.offset + Mathf.Cos(_time * levitation.speed) * levitation.amplitude;
			_t.localPosition = pos;
		}
	
	#endregion
	}
}