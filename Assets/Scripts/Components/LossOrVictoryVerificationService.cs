using Anthill.Core;
using DatsGames;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class LossOrVictoryVerificationService
{
    private static List<Enemy> _enemyOnLevel = new List<Enemy>();
    private static List<SpecialForces> _specialForcesOnLevel = new List<SpecialForces>();
    private static LossOfVicroryVerificationServiceFlags _lossOfVicroryVerificationServiceFlags = 
        LossOfVicroryVerificationServiceFlags.HostageNotFinding;
    private static Game _game;

    public void InitGame(Game game)
    {
        _game = game;
    }


    public void Add(SpecialForces character)
    {
        if (_specialForcesOnLevel.Contains(character))
        {
            return;
        }
        else
        {
            _specialForcesOnLevel.Add(character);
        }
    }

    public void Add(Enemy character)
    {
        if (_enemyOnLevel.Contains(character))
        {
            return;
        }
        else
        {
            _enemyOnLevel.Add(character);
        }
    }

    public void Remove(Enemy character)
    {
        if (_enemyOnLevel.Contains(character))
        {
            _enemyOnLevel.Remove(character);
        }
        else
        {
            return;
        }
    }

    public void Remove(SpecialForces character)
    {
        if (_specialForcesOnLevel.Contains(character))
        {
            _specialForcesOnLevel.Remove(character);
        }
        else
        {
            return;
        }
    }

    public void Clear()
    {
        _specialForcesOnLevel.Clear();
        _enemyOnLevel.Clear();
    }

    public void FindHostage()
    {
        _lossOfVicroryVerificationServiceFlags = LossOfVicroryVerificationServiceFlags.HostageFinding;
        if (_enemyOnLevel.Count == 0 &&
             _lossOfVicroryVerificationServiceFlags == LossOfVicroryVerificationServiceFlags.HostageFinding)
        {
            CallWin();
        }
        else
        {
            CallLoss();
        }
    }
  
    public void LossOrVictoryVerification()
    {
        if(_specialForcesOnLevel.Count == 0)
        {
            CallLoss();
        }
    }

    private void CallWin()
    {
        foreach (var item in _specialForcesOnLevel)
        {
            item.WrapperForObjectBeingMovedLineAlone.Finished();
        }
        // �������� ������������� UI.
        AntEngine.Get<Menu>()
            .GetController<InGameController>()
            .ShowRestartButton(false)
            .ShowProgressBar(false);

        AntEngine.Get<Menu>()
            .GetController<HealthBarController>()
            .Hide();
        // @info ���� ���������� � ��������� ����� �������� �� ������������� ���� �� �����.
        AntDelayed.Call(0.45f, () =>
        {
            AntEngine.Get<Menu>()
                .GetController<DebriefingController>()
                .Show();
        });
        StatsWrapper.Track(TrackId.LevelCompleted, "level", _game.LevelManager.LastScene);
    }

    private void CallLoss()
    {
        foreach (var item in _specialForcesOnLevel)
        {
            item.WrapperForObjectBeingMovedLineAlone.Finished();
        }
        AntEngine.Get<Menu>()
                        .GetController<PopupNotifyController>()
                        .SetDescription("Oops!")
                        .Show();
        // �������� ������������� UI.
        AntEngine.Get<Menu>()
            .GetController<InGameController>()
            .ShowRestartButton(false)
            .ShowProgressBar(false);
        AntEngine.Get<Menu>()
            .GetController<HealthBarController>()
            .Hide();
        // ���������� ���� �����.
        // @info ���� ���������� � ��������� ����� �������� �� ������������� ���� �� �����.
        AntDelayed.Call(1.25f, () =>
        {
            AntEngine.Get<Menu>()
                .GetController<GameOverController>()
                .Show();
        });
        StatsWrapper.Track(TrackId.LevelFailed, "level", _game.LevelManager.LastScene);
    }
}

public enum LossOfVicroryVerificationServiceFlags
{
    HostageFinding,
    HostageNotFinding
}
