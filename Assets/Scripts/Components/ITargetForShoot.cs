using UnityEngine;

public interface ITargetForShoot 
{
    Transform GetTarget();
    void TakeDamage(Weapon weapon);
    delegate void TargetForShootDelegate();
    event TargetForShootDelegate DieEvent;
    void SetWhoShootInMe(Character character);
}
