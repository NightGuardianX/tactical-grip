using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Weapon : MonoBehaviour
{
    protected Bullet _bullet;
    protected float _shootDelay = 0.25f;
    [SerializeField]
    protected int _damage = 10;
    private ITargetForShoot _target;
    private WeaponState _weaponState = WeaponState.NoShooting;
    private BulletFactory _bulletFactory = new BulletFactory(25);
    private Character _whoShoot;
    public int Damage { get => _damage; private set => _damage = value; }
    public Character WhoShoot { get => _whoShoot; private set => _whoShoot = value; }

    public void Shoot(ITargetForShoot target ,Character whoShoot )
    {
        _target = target;
        _whoShoot = whoShoot;
        _weaponState = WeaponState.Shooting;
        StartCoroutine(ShotsFiredInBurstWithDelay());
    }
    public void StopShoot()
    {
        _weaponState = WeaponState.NoShooting;
    }
    private void ApplyDamage()
    {
        _bullet.HitEvent -= ApplyDamage;
        _target.TakeDamage(this);

    }
    private IEnumerator ShotsFiredInBurstWithDelay()
    {
        while (_weaponState != WeaponState.NoShooting)
        {
            _bullet = _bulletFactory.CreateBullet(transform);
            _bullet.MoveToTarget(_target.GetTarget());
            _bullet.HitEvent += ApplyDamage;
            yield return new WaitForSeconds(_shootDelay);
        }
    }
}
public enum WeaponState
{
    Shooting,
    NoShooting
}
