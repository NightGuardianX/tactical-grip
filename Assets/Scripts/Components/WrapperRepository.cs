using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;
using Anthill.Core;
using DatsGames;

public class WrapperRepository
{
    private static List<WrapperForObjectBeingMovedLineAlone> _wrapperForObjectBeingMovedLineAlones = 
        new List<WrapperForObjectBeingMovedLineAlone>();

    public void Add(WrapperForObjectBeingMovedLineAlone wrapperForObjectBeingMovedLine)
    {
        if (!_wrapperForObjectBeingMovedLineAlones.Contains(wrapperForObjectBeingMovedLine))
        {
            _wrapperForObjectBeingMovedLineAlones.Add(wrapperForObjectBeingMovedLine);
        }
    }
    public void Remove(WrapperForObjectBeingMovedLineAlone wrapperForObjectBeingMovedLine)
    {
        if (_wrapperForObjectBeingMovedLineAlones.Contains(wrapperForObjectBeingMovedLine))
        {
            _wrapperForObjectBeingMovedLineAlones.Remove(wrapperForObjectBeingMovedLine);
        }
    }
    public void NotificateAllAboutStartGame()
    {
        foreach (var item in _wrapperForObjectBeingMovedLineAlones)
        {
            item.StartMove();
        }
    }
    public void NotificatioAllAboutStopMove()
    {
        foreach (var item in _wrapperForObjectBeingMovedLineAlones)
        {
            item.StopMove();
        }
    }
    public void Clear()
    {
        _wrapperForObjectBeingMovedLineAlones.Clear();
    }
    public void CheckEveryoneReadyStartMoving()
    {
        if( _wrapperForObjectBeingMovedLineAlones.All
            (x=> x.GetCurrentState()== Canvas.CarObjectStates.Ready))
        {
            AntEngine.Get<Menu>().
             Get<StartGameController>().
             Show();
        }
    }
}
