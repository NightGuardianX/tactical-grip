using DatsGames;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(WrapperForObjectBeingMovedLineAlone))]
public class SpecialForces : Character
{
    private WrapperForObjectBeingMovedLineAlone _wrapperForObjectBeingMovedLineAlone;
    public WrapperForObjectBeingMovedLineAlone WrapperForObjectBeingMovedLineAlone
    { get => _wrapperForObjectBeingMovedLineAlone; private set => _wrapperForObjectBeingMovedLineAlone = value; }
    
    protected override void Init()
    {
        base.Init();
        _wrapperForObjectBeingMovedLineAlone = GetComponent<WrapperForObjectBeingMovedLineAlone>();
        _wrapperForObjectBeingMovedLineAlone.Init(_animationHandler);
        _healthBarIndex = _healthBarController.RequestBar(HealthBarKind.Player);
        _healthBarController.SetMaxValue(_healthBarIndex, _healthPoints);
        DieEvent += StopMove;
        _lossOrVictoryVerificationService.Add(this);
        DieEvent += () => _lossOrVictoryVerificationService.Remove(this);
    }
    public void ActionAfterStartShootInTarget(Transform target)
    {
        WrapperForObjectBeingMovedLineAlone.StopMove();
        WrapperForObjectBeingMovedLineAlone.LockToTarget(target);
        _animationHandler.PlayFiringAnimation();
    }
    
    private void StopMove()
    {
        _wrapperForObjectBeingMovedLineAlone.StopMove();
    }
}
