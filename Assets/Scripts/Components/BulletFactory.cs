using DatsGames;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BulletFactory 
{
    private Bullet _bullet;
    private readonly string _bulletPath = "GameObjects/Bullet/Bullet";
    private float _bulletSpeed;

    public BulletFactory(float bulletSpeed)
    {
        _bulletSpeed = bulletSpeed;
    }

    public Bullet CreateBullet(Transform transform)
    {
        Bullet bullet = ObjectFactory.Create<Bullet>(_bulletPath , transform.position + Vector3.up);
        bullet.Init(_bulletSpeed);
        return bullet;
    }
}
