﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FieldOfView : MonoBehaviour
{
	[Header("Количество лучей для поиска")]
	[SerializeField] private int _rays;
	[Min(1)]
	[Header("Дистанция лучей для поиска")]
	[SerializeField] private float _distance;
	[Header("Угол обзора для поиска")]
	[SerializeField] private float _angle;
	[Header("Угол смещения лучей для поиска")]
	[SerializeField] private Vector3 _offset;
	private Character _character;
	private ITargetForShoot _targetForShoot;
	public void Init(Character character)
    {
		_character = character;
    }

	public void StopFieldOfView()
	{
		_rays = 0;
		_distance = 0;
		_angle = 0;
		_offset = Vector3.zero;
	}

	ITargetForShoot GetRaycast(Vector3 dir)
	{
		RaycastHit hit = new RaycastHit();
		Vector3 pos = transform.position + _offset;
		if (Physics.Raycast(pos, dir, out hit, _distance))
		{
			var hitObject =	hit.collider.GetComponent<ITargetForShoot>();
			if (hitObject!=null && hitObject is Enemy && _character is SpecialForces)
			{
				Debug.DrawLine(pos, hit.point, Color.green);
				return hitObject;
			}else if(hitObject != null && hitObject is SpecialForces && _character is Enemy)
            {
				Debug.DrawLine(pos, hit.point, Color.black);
				return hitObject;
			}
			else
			{
				Debug.DrawLine(pos, hit.point, Color.red);
				return null;
			}
		}
		else
		{
			Debug.DrawRay(pos, dir * _distance, Color.red);
			return null;
		}
	}

	private void RayToScan()
	{
		Vector3 dir;
		float j = Vector3.forward.z * -0.5f;
		for (int i = 0; i < _rays; i++)
		{
			var x = Mathf.Sin(j);
			var y = Mathf.Cos(j);
			j += _angle * Mathf.Deg2Rad / _rays;
			if (x != 0)
			{
				dir = transform.TransformDirection(new Vector3(-x, 0, y));
			}
			else
			{
				dir = transform.TransformDirection(new Vector3(x, 0, y));
			}
			_targetForShoot = GetRaycast(dir);
            if (_targetForShoot != null)
            {
				_character.ShootService.ShootInTarget(_targetForShoot);
                _targetForShoot.DieEvent += RestartFieldOffView;
				break;
            }
		}
	}
    private void RestartFieldOffView()
    {
		_targetForShoot = null;

	}
    void Update()
	{
		if (_targetForShoot == null)
		{
			RayToScan();
        }
        else
        {
			return;
        }
	}
}
