using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HostageRepository : MonoBehaviour
{
    [SerializeField] private List<Hostage> _hostages = new List<Hostage>();
    private HostageRepoitoryState _hostageRepoitoryState = HostageRepoitoryState.Finding;
    [SerializeField]
    private float _radius;
    [SerializeField]
    private LayerMask _targetMask;
    private LossOrVictoryVerificationService _lossOrVictoryVerificationService = new LossOrVictoryVerificationService();

    private void Start()
    {
        foreach (var item in _hostages)
        {
            item.Init(_radius, _targetMask);
            item.FindEvent += FindingHostage;
        }   
    }

    public void Remove(Hostage hostage)
    {
        if (_hostages.Contains(hostage))
        {
            _hostages.Remove(hostage);
            hostage.FindEvent -= FindingHostage;
        }
    }

    private void FixedUpdate()
    {
        if (_hostageRepoitoryState == HostageRepoitoryState.Finding)
        {
            foreach (var item in _hostages)
            {
                item.CheckSpecialForces();
            }
        }
    }

    public void FindingHostage()
    {
        _hostageRepoitoryState = HostageRepoitoryState.StopFinding;
        _lossOrVictoryVerificationService.FindHostage();
    }
}
public enum HostageRepoitoryState
{
    Finding,
    StopFinding
}
