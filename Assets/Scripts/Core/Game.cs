﻿namespace DatsGames
{
	using UnityEngine;
	using Anthill.Core;
	using Anthill.Inject;

	/// <summary>
	/// Данный класс следует запускать первым при инициализации игры.
	/// Настройку порядка инициализации скриптов можно найти в Player Settings ▶ Scripts Execution Order.
	/// </summary>
	public class Game : AntAbstractBootstrapper
	{
	#region Getters / Setters

		public UIRoot UiRoot { get; private set; }
		public UICamera UiCamera { get; private set; }
		public LevelManager LevelManager { get; private set; }

		private LossOrVictoryVerificationService _lossOrVictoryVerificationService = new LossOrVictoryVerificationService();

	#endregion

	#region AntAbstractBootstrapper Implementation

		/// <summary>
		/// Данный метод вызывается в Awake базового класса Game, здесь необходимо получить доступ
		/// ко всем менеджерам и зарегистрировать их в DiContainer чтобы можно было получать к ним
		/// доступ в любом другом месте.
		/// </summary>
		public override void Configure(IInjectContainer aContainer)
		{
			aContainer.RegisterSingleton(this);

			UiRoot = GameObject.FindObjectOfType<UIRoot>();
			aContainer.RegisterSingleton(UiRoot);

			UiCamera = GameObject.FindObjectOfType<UICamera>();
			aContainer.RegisterSingleton(UiCamera);

			// Менеджер уровней.
			LevelManager = GetComponent<LevelManager>();
			aContainer.RegisterSingleton(LevelManager);

			// Помошник для отслеживания изменений разрешения экрана.
			aContainer.RegisterSingleton(GetComponent<ResizeScreenHandler>());
		}
		
	#endregion

	#region Unity Calls
		
		private void Start()
		{
			LoggerSetup();
			Application.targetFrameRate = 60;

			// Активируем все View в UiRoot.
			ActivateAll(UiRoot.transform);

			// Инициализация систем для ECS.
			InitializeSystems();

			// Инициализация контроллеров UI.
			InitializeUI();

			StatsWrapper.Track(TrackId.GameStarted);
			_lossOrVictoryVerificationService.InitGame(this);

			// Моментальное отображение прелоадера.
			AntEngine.Get<Menu>()
				.GetController<TransitionController>()
				.ShowImmediately();
			//AntEngine.Get<Menu>()
			//	.GetController<TouchStickController>()
			//	.SetEnable(false)
			//	.SetVisible(false);

			// Планируем действие на завершение загрузки уровня.
			LevelManager.OnLoaded(() =>
				{
					// Скрываем экран загрузки.
					AntEngine.Get<Menu>()
						.GetController<TransitionController>()
						.Hide();

					// Отображаем лобби.
					AntDelayed.Call(0.25f, () =>
					{
						AntEngine.Get<Menu>()
							.GetController<LobbyController>()
							.Show();
					});
				});
			
			// Определяем текущий уровень для загрузки.
			var saveBox = SaveBoxLoader.Load();
			if (saveBox.levelIndex >= LevelManager.TotalLevels - 1)
			{
				// Если индекс уровня в сохранении равен последнему, то загружаем случайный уровень.
				LevelManager.LoadRandom();
			}
			else
			{
				// Загружаем текущий уровень.
				LevelManager.LoadLevel(saveBox.levelIndex);
			}
		}

		private void Update()
		{
			AntEngine.Execute();
		}

		private void FixedUpdate()
		{
			AntEngine.ExecuteFixed();
		}
		
	#endregion

	#region Private Methods
		
		/// <summary>
		/// Инициализация систем для ECS.
		/// </summary>
		private void InitializeSystems()
		{
			AntEngine.Add<Menu>(0);
		}

		/// <summary>
		/// Инициализация контроллеров для UI.
		/// </summary>
		private void InitializeUI()
		{
			var menu = AntEngine.Get<Menu>();
			menu.Create<TransitionController>();
			menu.Create<FlyingCoinsController>();
			menu.Create<LobbyController>();
			menu.Create<InGameController>();
			menu.Create<DebriefingController>();
			menu.Create<GameOverController>();
			menu.Create<StartGameController>();
			//menu.Create<TouchStickController>();

			// @info Закомментируйте строку ниже, если не используете всплывающие сообщения.
			menu.Create<PopupNotifyController>();

			// @info Закомментируйте строку ниже, если не используете всплывающие текстовые метки.
			menu.Create<FlyingLabelsController>();

			// @info Закомментируйте строку ниже, если не используете бары здоровья.
			menu.Create<HealthBarController>();
		}

		/// <summary>
		/// Настройка логгирования.
		/// </summary>
		private void LoggerSetup()
		{
//#if DebugLog && !FINAL_BUILD
			// @info Включаем подробный логгинг для всех логов через класс A.
			A.verbosity = Verbosity.Verbose;              // Подробное логгирование.
			// A.verbosity = Verbosity.Normal;            // Базовое логгирование.
			// A.verbosity = Verbosity.ErrorsAndWarnings; // Только ошибки и предупреждения.
//#endif
#if UNITY_IOS || UNITY_ANDROID
			// @info Выключаем форматирование для логгера под мобильные платформы.
			A.stripHtmlTags = true;
#endif
			A.Verbose($"{GameTemplate.Name} Ver.{GameTemplate.Version}");
		}

		private void ActivateAll(Transform aTransform)
		{
			Transform t;
			for (int i = 0, n = aTransform.childCount; i < n; i++)
			{
				t = aTransform.GetChild(i);
				if (!t.gameObject.activeSelf)
				{
					t.gameObject.SetActive(true);
				}
			}
		}
		
	#endregion

	#region Event Handlers
		
		// ..
		
	#endregion
	}
}
