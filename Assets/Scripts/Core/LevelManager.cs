namespace DatsGames
{
	using UnityEngine;
	using Anthill.Core;
	using Anthill.Utils;
	using UnityEngine.SceneManagement;
	using System.Collections.Generic;
	
	[RequireComponent(typeof(LevelLoader))]
	public class LevelManager : MonoBehaviour
	{
		public struct RandomLevelItem
		{
			public int levelIndex;
			public float probability;
		}

		public delegate void LevelManagerDelegate();

		/// <summary>
		/// Событие возникающее когда процесс загрузки уровня завершен.
		/// </summary>
		public event LevelManagerDelegate EventFinishLoading;

		/// <summary>
		/// Набор уровней для загрузки/выгрузки.
		/// </summary>
		public LevelsPreset levelsPreset;

		/// <summary>
		/// Имя контейнера внутри загружаемых сцен сущности которых должны
		/// быть добавлены в AntEngine после загрузки.
		/// </summary>
		public string entityContainerName = "Stage";

		/// <summary>
		/// Устанавливает загруженную сцену активной (применяет настройки скайбоксов
		/// и освещения из загруженной сцены).
		/// </summary>
		public bool setLoadedSceneAsActive = false;

	#region Private Variables

		private RandomLevelItem[] _randomLevels;
		private LevelLoader _loader;

		private LevelManagerDelegate _loadedCallback;
		private LevelManagerDelegate _unloadedCallback;

	#endregion

	#region Getters / Setters

		/// <summary>
		/// Определяет последнюю загруженную сцену.
		/// </summary>
		public string LastScene
		{
			get => (_loader.scenesHistory.Count > 0)
				? _loader.scenesHistory[_loader.scenesHistory.Count - 1]
				: string.Empty;
		}

		/// <summary>
		/// Список последних загруженных сцен.
		/// </summary>
		public List<string> ScenesHistory
		{
			get => _loader.scenesHistory;
		}
		
		/// <summary>
		/// Определяет загружен ли какой-нибудь уровень.
		/// </summary>
		public bool IsLevelLoaded
		{
			get => (_loader != null) ? _loader.HasScene : false;
		}

		/// <summary>
		/// Определяет общее количество уровней в текущем наборе.
		/// </summary>
		/// <value></value>
		public int TotalLevels
		{
			get
			{
				A.Assert(levelsPreset == null, "LevelsPreset is not setted!");
				return levelsPreset.levels.Length;
			}
		}
		
	#endregion

	#region Unity Calls
	
		private void Awake()
		{
			_loader = GetComponent<LevelLoader>();
			A.Assert(_loader == null, "LevelManager requires LevelLoader script on the same GameObject!");
		}
	
	#endregion

	#region Public Methods

		/// <summary>
		/// Выбирает и загружает случайный уровень из LevelPreset из тех уровней
		/// которые имеют флаг RND.
		/// </summary>
		public LevelManager LoadRandom()
		{
			if (_randomLevels == null)
			{
				_randomLevels = InitializeRandomLevels();
			}

			return LoadLevel(SelectRandomLevel());
		}

		/// <summary>
		/// Загружает уровень по индексу.
		/// </summary>
		/// <param name="aLevelIndex">Индекс уровня который нужно загрузить.</param>
		public LevelManager LoadLevel(int aLevelIndex)
		{
			A.Assert(levelsPreset == null, "LevelsPreset is not setted!");
			if (aLevelIndex >= 0 && aLevelIndex < levelsPreset.levels.Length)
			{
				LoadLevel(levelsPreset.levels[aLevelIndex].sceneName);
			}
			else
			{
				A.Warning($"Index of level out of bounds ({aLevelIndex}).", "LevelManager");
			}

			return this;
		}

		/// <summary>
		/// Загружает уровень по имени сцены.
		/// </summary>
		/// <param name="aLevelName">Имя сцены которую нужно загрузить.</param>
		public LevelManager LoadLevel(string aLevelName)
		{
			_loader.LoadScene(aLevelName)
				.OnFinishLoading(() =>
				{
					if (setLoadedSceneAsActive)
					{
						SceneManager.SetActiveScene(SceneManager.GetSceneByName(aLevelName));
					}
					
					AntEngine.AddEntitiesFromHierarchy(entityContainerName);
					AntEngine.Initialize();

					EventFinishLoading?.Invoke();
					_loadedCallback?.Invoke();
					_loadedCallback = null;
				});
			return this;
		}

		/// <summary>
		/// Выгружает текущий уровень, выбирает и загружает случайный уровень 
		/// из LevelPreset из тех уровней которые имеют флаг RND.
		/// </summary>
		public LevelManager UnloadCurrentAndLoadRandom()
		{
			if (_randomLevels == null)
			{
				_randomLevels = InitializeRandomLevels();
			}

			return UnloadCurrentAndLoad(SelectRandomLevel());
		}
		
		/// <summary>
		/// Выгружает уровень по имени сцены.
		/// </summary>
		/// <param name="aLevelName">Имя сцены которую нужно выгрузить.</param>
		public LevelManager UnloadLevel(string aLevelName)
		{
			_loader.UnloadScene(aLevelName)
				.OnBeginUnloading(() =>
				{
					AntEngine.RemoveEntitiesFromHierarchy(entityContainerName);
					AntEngine.Cleanup();

					_unloadedCallback?.Invoke();
					_unloadedCallback = null;
				});
			return this;
		}

		/// <summary>
		/// Выгружает текущий уровень и загружает следующий по индексу.
		/// </summary>
		/// <param name="aLevelIndex">Индекс уровня который нужно загрузить.</param>
		public LevelManager UnloadCurrentAndLoad(int aLevelIndex)
		{
			A.Assert(levelsPreset == null, "LevelsPreset is not setted!");
			if (aLevelIndex >= 0 && aLevelIndex < levelsPreset.levels.Length)
			{
				UnloadCurrentAndLoad(levelsPreset.levels[aLevelIndex].sceneName);
			}
			else
			{
				A.Warning($"Index of level out of bounds ({aLevelIndex}).", "LevelManager");
			}

			return this;
		}

		/// <summary>
		/// Выгружает текущий уровень и загружает следующий по имени сцены.
		/// </summary>
		/// <param name="aLevelName">Имя сцены которую нужно загрузить.</param>
		public LevelManager UnloadCurrentAndLoad(string aLevelName)
		{
			_loader.UnloadCurrentAndLoad(aLevelName)
				.OnBeginUnloading(() =>
				{
					AntEngine.RemoveEntitiesFromHierarchy(entityContainerName);
					AntEngine.Cleanup();

					_unloadedCallback?.Invoke();
					_unloadedCallback = null;
				})
				.OnFinishLoading(() =>
				{
					if (setLoadedSceneAsActive)
					{
						SceneManager.SetActiveScene(SceneManager.GetSceneByName(aLevelName));
					}
					
					AntEngine.AddEntitiesFromHierarchy(entityContainerName);
					AntEngine.Initialize();

					EventFinishLoading?.Invoke();
					_loadedCallback?.Invoke();
					_loadedCallback = null;
				});
			return this;
		}

		/// <summary>
		/// Устанавливает одноразовый обратный вызов на событие завершения загрузки.
		/// </summary>
		/// <param name="aCallback">Обратный вызов.</param>
		public LevelManager OnLoaded(LevelManagerDelegate aCallback)
		{
			_loadedCallback = aCallback;
			return this;
		}

		/// <summary>
		/// Устанавливает одноразовый обратный вызов на событие завершения выгрузки.
		/// </summary>
		/// <param name="aCallback">Обратный вызов.</param>
		public LevelManager OnUnloaded(LevelManagerDelegate aCallback)
		{
			_unloadedCallback = aCallback;
			return this;
		}
		
	#endregion

	#region Private Methods
		
		private RandomLevelItem[] InitializeRandomLevels()
		{
			A.Assert(levelsPreset == null, "LevelsPreset is not setted!");

			int count = 0;
			for (int i = 0, n = levelsPreset.levels.Length; i < n; i++)
			{
				if (levelsPreset.levels[i].isAllowForRandomSelection)
				{
					count++;
				}
			}

			int j = 0;
			var result = new RandomLevelItem[count];
			for (int i = 0, n = levelsPreset.levels.Length; i < n; i++)
			{
				if (levelsPreset.levels[i].isAllowForRandomSelection)
				{
					result[j] = new RandomLevelItem
					{
						levelIndex = i,
						probability = 1.0f
					};
					j++;
				}
			}

			return result;
		}

		private int SelectRandomLevel()
		{
			// 1. Складываем веротяность выпадения всех уровней.
			// -------------------------------------------------
			float length = 0.0f;
			for (int i = 0, n = _randomLevels.Length; i < n; i++)
			{
				length += _randomLevels[i].probability;
			}

			// 2. Выбираем случайный уровень.
			// ------------------------------
			float rnd = AntRandom.Range(0.0f, length);
			float cur = 0.0f;
			int index = -1;
			for (int i = 0, n = _randomLevels.Length; i < n; i++)
			{
				if (rnd >= cur && rnd <= cur + _randomLevels[i].probability)
				{
					index = i;
					break;
				}
				cur += _randomLevels[i].probability;
			}

			// 3. Увеличиваем вероятность появления для всех уровней.
			// ------------------------------------------------------
			RandomLevelItem item;
			for (int i = 0, n = _randomLevels.Length; i < n; i++)
			{
				item = _randomLevels[i];
				item.probability += 0.05f;
				_randomLevels[i] = item;
			}

			// 4. Обнуляем шансы выпадения для выбранного уровня.
			// --------------------------------------------------
			item = _randomLevels[index];
			item.probability = 0.0f;
			_randomLevels[index] = item;
			
			return _randomLevels[index].levelIndex;
		}
		
	#endregion

	#region Event Handlers

		// ..
		
	#endregion
	}
}