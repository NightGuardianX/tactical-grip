namespace DatsGames
{
	using UnityEngine;

	public static class ObjectFactory
	{
		public static T Create<T>(string aPrefabPath, Transform aParent = null)
		{
			var go = (GameObject) GameObject.Instantiate(Resources.Load(aPrefabPath));
			A.Assert(go == null, $"Can't create `{aPrefabPath}` prefab.");
			if (aParent != null)
			{
				go.transform.SetParent(aParent, false);
			}
			
			var result = go.GetComponent<T>();
			if (result == null)
			{
				result = go.GetComponentInChildren<T>();
			}
			
			return result;
		}
		public static T Create<T>(string aPrefabPath, Vector3 position, Transform aParent = null)
		{
			var go = (GameObject)GameObject.Instantiate(Resources.Load(aPrefabPath), position, Quaternion.identity);
			A.Assert(go == null, $"Can't create `{aPrefabPath}` prefab.");
			if (aParent != null)
			{
				go.transform.SetParent(aParent, false);
			}

			var result = go.GetComponent<T>();
			if (result == null)
			{
				result = go.GetComponentInChildren<T>();
			}

			return result;
		}
	}
}