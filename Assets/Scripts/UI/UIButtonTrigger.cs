namespace DatsGames
{
	using UnityEngine;
	using DG.Tweening;

	public class UIButtonTrigger : MonoBehaviour
	{
		public delegate void TriggerDelegate();
		public event TriggerDelegate EventOver;
		public event TriggerDelegate EventOut;
		public event TriggerDelegate EventDown;
		public event TriggerDelegate EventUp;
		public event TriggerDelegate EventClick;

		public bool isAnimate = true;
		
		private Tween _tween;
		private bool _isActive = true;

		protected Transform _t;
		protected UIWidget _widget;
		protected bool _isInitialized;

		private void Awake()
		{
			var btn = GetComponent<UIButton>();
			if (btn != null)
			{
				btn.tweenTarget = null;
			}
		}

		protected virtual void Initialize()
		{
			if (_isInitialized)
			{
				return;
			}

			_isInitialized = true;
			_t = GetComponent<Transform>();
			_widget = GetComponent<UIWidget>();
		}

		public void OnHover(bool aIsOver)
		{
			if (aIsOver && EventOver != null)
			{
				EventOver();
			}
			else if (EventOut != null)
			{
				EventOut();
			}
		}

		public void OnPress(bool aIsDown)
		{
			if (aIsDown)
			{
				if (isAnimate)
				{
					if (!_isInitialized)
					{
						Initialize();
					}

					_tween?.Kill();
					_tween = DOTween.To(
						() => _t.localScale, 
						x => _t.localScale = x, 
						new Vector3(0.9f, 0.9f, 0.9f),
						0.1f
					)
						.SetEase(Ease.OutCubic);
				}

				EventDown?.Invoke();
			}
			else
			{
				if (isAnimate)
				{
					if (!_isInitialized)
					{
						Initialize();
					}

					_tween?.Kill();
					_tween = DOTween.To(
						() => _t.localScale, 
						x => _t.localScale = x, 
						new Vector3(1.0f, 1.0f, 1.0f), 
						0.25f
					)
						.SetEase(Ease.OutCubic);
				}
					
				EventUp?.Invoke();
			}
		}

		public void OnClick()
		{
			EventClick?.Invoke();
		}

		public float Alpha
		{
			get
			{
				if (!_isInitialized)
				{
					Initialize();
				}

				return _widget.alpha;
			}
			set
			{
				if (!_isInitialized)
				{
					Initialize();
				}

				_widget.alpha = value;
			}
		}

		public float Scale
		{
			get
			{
				if (!_isInitialized)
				{
					Initialize();
				}

				return _t.localScale.x;
			}
			set
			{
				if (!_isInitialized)
				{
					Initialize();
				}
				
				_t.localScale = new Vector3(value, value, value);
			}
		}

		public bool Active
		{
			get => _isActive;
			set
			{
				_isActive = value;
				gameObject.SetActive(_isActive);
			}
		}
	}
}