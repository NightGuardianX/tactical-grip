namespace DatsGames
{
	using Anthill.Core;
	using Anthill.Inject;
	using UnityEngine;

	public class PopupNotifyController : ISystem, IController
	{
		private PopupNotifyView _view;
		
	#region Getters / Setters
		
		[Inject] public Game Game { get; set; }

	#endregion

	#region ISystem Implementation

		public void AddedToEngine()
		{
			AntInject.Inject<PopupNotifyController>(this);
			_view = ObjectFactory.Create<PopupNotifyView>("UI/PopupNotifyView/PopupNotifyView", Game.UiRoot.transform);
			_view.SetAnchor(Game.UiRoot.gameObject, -10.0f, 10.0f, 10.0f, -10.0f);
			_view.Active = false;

			_view.EventComplete += CompleteHandler;
		}
		
		public void RemovedFromEngine()
		{
			_view.EventComplete -= CompleteHandler;
			GameObject.Destroy(_view.gameObject);
			_view = null;
		}
	
	#endregion

	#region IController Implementation
	
		public void Show()
		{
			_view.Active = true;
			_view.Show();
		}

		public void Hide()
		{
			_view.Hide();
		}
	
	#endregion

	#region Public Methods

		public PopupNotifyController SetDescription(string aValue)
		{
			_view.SetDescription(aValue);
			return this;
		}

	#endregion

	#region Private Methods
		
		// ..

	#endregion

	#region Event Handlers
		
		private void CompleteHandler()
		{
			_view.Active = false;
		}

	#endregion
	}
}