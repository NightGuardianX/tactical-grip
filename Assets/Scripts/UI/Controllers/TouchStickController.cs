namespace DatsGames
{
	using UnityEngine;
	using Anthill.Core;
	using Anthill.Inject;

    public class TouchStickController : ISystem, IController
	{
	#region Private Variables
		
		private TouchStickView _view;
		private bool _isHasHandlers;
		
	#endregion

	#region Getters Setters

		[Inject] public UIRoot UiRoot { get; set; }

		/// <summary>
		/// Определяет дефолтное положение стика относительно центра экрана по высоте.
		/// </summary>
		/// <value>Значение от 0 до 1.</value>
		public float StickOffset
		{
			get => _view.StickOffset;
			set => _view.StickOffset = value;
		}

		/// <summary>
		/// Определяет текущее направление стика (нормализованное значение).
		/// </summary>
		public Vector2 Direction
		{
			get => _view.Direction;
		}

		/// <summary>
		/// Определяет текущий угол стика в градусах.
		/// </summary>
		public float Angle
		{
			get => _view.Angle;
		}

		/// <summary>
		/// Определяет текущую силу стика (нормализованное значение).
		/// </summary>
		public float Force
		{
			get => _view.Distance;
		}

		/// <summary>
		/// Определяет косается ли пользователь стика.
		/// </summary>
		public bool IsTouched
		{
			get => _view.IsTouched;
		}

	#endregion

	#region ISystem Implementation

		public void AddedToEngine()
		{
			AntInject.Inject<TouchStickController>(this);
			_view = ObjectFactory.Create<TouchStickView>("UI/TouchStickView/TouchStickView", UiRoot.transform);
		}
	
		public void RemovedFromEngine()
		{
			GameObject.Destroy(_view.gameObject);
			_view = null;
		}
	
	#endregion

	#region IController Implementation

		public void Show() 
		{
			_view.Show();
		}

		public void Hide()
		{
			_view.Hide();
		}

	#endregion
	
	#region Public Methods

		/// <summary>
		/// Сбрасывает состояние стика.
		/// </summary>
		public TouchStickController Reset()
		{
			_view.Reset();
			return this;
		}

		/// <summary>
		/// Включает/выключает функционал стика. При отключении стика выключается ввод
		/// но сам стик по прежнему будет оставаться на экране.
		/// </summary>
		/// <param name="aEnable">Включить или выключить</param>
		public TouchStickController SetEnable(bool aEnable)
		{
			_view.SetEnable(aEnable);
			return this;
		}

		/// <summary>
		/// Скрывает/отображает стик. При полном скрытии стика он будет работать.
		/// </summary>
		/// <param name="aVisible">Скрыть или отобразить.</param>
		public TouchStickController SetVisible(bool aVisible)
		{
			_view.SetVisible(aVisible);
			return this;
		}

		/// <summary>
		/// Устанавливает одноразовый обратный вызов на косание стика.
		/// </summary>
		/// <param name="aCallback">Обратный вызов.</param>
		public TouchStickController OnTouch(TouchStickView.TouchStickViewDelegate aCallback)
		{
			_view.OnTouch(aCallback);
			return this;
		}

		/// <summary>
		/// Устанавливает одноразовый обратный вызов на отпускание стика.
		/// </summary>
		/// <param name="aCallback">Обратный вызов.</param>
		public TouchStickController OnRelease(TouchStickView.TouchStickViewDelegate aCallback)
		{
			_view.OnRelease(aCallback);
			return this;
		}

	#endregion

	#region Private Methods

		// ..
		
	#endregion

	#region Event Handlers

		// ..

	#endregion
	}
}
