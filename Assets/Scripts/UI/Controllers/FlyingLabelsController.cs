namespace DatsGames
{
	using UnityEngine;
	using System.Collections.Generic;
	using Anthill.Core;
	using Anthill.Inject;
	using Anthill.Utils;

	public class FlyingLabelsController : IController, ISystem, IExecuteSystem
	{
		public enum ValueMode
		{
			Add,
			Replace
		}

	#region Private Variables

		private FlyingLabelsView _view;
		private List<FlyingLabel> _labelsPool;
		private AntNodeList<AntNode<FlyingLabel>> _labelNodes;
		private Camera _worldCamera;

	#endregion

	#region Getters / Setters

		[Inject] public Game Game { get; set; }

	#endregion

	#region ISystem Implementation
		
		public void AddedToEngine()
		{
			AntInject.Inject<FlyingLabelsController>(this);
			_worldCamera = Camera.main;

			_labelNodes = AntEngine.GetNodes<AntNode<FlyingLabel>>();

			_view = ObjectFactory.Create<FlyingLabelsView>("UI/FlyingLabelsView/FlyingLabelsView", Game.UiRoot.transform);
			_view.SetAnchor(Game.UiRoot.gameObject);
			
			InitializePools();
		}

		public void RemovedFromEngine()
		{
			for (int i = 0, n = _labelsPool.Count; i < n; i++)
			{
				GameObject.Destroy(_labelsPool[i].gameObject);
			}
			_labelsPool.Clear();
			_labelNodes = null;

			GameObject.Destroy(_view.gameObject);
			_view = null;
		}
		
	#endregion

	#region IExecuteSystem Implementation
		
		public void Execute()
		{
			UpdateLabels();
		}
		
	#endregion
	
	#region IController Implementation
		
		public void Show()
		{
			// ..
		}

		public void Hide()
		{
			// ..
		}
		
	#endregion
	
	#region Public Methods

		public void SpawnLabel<T>(string aValue, Vector3 aWorldPosition, T aKind) where T : System.Enum
		{
			SpawnLabel(aValue, aWorldPosition, aKind.ToString());
		}
		
		public void SpawnLabel(string aValue, Vector3 aWorldPosition, string aKind = "Default")
		{
			var label = FindNear(aKind, 0.75f, aWorldPosition);
			if (label != null)
			{
				label.Caption = aValue;
			}
			else
			{
				label = GetLabel(aKind);
				if (label == null)
				{
					A.Warning($"Can't spawn label `{aKind}`");
					return;
				}
				label.Caption = aValue;
			}

			Spawn(label, aWorldPosition);
		}

		public void SpawnLabel<T>(int aValue, Vector3 aWorldPosition, 
			T aKind, ValueMode aValueMode = ValueMode.Add) where T : System.Enum
		{
			SpawnLabel(aValue, aWorldPosition, aKind.ToString(), aValueMode);
		}

		public void SpawnLabel(int aValue, Vector3 aWorldPosition, 
			string aKind = "Default", ValueMode aValueMode = ValueMode.Add)
		{
			var label = FindNear(aKind, 0.75f, aWorldPosition);
			if (label != null)
			{
				label.Value = (aValueMode == ValueMode.Add)
					? label.Value + aValue
					: aValue;
			}
			else
			{
				label = GetLabel(aKind);
				if (label == null)
				{
					A.Warning($"Can't spawn label `{aKind}`");
					return;
				}
				label.Value = aValue;
			}

			Spawn(label, aWorldPosition);
		}

		/// <summary>
		/// Конвертирует координаты из WorldSpace в UI Screen Space для nGUI.
		/// </summary>
		/// <param name="aWorldPosition">Позиция в WorldSpace.</param>
		/// <returns>Позиция в UI Screen Space для NGUI.</returns>
		public Vector3 ConvertToUICoordinates(Vector3 aWorldPosition)
		{
			Vector2 pos = _worldCamera.WorldToViewportPoint(aWorldPosition);
		
			float ratio = (float) Game.UiRoot.activeHeight / Screen.height;
			float width = Mathf.Ceil(Screen.width * ratio);
			float height = Mathf.Ceil(Screen.height * ratio);

			return  new Vector3(
				(pos.x * width) - (width * 0.5f),
				(pos.y * height) - (height * 0.5f),
				0.0f
			);
		}
		
	#endregion

	#region Private Methods

		private void Spawn(FlyingLabel aLabel, Vector3 aWorldPosition)
		{
			aLabel.WorldPosition = aWorldPosition;
			aLabel.transform.localPosition = ConvertToUICoordinates(aWorldPosition);
			aLabel.gameObject.SetActive(true);

			if (!aLabel.IsVisible)
			{
				AntEngine.AddEntity(aLabel.GetComponent<AntEntity>());
			}

			aLabel.Show();
		}

		private void UpdateLabels()
		{
			float dt = Time.deltaTime;
			for (int i = _labelNodes.Count - 1; i >= 0 ; i--)
			{
				var node = _labelNodes[i];
				var p = node.Entity.Transform.localPosition;
				p.y += node.Component1.flyingSpeed * dt;
				node.Entity.Transform.localPosition = p;

				node.Component1.LifeTime -= dt;
				if (node.Component1.LifeTime <= 0.0f && !node.Component1.IsFree)
				{
					node.Component1.Hide();
				}

				if (node.Component1.IsFree && !node.Component1.IsVisible)
				{
					AntEngine.RemoveEntity(node.Entity);
				}
			}
		}
		
		private void InitializePools()
		{
			GameObject go = null;
			_labelsPool = new List<FlyingLabel>();
			for (int i = 0, n = _view.labels.Length; i < n; i++)
			{
				for (int j = 0, nj = _view.labels[i].quantity; j < nj; j++)
				{
					go = GameObject.Instantiate(_view.labels[i].prefab);
					go.transform.SetParent(_view.transform, false);
					go.SetActive(false);
					var lbl = go.GetComponent<FlyingLabel>();
					lbl.kind = _view.labels[i].kind;
					_labelsPool.Add(lbl);
				}
			}
		}

		private FlyingLabel FindNear(string aKind, float aDist, Vector3 aWorldPosition)
		{
			for (int i = 0, n = _labelsPool.Count; i < n; i++)
			{
				if (_labelsPool[i].IsVisible && _labelsPool[i].kind.Equals(aKind))
				{
					if (AntMath.Distance(_labelsPool[i].WorldPosition, aWorldPosition) < aDist)
					{
						return _labelsPool[i];
					}
				}
			}
			
			return null;
		}

		private FlyingLabel GetLabel(string aKind)
		{
			for (int i = 0, n = _labelsPool.Count; i < n; i++)
			{
				if (_labelsPool[i].IsFree && 
					!_labelsPool[i].IsVisible && 
					_labelsPool[i].kind.Equals(aKind))
				{
					return _labelsPool[i];
				}
			}
			
			return null;
		}
		
	#endregion
	}
}