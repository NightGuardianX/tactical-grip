using Anthill.Core;
using Anthill.Inject;
using DatsGames;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StartGameController : IController, ISystem
{
    private StartGameView _view;
    private WrapperRepository _wrapperRepository =  new WrapperRepository();
    private EnemyRepository _enemyRepository = new EnemyRepository();
    #region Getters / Setters
    [Inject] public Game Game { get; set; }

    #endregion
    public void AddedToEngine()
    {
        AntInject.Inject<StartGameController>(this);
        _view = ObjectFactory.Create<StartGameView>("UI/StartGameView/StartGameView",
            Game.UiRoot.transform);
        _view.SetAnchor(Game.UiRoot.gameObject);
        _view.Active = false;
    }

    public void Hide()
    {
        _view.Hide();
        _view.EventStartGame -= StartGameHandler;
    }

    public void RemovedFromEngine()
    {
        GameObject.Destroy(_view.gameObject);
        _view = null;
    }

    public void Show()
    {
        _view.Show();
        _view.EventStartGame += StartGameHandler;
    }
    private void StartGameHandler()
    {
        _wrapperRepository.NotificateAllAboutStartGame();
        _enemyRepository.NotificateAllAboutStartGame();
        StatsWrapper.Track(TrackId.LevelStart, "level", Game.LevelManager.LastScene);
        Hide();
    }
}
