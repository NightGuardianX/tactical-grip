namespace DatsGames
{
	interface IController
	{
		void Show();
		void Hide();
	}
}