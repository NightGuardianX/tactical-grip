namespace DatsGames
{
	using UnityEngine;
	using Anthill.Core;
	using Anthill.Inject;

	public class InGameController : IController, ISystem
	{
		private InGameView _view;
		private WrapperRepository _wrapperRepository = new WrapperRepository();
		private LossOrVictoryVerificationService _lossOrVictoryService =
			new LossOrVictoryVerificationService();
		private EnemyRepository _enemyRepository = new EnemyRepository();

	#region Getters / Setters
		
		[Inject] public Game Game { get; set; }
		[Inject] public LevelManager LevelManager { get; set; }

		public float Cash
		{
			get => _view.cashBar.Value;
			set => _view.cashBar.Value = value;
		}
		
		public int CurrentLevel
		{
			get => _view.levelProgress.CurrentLevel;
			set => _view.levelProgress.CurrentLevel = value;
		}

		public int NextLevel
		{
			get => _view.levelProgress.NextLevel;
			set => _view.levelProgress.NextLevel = value;
		}

		public float LevelProgress
		{
			get => _view.levelProgress.Progress;
			set => _view.levelProgress.Progress = value;
		}
		
	#endregion

	#region ISystem Implementation
		
		public void AddedToEngine()
		{
			AntInject.Inject<InGameController>(this);
			_view = ObjectFactory.Create<InGameView>("UI/InGameView/InGameView", Game.UiRoot.transform);
			_view.SetAnchor(Game.UiRoot.gameObject);
			_view.Active = false;
		}

		public void RemovedFromEngine()
		{
			GameObject.Destroy(_view.gameObject);
			_view = null;
		}
		
	#endregion

	#region IController Implementation

		public void Show()
		{
			_view.Show();
			ShowProgressBar(false);
		}

		public void Hide()
		{
			_view.Hide();
		}
		
	#endregion

	#region Public Methods

		public InGameController SetCashValue(int aValue)
		{
			_view.cashBar.SetValue(aValue);
			return this;
		}

		public InGameController ShowCashBar(bool aVisible)
		{
			_view.cashBar.Show(aVisible);
			return this;
		}

		public InGameController ShowProgressBar(bool aVisible)
		{
			_view.levelProgress.Show(aVisible);
			return this;
		}

		public InGameController ShowRestartButton(bool aVisible)
		{
			if (aVisible)
			{
				_view.buttonsBar.Show();
				_view.buttonsBar.EventRestart += RestartHandler;
			}
			else
			{
				_view.buttonsBar.Hide();
				_view.buttonsBar.EventRestart -= RestartHandler;
			}

			return this;
		}

		public Vector3 GetCashBarPosition()
		{
			var p = _view.cashBar.transform.parent;
			var pos = _view.cashBar.transform.localPosition;
			pos.x += 30.0f;
			while (!System.Object.ReferenceEquals(p.transform, _view.transform))
			{
				pos += p.transform.localPosition;
				p = p.transform.parent;
			}
			return pos + p.transform.localPosition;
		}

		public InGameController SetProgress(float aValue)
		{
			_view.levelProgress.SetProgress(aValue);
			return this;
		}
		
	#endregion

	#region Event Handlers
		
		private void RestartHandler()
		{
			
			AntEngine.Get<Menu>()
				.GetController<HealthBarController>()
				.Hide();
			_wrapperRepository.Clear();
			_lossOrVictoryService.Clear();
			_enemyRepository.Clear();
			ShowRestartButton(false);
			ShowProgressBar(false);
			AntEngine.Get<Menu>()
				.GetController<TransitionController>()
				.OnShowComplete(() =>
				{
					LevelManager.UnloadCurrentAndLoad(LevelManager.LastScene)
						.OnLoaded(() =>
						{
							AntEngine.Get<Menu>()
								.GetController<TransitionController>()
								.Hide();
							
							AntEngine.Get<Menu>()
								.GetController<LobbyController>()
								.Show();
						});
				})
				.Show(0.25f);
		}
		
	#endregion
	}
}