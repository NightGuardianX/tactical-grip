namespace DatsGames
{
	using UnityEngine;
	using Anthill.Core;
	using Anthill.Inject;

	public class LobbyController : IController, ISystem
	{
		private LobbyView _view;
		private SaveBox _saveBox;

	#region Getters / Setters
		
		[Inject] public Game Game { get; set; }
		
	#endregion

	#region ISystem Implementation
		
		public void AddedToEngine()
		{
			AntInject.Inject<LobbyController>(this);
			_view = ObjectFactory.Create<LobbyView>("UI/LobbyView/LobbyView", Game.UiRoot.transform);
			_view.SetAnchor(Game.UiRoot.gameObject);
			_view.Active = false;
			_saveBox = new SaveBox();
		}

		public void RemovedFromEngine()
		{
			GameObject.Destroy(_view.gameObject);
			_view = null;
			_saveBox = null;
		}
		
	#endregion
	
	#region IController Implementation

		public void Show()
		{
			SaveBoxLoader.Load(_saveBox);
			_view.CurrentLevel = _saveBox.displayLevelIndex;
			_view.EventStartGame = StartGameHandler;
			_view.Show();
		}

		public void Hide()
		{
			_view.Hide();
		}
		
	#endregion

	#region Event Handlers
		
		private void StartGameHandler()
		{
			AntEngine.Get<Menu>()
				.GetController<HealthBarController>()
				.Show();
				
			var ingame = AntEngine.Get<Menu>()
				.GetController<InGameController>();

			ingame.CurrentLevel = _saveBox.displayLevelIndex;
			ingame.NextLevel = _saveBox.displayLevelIndex + 1;

			ingame.Show();
			//ingame.ShowProgressBar(true);
			ingame.ShowRestartButton(true);
				
			Hide();
		}
		
	#endregion
	}
}