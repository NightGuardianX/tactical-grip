namespace DatsGames
{
	using UnityEngine;
	using System.Collections.Generic;
	using Anthill.Core;
	using Anthill.Inject;

	public class FlyingCoinsController : IController, ISystem, IExecuteSystem
	{
		public delegate void FlyingCoinDelegate();
		public event FlyingCoinDelegate EventCoinSpawn;
		public event FlyingCoinDelegate EventCoinCollect;

		private FlyingCoinsView _view;
		private List<FlyingCoin> _coinsPool;
		private AntNodeList<AntNode<FlyingCoin>> _coinNodes;

		private FlyingCoinDelegate _completeCallback;
		private int _spawnCount;
		private Camera _worldCamera;

	#region Getters / Setters
		
		[Inject] public Game Game { get; set; }
		
	#endregion

	#region ISystem Implementation
		
		public void AddedToEngine()
		{
			AntInject.Inject<FlyingCoinsController>(this);
			_coinNodes = AntEngine.GetNodes<AntNode<FlyingCoin>>();
			_worldCamera = Camera.main;

			_view = ObjectFactory.Create<FlyingCoinsView>("UI/FlyingCoinsView/FlyingCoinsView", Game.UiRoot.transform);
			_view.SetAnchor(Game.UiRoot.gameObject);
			
			InitializePools();
		}

		public void RemovedFromEngine()
		{
			_coinNodes = null;

			for (int i = 0, n = _coinsPool.Count; i < n; i++)
			{
				GameObject.Destroy(_coinsPool[i].gameObject);
			}
			_coinsPool.Clear();

			GameObject.Destroy(_view.gameObject);
			_view = null;
		}
		
	#endregion

	#region IExecuteSystem Implementation
		
		public void Execute()
		{
			UpdateCoins();
		}
		
	#endregion

	#region IController Implementation
		
		public void Show()
		{
			// ..
		}

		public void Hide()
		{
			// ..
		}
		
	#endregion

	#region Public Methods

		/// <summary>
		/// Создает определенное количество монет в WorldSpace пространстве и запускает их в полет из
		/// стартовой позиции в финишную.
		/// </summary>
		/// <param name="aAmount">Количество монет для спавна.</param>
		/// <param name="aSpawnPos">Стартовая позиция в WorldSpace.</param>
		/// <param name="aTargetPos">Конечная позиция в координатах NGUI.</param>
		public FlyingCoinsController SpawnCoinsWorld(int aAmount, Vector3 aSpawnPos, Vector3 aTargetPos)
		{
			SpawnCoins(aAmount, ConvertToUICoordinates(aSpawnPos), aTargetPos);
			return this;
		}

		/// <summary>
		/// Создает определенное количество монет в пространстве UI и запускает их в полет из стартовой
		/// позиции в финишную.
		/// </summary>
		/// <param name="aAmount">Количество монет для спавна.</param>
		/// <param name="aSpawnPos">Стартовая позиция в координатах NGUI.</param>
		/// <param name="aTargetPos">Конечная позиция в координатах NGUI.</param>
		public FlyingCoinsController SpawnCoins(int aAmount, Vector3 aSpawnPos, Vector3 aTargetPos)
		{
			for (int i = 0; i < aAmount; i++)
			{
				var coin = GetCoin();
				coin.Spawn(aSpawnPos, aTargetPos);
				AntEngine.AddEntity(coin.GetComponent<AntEntity>());
			}

			_spawnCount = aAmount;
			return this;
		}
		
		/// <summary>
		/// Устанавливает одноразовый вызов на событие завершения анимации полета монет.
		/// </summary>
		/// <param name="aCallback">Ссылка на метод.</param>
		public FlyingCoinsController OnComplete(FlyingCoinDelegate aCallback)
		{
			_completeCallback = aCallback;
			return this;
		}

		/// <summary>
		/// Конвертирует координаты из WorldSpace в UI Screen Space для nGUI.
		/// </summary>
		/// <param name="aWorldPosition">Позиция в WorldSpace.</param>
		/// <returns>Позиция в UI Screen Space для NGUI.</returns>
		public Vector3 ConvertToUICoordinates(Vector3 aWorldPosition)
		{
			Vector2 pos = _worldCamera.WorldToViewportPoint(aWorldPosition);
		
			float ratio = (float) Game.UiRoot.activeHeight / Screen.height;
			float width = Mathf.Ceil(Screen.width * ratio);
			float height = Mathf.Ceil(Screen.height * ratio);

			return  new Vector3(
				(pos.x * width) - (width * 0.5f),
				(pos.y * height) - (height * 0.5f),
				0.0f
			);
		}

	#endregion

	#region Private Methods

		private void UpdateCoins()
		{
			float dt = Time.deltaTime;
			for (int i = _coinNodes.Count - 1; i >= 0 ; i--)
			{
				var node = _coinNodes[i];
				node.Component1.UpdateLogic(dt);
				if (node.Component1.CurrentState == FlyingCoin.State.Spawn &&
					node.Component1.CurrentState != node.Component1.PrevState)
				{
					node.Component1.PrevState = node.Component1.CurrentState;
					EventCoinSpawn?.Invoke();
				}

				if (node.Component1.CurrentState == FlyingCoin.State.Finish)
				{
					node.Component1.Free();
					AntEngine.RemoveEntity(node.Entity);
					EventCoinCollect?.Invoke();

					_spawnCount--;
					if (_spawnCount <= 0)
					{
						_completeCallback?.Invoke();
						_completeCallback = null;
					}
				}
			}
		}
		
		private void InitializePools()
		{
			_coinsPool = new List<FlyingCoin>();
			for (int i = 0, n = _view.coins.Length; i < n; i++)
			{
				for (int j = 0, nj = _view.coins[i].quantity; j < nj; j++)
				{
					var go = GameObject.Instantiate(_view.coins[i].prefab);
					go.transform.SetParent(_view.coinsContainerRef.transform, false);
					go.SetActive(false);
					_coinsPool.Add(go.GetComponent<FlyingCoin>());
				}
			}
		}

		private FlyingCoin GetCoin()
		{
			for (int i = 0, n = _coinsPool.Count; i < n; i++)
			{
				if (_coinsPool[i].IsFree)
				{
					return _coinsPool[i];
				}
			}
			
			var go = GameObject.Instantiate(_view.coins[0].prefab);
			var coin = go.GetComponent<FlyingCoin>();
			go.transform.SetParent(_view.coinsContainerRef.transform, false);
			go.SetActive(false);
			_coinsPool.Add(coin);
			return coin;
		}
		
	#endregion
	}
}