namespace DatsGames
{
	using Anthill.Core;
	using Anthill.Inject;
	using UnityEngine;

	public class TransitionController : IController, ISystem
	{
		public delegate void TransitionControllerDelegate();

		/// <summary>
		/// Событие возникающее когда анимация появления перехода закончилась.
		/// </summary>
		public event TransitionControllerDelegate EventShowComplete;

		/// <summary>
		/// Событие возникающее когда анимация исчезновения перехода закончилась.
		/// </summary>
		public event TransitionControllerDelegate EventHideComplete;

	#region Prviate Variables

		private TransitionView _view;
		private TransitionControllerDelegate _finishShowingCallback;
		private TransitionControllerDelegate _finishHidingCallback;

	#endregion

	#region Getters / Setters
		
		[Inject] public Game Game { get; set; }
		
	#endregion

	#region ISystem Implementation
		
		public void AddedToEngine()
		{
			AntInject.Inject<TransitionController>(this);
			_view = ObjectFactory.Create<TransitionView>("UI/TransitionView/TransitionView", Game.UiRoot.transform);
			_view.SetAnchor(Game.UiRoot.gameObject);
			_view.Active = false;
		}

		public void RemovedFromEngine()
		{
			GameObject.Destroy(_view.gameObject);
			_view = null;
		}
		
	#endregion

	#region IController Implementation
		
		public void Show()
		{
			Show(0.0f);
		}

		public void Hide()
		{
			_view.OnHideComplete(() =>
			{
				EventHideComplete?.Invoke();
				_finishHidingCallback?.Invoke();
				_finishHidingCallback = null;
			})
				.Hide();
		}

	#endregion

	#region Public Methods

		/// <summary>
		/// Отображает прелоадер с анимацией появления/исчезновения.
		/// </summary>
		/// <param name="aVisible">Видимость прелоадера.</param>
		public TransitionController ShowPreloader(bool aVisible)
		{
			A.Assert(_view.preloader == null, "No preloader in the Transition screen.");
			if (aVisible)
			{
				_view.preloader.Show(true);
			}
			else
			{
				_view.preloader.Hide(true);
			}
			return this;
		}

		/// <summary>
		/// Отображает моментально прелоадер, без его анимации появления/исчезновения.
		/// </summary>
		/// <param name="aVisible">Видимость прелоадера.</param>
		public TransitionController ShowPreloaderImmediately(bool aVisible)
		{
			A.Assert(_view.preloader == null, "No preloader in the Transition screen.");
			if (aVisible)
			{
				_view.preloader.Show(false);
			}
			else
			{
				_view.preloader.Hide(false);
			}
			return this;
		}
		
		/// <summary>
		/// Моментальное отображение окна перехода.
		/// </summary>
		public TransitionController ShowImmediately()
		{
			_view.Active = true;
			_view.Alpha = 1.0f;
			return this;
		}

		/// <summary>
		/// Отображение окна перехода с предварительной задержкой.
		/// </summary>
		/// <param name="aDelay">Задержка в секундах.</param>
		public TransitionController Show(float aDelay)
		{
			_view.OnShowComplete(() =>
			{
				EventShowComplete?.Invoke();
				_finishShowingCallback?.Invoke();
				_finishShowingCallback = null;
			})
				.Show(aDelay);
			return this;
		}

		/// <summary>
		/// Устанавливает одноразовый обратный вызов на событие завершения анимации появления.
		/// </summary>
		/// <param name="aCallback">Обратный вызов.</param>
		public TransitionController OnShowComplete(TransitionControllerDelegate aCallback)
		{
			_finishShowingCallback = aCallback;
			return this;
		}

		/// <summary>
		/// Устанавливает одноразовый обратный вызов на событие завершения анимации исчезновения.
		/// </summary>
		/// <param name="aCallback">Обратный вызов.</param>
		public TransitionController OnHideComplete(TransitionControllerDelegate aCallback)
		{
			_finishHidingCallback = aCallback;
			return this;
		}
		
	#endregion
	}
}