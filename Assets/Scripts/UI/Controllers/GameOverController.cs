namespace DatsGames
{
	using UnityEngine;
	using Anthill.Core;
	using Anthill.Inject;
	
	public class GameOverController : IController, ISystem
	{
		private GameOverView _view;
		private bool _hasHandlers;
		private SaveBox _saveBox;
		private WrapperRepository _wrapperRepository = new WrapperRepository();
		private LossOrVictoryVerificationService _lossOrVictoryService =
			new LossOrVictoryVerificationService();
		private EnemyRepository _enemyRepository = new EnemyRepository();
		#region Getters / Setters

		[Inject] public Game Game { get; set; }
		[Inject] public LevelManager LevelManager { get; set; }
		
	#endregion

	#region ISystem Implementation
		
		public void AddedToEngine()
		{
			AntInject.Inject<GameOverController>(this);
			_view = ObjectFactory.Create<GameOverView>("UI/GameOverView/GameOverView", Game.UiRoot.transform);
			_view.SetAnchor(Game.UiRoot.gameObject);
			_view.Active = false;

			_saveBox = new SaveBox();
		}

		public void RemovedFromEngine()
		{
			GameObject.Destroy(_view.gameObject);
			_view = null;
			_saveBox = null;
		}
		
	#endregion

	#region IController Implementation
		
		public void Show()
		{
			AddHandlers();

			SaveBoxLoader.Load(_saveBox);
			_view.LevelValue = _saveBox.displayLevelIndex;
			_view.Show();
		}

		public void Hide()
		{
			RemoveHandlers();
			_view.Hide();
		}
		
	#endregion
	
	#region Private Methods
		
		private void AddHandlers()
		{
			if (_hasHandlers)
			{
				return;
			}

			_hasHandlers = true;
			_view.EventRestart += RestartHandler;
		}

		private void RemoveHandlers()
		{
			if (!_hasHandlers)
			{
				return;
			}

			_hasHandlers = false;
			_view.EventRestart -= RestartHandler;
		}
		
	#endregion

	#region Event Handlers

		private void RestartHandler()
		{
			RemoveHandlers();
			_view.HideRestartButton();
			Hide();
			_wrapperRepository.Clear();
			_lossOrVictoryService.Clear();
			_enemyRepository.Clear();
			AntDelayed.Call(0.25f, () =>
			{
				AntEngine.Get<Menu>()
					.GetController<TransitionController>()
					.OnShowComplete(() =>
					{
						LevelManager.UnloadCurrentAndLoad(LevelManager.LastScene)
							.OnLoaded(() =>
							{
								AntEngine.Get<Menu>()
									.GetController<TransitionController>()
									.Hide();
								
								AntEngine.Get<Menu>()
									.GetController<LobbyController>()
									.Show();
							});
					})
					.Show(0.25f);
			});
		}
		
	#endregion
	}
}