namespace DatsGames
{
	using UnityEngine;
	using Anthill.Core;
	using Anthill.Inject;

	public class HealthBarController : ISystem, IController
	{
		private HealthBarView _view;
		private Camera _worldCamera;

	#region Getters / Setters
		
		[Inject] public Game Game { get; set; }

	#endregion

	#region ISystem Implementation

		public void AddedToEngine()
		{
			AntInject.Inject<HealthBarController>(this);
			_view = ObjectFactory.Create<HealthBarView>("UI/HealthBarView/HealthBarView", Game.UiRoot.transform);
			_view.SetAnchor(Game.UiRoot.gameObject, -10.0f, 10.0f, 10.0f, -10.0f);
			_view.Active = false;
		}
		
		public void RemovedFromEngine()
		{
			GameObject.Destroy(_view.gameObject);
			_view = null;
		}

		public void InitCamera(Camera camera)
        {
			_worldCamera = camera;
		}
	
	#endregion
		
	#region IController Implementation
	
		public void Show()
		{
			_view.Show();
		}

		public void Hide()
		{
			_view.Hide();
		}
	
	#endregion

	#region Public Methods

		public int RequestBar(HealthBarKind aKind)
		{
			return _view.RequestBar(aKind);
		}

		public void ReleaseBar(int aIndex)
		{
			_view.ReleaseBar(aIndex);
		}

		public void SetPositionWorld(int aBarIndex, Vector3 aWorldPosition)
		{
			_view.SetPosition(aBarIndex, ConvertToUICoordinates(aWorldPosition));
		}

		public void SetPosition(int aBarIndex, Vector3 aPosition)
		{
			_view.SetPosition(aBarIndex, aPosition);
		}

		public void UpdateBarWorld(int aBarIndex, float aValue, Vector3 aWorldPosition)
		{
			_view.UpdateBar(aBarIndex, aValue, ConvertToUICoordinates(aWorldPosition));
		}

		public void SetMaxValue(int aBarIndex, float aValue)
        {
			_view.SetMaxValue(aBarIndex, aValue);
        }
		public void UpdateBar(int aBarIndex, float aValue, Vector3 aPosition)
		{
			_view.UpdateBar(aBarIndex, aValue, aPosition);
		}

		/// <summary>
		/// Конвертирует координаты из WorldSpace в UI Screen Space для nGUI.
		/// </summary>
		/// <param name="aWorldPosition">Позиция в WorldSpace.</param>
		/// <returns>Позиция в UI Screen Space для NGUI.</returns>
		public Vector3 ConvertToUICoordinates(Vector3 aWorldPosition)
		{
			Vector2 pos = _worldCamera.WorldToViewportPoint(aWorldPosition);
		
			float ratio = (float) Game.UiRoot.activeHeight / Screen.height;
			float width = Mathf.Ceil(Screen.width * ratio);
			float height = Mathf.Ceil(Screen.height * ratio);

			return  new Vector3(
				(pos.x * width) - (width * 0.5f),
				(pos.y * height) - (height * 0.5f),
				0.0f
			);
		}

	#endregion

	#region Private Methods

		// ..

	#endregion

	#region Event Handlers
		
		// ..

	#endregion
	}
}