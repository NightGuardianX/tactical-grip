namespace DatsGames
{
	using UnityEngine;
	using Anthill.Core;
	using Anthill.Inject;
	
	public class DebriefingController : IController, ISystem
	{
		/// <summary>
		/// Награда за прохождение уровня.
		/// </summary>
		private const int _reward = 250;

		/// <summary>
		/// Ценность одной монетки для визуального эффекта. 
		/// Итоговая награда / _denomination = количество монет для спавна.
		/// </summary>
		private const int _denomination = 10;

		private DebriefingView _view;
		private FlyingCoinsController _coins;
		private InGameController _ingame;
		private bool _isHasHandlers;
		private SaveBox _saveBox;
		private WrapperRepository _wrapperRepository = new WrapperRepository();
		private LossOrVictoryVerificationService _lossOrVictoryService =
			new LossOrVictoryVerificationService();
		private EnemyRepository _enemyRepository = new EnemyRepository();

		#region Getters / Setters

		[Inject] public Game Game { get; set; }
		[Inject] public LevelManager LevelManager { get; set; }
		
	#endregion

	#region ISystem Implementation
		
		public void AddedToEngine()
		{
			AntInject.Inject<DebriefingController>(this);
			_view = ObjectFactory.Create<DebriefingView>("UI/DebriefingView/DebriefingView", Game.UiRoot.transform);
			_view.SetAnchor(Game.UiRoot.gameObject);
			_view.Active = false;

			_coins = AntEngine.Get<Menu>()
				.GetController<FlyingCoinsController>();
			_coins.EventCoinSpawn += SpawnCoinHandler;
			_coins.EventCoinCollect += CollectCoinHandler;

			_ingame = AntEngine.Get<Menu>()
				.GetController<InGameController>();

			// Модель данных для сохранения.
			_saveBox = new SaveBox();
		}

		public void RemovedFromEngine()
		{
			_coins.EventCoinSpawn -= SpawnCoinHandler;
			_coins.EventCoinCollect -= CollectCoinHandler;
			_coins = null;

			GameObject.Destroy(_view.gameObject);
			_view = null;
		}
		
	#endregion

	#region IController Implementation
		
		public void Show()
		{
			AddHandlers();
			_view.RewardValue = _reward;

			// Загружаем сохранение.
			SaveBoxLoader.Load(_saveBox);
			_view.LevelValue = _saveBox.displayLevelIndex;

			_view.Show();
		}

		public void Hide()
		{
			RemoveHandlers();
			_view.Hide();
		}
		
	#endregion

	#region Private Methods
		
		private void AddHandlers()
		{
			if (_isHasHandlers)
			{
				return;
			}

			_isHasHandlers = true;
			_view.EventClaim += ClaimHandler;
		}

		private void RemoveHandlers()
		{
			if (!_isHasHandlers)
			{
				return;
			}

			_isHasHandlers = false;
			_view.EventClaim -= ClaimHandler;
		}
		
	#endregion

	#region Event Handlers

		private void SpawnCoinHandler()
		{
			_view.RewardValue = -_denomination;
		}
		
		private void CollectCoinHandler()
		{
			_ingame.Cash += _denomination;
		}

		private void ClaimHandler()
		{
			RemoveHandlers();
			_view.HideClaimButton();

			var ingame = AntEngine.Get<Menu>()
				.GetController<InGameController>();
			
			ingame.SetCashValue(_saveBox.coins)
				.ShowCashBar(true);
			
			AntEngine.Get<Menu>()
				.GetController<FlyingCoinsController>()
				// Спавним летающие монетки.
				.SpawnCoins(
					_reward / _denomination, 
					_view.GetCoinsIconPosition(), 
					ingame.GetCashBarPosition()
				)
				// Когда монетки закончат анимацию...
				.OnComplete(() =>
				{
					// Скрываем кэшбар и текущий контроллер.
					ingame.ShowCashBar(false);
					Hide();

					// Записываем изменения в модель данных.
					_saveBox.coins += _reward;
					_saveBox.levelIndex++;
					_saveBox.displayLevelIndex++;

					// Сохраняем изменения.
					SaveBoxLoader.Write(_saveBox);
					_wrapperRepository.Clear();
					_enemyRepository.Clear();
					_lossOrVictoryService.Clear();
					// Через 0.25 сек.
					AntDelayed.Call(0.5f, () =>
					{
						AntEngine.Get<Menu>()
							.GetController<TransitionController>()
							.OnShowComplete(() =>
							{
								// Устанавливаем обратный вызов на загрузку уровня.
								LevelManager.OnLoaded(() =>
									{
										// Скрываем экран перехода когда загрузка окончится.
										AntEngine.Get<Menu>()
											.GetController<TransitionController>()
											.Hide();
										
										// Отображаем лобби.
										AntEngine.Get<Menu>()
											.GetController<LobbyController>()
											.Show();
									});

								// Если пройдены все уровни.
								if (_saveBox.levelIndex >= LevelManager.TotalLevels - 1)
								{
									// Остаемся на последнем уровне.
									_saveBox.levelIndex = LevelManager.TotalLevels - 1;
									SaveBoxLoader.Write(_saveBox);
									
									// Загружаем случайный уровень.
									LevelManager.UnloadCurrentAndLoadRandom();
								}
								else
								{
									// Загружаем следующий уровень по порядку.
									LevelManager.UnloadCurrentAndLoad(_saveBox.levelIndex);
								}
							})
							// Покаываем экран перехода.
							.Show();
					});
				});
		}
		
	#endregion
	}
}