namespace DatsGames
{
	using UnityEngine;
	using DG.Tweening;

	public static class UIButtonTriggerExtension
	{
		public static Tween DoAlpha(this UIButtonTrigger aTarget, float aAlpha, float aDuration)
		{
			return DOTween.To(
				() => aTarget.Alpha,
				x => aTarget.Alpha = x,
				aAlpha,
				aDuration
			);
		}

		public static Tween DoAlpha(this UIButtonTrigger aTarget, float aFromAlpha, float aToAlpha, float aDuration)
		{
			aTarget.Alpha = aFromAlpha;
			return DOTween.To(
				() => aTarget.Alpha,
				x => aTarget.Alpha = x,
				aToAlpha,
				aDuration
			);
		}

		public static Tween DoPosition(this UIButtonTrigger aTarget, Vector3 aPosition, float aDuration)
		{
			return DOTween.To(
				() => aTarget.transform.localPosition,
				x => aTarget.transform.localPosition = x,
				aPosition,
				aDuration
			);
		}

		public static Tween DoPosition(this UIButtonTrigger aTarget, Vector3 aFromPosition, Vector3 aToPosition, float aDuration)
		{
			aTarget.transform.localPosition = aFromPosition;
			return DOTween.To(
				() => aTarget.transform.localPosition,
				x => aTarget.transform.localPosition = x,
				aToPosition,
				aDuration
			);
		}

		public static Tween DoScale(this UIButtonTrigger aTarget, Vector3 aScale, float aDuration)
		{
			return DOTween.To(
				() => aTarget.transform.localScale,
				x => aTarget.transform.localScale = x,
				aScale,
				aDuration
			);
		}

		public static Tween DoScale(this UIButtonTrigger aTarget, Vector3 aFromScale, Vector3 aToScale, float aDuration)
		{
			aTarget.transform.localScale = aFromScale;
			return DOTween.To(
				() => aTarget.transform.localScale,
				x => aTarget.transform.localScale = x,
				aToScale,
				aDuration
			);
		}
	}
}