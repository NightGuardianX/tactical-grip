namespace DatsGames
{
	using UnityEngine;
	using DG.Tweening;

	public static class UILabelExtension
	{
		public static Tween DoAlpha(this UILabel aTarget, float aAlpha, float aDuration)
		{
			return DOTween.To(
				() => aTarget.alpha,
				x => aTarget.alpha = x,
				aAlpha,
				aDuration
			);
		}

		public static Tween DoAlpha(this UILabel aTarget, float aFromAlpha, float aToAlpha, float aDuration)
		{
			aTarget.alpha = aFromAlpha;
			return DOTween.To(
				() => aTarget.alpha,
				x => aTarget.alpha = x,
				aToAlpha,
				aDuration
			);
		}

		public static Tween DoPosition(this UILabel aTarget, Vector3 aPosition, float aDuration)
		{
			return DOTween.To(
				() => aTarget.transform.localPosition,
				x => aTarget.transform.localPosition = x,
				aPosition,
				aDuration
			);
		}

		public static Tween DoPosition(this UILabel aTarget, Vector3 aFromPosition, Vector3 aToPosition, float aDuration)
		{
			aTarget.transform.localPosition = aFromPosition;
			return DOTween.To(
				() => aTarget.transform.localPosition,
				x => aTarget.transform.localPosition = x,
				aToPosition,
				aDuration
			);
		}

		public static Tween DoScale(this UISprite aTarget, Vector3 aScale, float aDuration)
		{
			return DOTween.To(
				() => aTarget.transform.localScale,
				x => aTarget.transform.localScale = x,
				aScale,
				aDuration
			);
		}

		public static Tween DoScale(this UILabel aTarget, Vector3 aFromScale, Vector3 aToScale, float aDuration)
		{
			aTarget.transform.localScale = aFromScale;
			return DOTween.To(
				() => aTarget.transform.localScale,
				x => aTarget.transform.localScale = x,
				aToScale,
				aDuration
			);
		}
	}
}