using DatsGames;
using DG.Tweening;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StartGameView : UIElement
{
    public delegate void StartGameDelegate();
    public event StartGameDelegate EventStartGame;

	[SerializeField] private UIButtonTrigger _btnStartLevel;
	private bool _isHasHandlers;
	public UIWidget bottom;
	private Tween _tweenBottom;
	private Tween _tweenAlpha;
	public StartGameView Show()
	{
		Active = true;
		AddHandler();
		_tweenBottom?.Kill();
		_tweenBottom = bottom.DoPosition(
			new Vector3(0.0f, -Widget.height * 0.5f - bottom.height * 0.5f, 0.0f),
			new Vector3(0.0f, -Widget.height * 0.5f + bottom.height * 0.5f - 20.0f, 0.0f),
			0.5f
		)
			.SetEase(Ease.OutCubic);
		_tweenAlpha?.Kill();
		_tweenAlpha = _widget.DoAlpha(0.0f, 1.0f, 0.5f)
			.SetEase(Ease.OutSine);
		return this;
	}

	public StartGameView Hide()
	{
		RemoveHandler();
		_tweenBottom?.Kill();
		_tweenBottom = bottom.DoPosition(new Vector3(0.0f, -Widget.height * 0.5f - bottom.height * 0.5f, 0.0f), 0.5f)
			.SetEase(Ease.OutCubic);
		_tweenAlpha?.Kill();
		_tweenAlpha = _widget.DoAlpha(0.0f, 0.5f)
			.SetEase(Ease.OutCubic)
			.OnComplete(() => Active = false);
		return this;
	}

	public void AddHandler()
    {
        if (_isHasHandlers)
        {
			return;
        }
		_isHasHandlers = true;
		_btnStartLevel.EventClick += StartGameHandler;
	}

	public void RemoveHandler()
    {
		if (!_isHasHandlers)
		{
			return;
		}
		_isHasHandlers = false;
		_btnStartLevel.EventClick -= StartGameHandler;
	}

	private void StartGameHandler()
    {
		EventStartGame?.Invoke();
	}
}
