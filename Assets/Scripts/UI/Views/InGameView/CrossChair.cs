namespace DatsGames
{
	using UnityEngine;
	
	public class CrossChair : UIElement
	{
		[System.Serializable]
		public struct Item
		{
			public Transform transform;
			public Vector3 defaultPos;
			public Vector3 outPos;
		}

		public AnimationCurve animationCurve = new AnimationCurve();
		public float animationTime;
		public Item[] items = new Item[0];

		private bool _isAnimate;
		private float _time;
	
		#region Unity Calls

		private void Update()
		{
			if (!_isAnimate)
			{
				return;
			}

			float t = Time.time - _time;
			for (int i = 0, n = items.Length; i < n; i++)
			{
				items[i].transform.localPosition = Vector3.Lerp(
					items[i].outPos,
					items[i].defaultPos,
					animationCurve.Evaluate(t / animationTime)
				);
			}

			if (t > animationTime)
			{
				_isAnimate = false;
			}
		}
		
		#endregion
		#region Public Methods
		
		public void ShotAnimation()
		{
			_time = Time.time;
			_isAnimate = true;
			for (int i = 0, n = items.Length; i < n; i++)
			{
				items[i].transform.localPosition = items[i].outPos;
			}
		}
		
		#endregion
	}
}