namespace DatsGames
{
	using UnityEngine;
	using Anthill.Inject;

	public class InGameView : UIElement
	{
	#region Public Variables

		public UIWidget top;
		public CashBar cashBar;
		public ButtonsBar buttonsBar;
		public LevelProgress levelProgress;

	#endregion
	
	#region Getters / Setters
		
		[Inject] public ResizeScreenHandler ResizeScreenHandler { get; set; }
		
	#endregion

	#region Unity Calls
		
		protected override void Awake()
		{
			base.Awake();
			AntInject.Inject<InGameView>(this);
			cashBar.gameObject.SetActive(false);
			levelProgress.gameObject.SetActive(false);

			// Setup the screen resize handler.
			// --------------------------------
			ResizeScreenHandler.OnResizeEvent(UpdateVisual);
			UpdateVisual(new Vector2(ResizeScreenHandler.Width, ResizeScreenHandler.Height), ResizeScreenHandler.Scale);
		}
		
	#endregion

	#region Public Methods
		
		public InGameView Show()
		{
			Active = true;
			return this;
		}

		public InGameView Hide()
		{
			Active = false;
			return this;
		}
		
	#endregion

	#region Event Handlers
	
		private void UpdateVisual(Vector2 aScreenSize, float aScale)
		{
			// bottom.transform.localPosition = new Vector3(0.0f, -_widget.height * 0.5f + bottom.height * 0.5f - 20.0f, 0.0f);
			top.transform.localPosition = new Vector3(0.0f, _widget.height * 0.5f - top.height * 0.5f + 10.0f, 0.0f);
		}
	
	#endregion
	}
}