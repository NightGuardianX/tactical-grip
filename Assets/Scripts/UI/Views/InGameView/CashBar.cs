namespace DatsGames
{
	using UnityEngine;
	using Anthill.Utils;
	using DG.Tweening;
	
	public class CashBar : UIElement
	{
		#region Public Variables

		public UIWidget container;
		public UILabel label;
		public float animateSpeed;

		#endregion
		#region Private Variables

		private float _currentValue;
		private float _displayValue;
		private bool _isAnimate;

		private Tween _tweenPosition;
		private Tween _tweenAlpha;

		#endregion
		#region Getters / Setters
		
		public float Value
		{
			get => _currentValue;
			set
			{
				if (!AntMath.Equal(_currentValue, value))
				{
					_currentValue = value;
					_isAnimate = true;
				}
			}
		}
		
		#endregion
		#region Unity Calls
	
		private void Update()
		{
			if (_isAnimate)
			{
				_displayValue = AntMath.Lerp(_displayValue, _currentValue, animateSpeed * Time.deltaTime);
				if (AntMath.Equal(_displayValue, _currentValue, 0.5f))
				{
					_displayValue = _currentValue;
					_isAnimate = false;
				}

				label.text = _displayValue.ToString("0");
			}
		}
	
		#endregion
		#region Public Methods

		public void SetValue(int aValue)
		{
			_currentValue = aValue;
			_displayValue = aValue;
			label.text = _displayValue.ToString("0");
		}
		
		public void Show(bool aVisible)
		{
			if (aVisible)
			{
				gameObject.SetActive(true);

				_tweenPosition?.Kill();
				_tweenPosition = container.DoPosition(
					new Vector3(0.0f, 60.0f, 0.0f),
					Vector3.zero,
					0.25f
				)
					.SetEase(Ease.OutSine);

				_tweenAlpha?.Kill();
				_tweenAlpha = container.DoAlpha(0.0f, 1.0f, 0.25f)
					.SetEase(Ease.OutSine);
			}
			else
			{
				_tweenPosition?.Kill();
				_tweenPosition = container.DoPosition(
					new Vector3(0.0f, 60.0f, 0.0f),
					0.25f
				)
					.SetEase(Ease.InSine);

				_tweenAlpha?.Kill();
				_tweenAlpha = container.DoAlpha(0.0f, 0.25f)
					.SetEase(Ease.InSine)
					.OnComplete(() => gameObject.SetActive(false));
			}
		}

		#endregion
	}
}