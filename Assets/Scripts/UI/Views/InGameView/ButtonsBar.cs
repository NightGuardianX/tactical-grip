namespace DatsGames
{
	using UnityEngine;
	using DG.Tweening;
	
	public class ButtonsBar : UIElement
	{
		public delegate void ButtonsBarDelegate();
		public event ButtonsBarDelegate EventRestart;

		public UIWidget container;
		public UIButtonTrigger btnRestart;

		private Tween _tweenPosition;
		private Tween _tweenAlpha;
		private bool _isHasHandlers;

		#region Getters / Setters
		
		// ..
		
		#endregion
		#region Unity Calls
	
		// ..
	
		#endregion
		#region Public Methods
		
		public void Show()
		{
			AddHandlers();
			
			this.gameObject.SetActive(true);

			_tweenPosition?.Kill();
			_tweenPosition = container.DoPosition(
				new Vector3(0.0f, 60.0f, 0.0f),
				Vector3.zero,
				0.25f
			)
				.SetEase(Ease.OutSine);

			_tweenAlpha?.Kill();
			_tweenAlpha = container.DoAlpha(0.0f, 1.0f,	0.25f)
				.SetEase(Ease.OutSine);
		}

		public void Hide()
		{
			RemoveHandlers();

			_tweenPosition?.Kill();
			_tweenPosition = container.DoPosition(
				new Vector3(0.0f, 60.0f, 0.0f),
				0.25f
			)
				.SetEase(Ease.InSine);

			_tweenAlpha?.Kill();
			_tweenAlpha = container.DoAlpha(0.0f, 0.25f)
				.SetEase(Ease.InSine)
				.OnComplete(() => this.gameObject.SetActive(false));
		}
		
		#endregion
		#region Private Methods
		
		private void AddHandlers()
		{
			if (_isHasHandlers)
			{
				return;
			}

			_isHasHandlers = true;
			btnRestart.EventClick += RestartTapHandler;
		}

		private void RemoveHandlers()
		{
			if (!_isHasHandlers)
			{
				return;
			}

			_isHasHandlers = false;
			btnRestart.EventClick -= RestartTapHandler;
		}
		
		#endregion
		#region Event Handlers
		
		private void RestartTapHandler()
		{
			EventRestart?.Invoke();
		}
		
		#endregion
	}
}