namespace DatsGames
{
	using UnityEngine;
	using Anthill.Utils;
	using DG.Tweening;
	
	public class LevelProgress : UIElement
	{
		#region Public Variables

		public UIWidget container;
		public UILabel currentLevel;
		public UILabel nextLevel;
		public UISprite fill;
		public UISprite roller;

		[Header("Settings")]
		public float fillOffset = 0.125f;
		public float rollerStartPosition = -198.0f;
		public float rollerEndPosition = 198.0f;

		#endregion
		#region Private Variables

		private float _progress;
		private float _displayProgress;
		private bool _isAnimate;
		private int _currentLevel;
		private int _nextLevel;

		private Tween _tweenPosition;
		private Tween _tweenAlpha;

		#endregion
		#region Getters / Setters

		public int CurrentLevel
		{
			get => _currentLevel;
			set
			{
				_currentLevel = value;
				currentLevel.text = _currentLevel.ToString();
			}
		}

		public int NextLevel
		{
			get => _nextLevel;
			set
			{
				_nextLevel = value;
				nextLevel.text = _nextLevel.ToString();
			}
		}
		
		public float Progress
		{
			get => _progress;
			set
			{
				_progress = value;
				_displayProgress = _progress;
				UpdateVisual();
			}
		}
		
		#endregion
		#region Unity Calls
	
		private void Update()
		{
			if (_isAnimate)
			{
				_displayProgress = AntMath.Lerp(_displayProgress, _progress, 4.0f * Time.deltaTime);
				if (AntMath.Equal(_displayProgress, _progress, 0.01f))
				{
					_displayProgress = _progress;
					_isAnimate = false;
				}
				UpdateVisual();
			}
		}
	
		#endregion
		#region Private Methods
		
		private void UpdateVisual()
		{
			fill.fillAmount = AntMath.Remap(_displayProgress, 0.0f, 1.0f, fillOffset, 1.0f - fillOffset);
			float dist = Mathf.Abs(rollerStartPosition) + Mathf.Abs(rollerEndPosition);
			roller.transform.localPosition = new Vector3(dist * _displayProgress + rollerStartPosition, 0.0f, 0.0f);
		}
		
		#endregion
		#region Public Methods
		
		public void Show(bool aVisible)
		{
			if (aVisible)
			{
				this.gameObject.SetActive(true);
				container.alpha = 0.0f;

				_tweenPosition?.Kill();
				_tweenPosition = container.DoPosition(
					new Vector3(0.0f, 60.0f, 0.0f),
					Vector3.zero,
					0.25f
				)
					.SetEase(Ease.OutSine);

				_tweenAlpha?.Kill();
				_tweenAlpha = container.DoAlpha(1.0f, 0.25f)
					.SetEase(Ease.OutSine);
			}
			else
			{
				_tweenPosition?.Kill();
				_tweenPosition = container.DoPosition(
					new Vector3(0.0f, 60.0f, 0.0f),
					0.25f
				)
					.SetEase(Ease.InSine);

				_tweenAlpha?.Kill();
				_tweenAlpha = container.DoAlpha(0.0f, 0.25f)
					.SetEase(Ease.InSine)
					.OnComplete(() => this.gameObject.SetActive(false));
			}
		}

		public void SetProgress(float aValue)
		{
			_progress = aValue;
			_isAnimate = true;
		}
		
		#endregion
	}
}