namespace DatsGames
{
	using UnityEngine;
	using DG.Tweening;
	
	public class FlyingLabel : UIElement
	{
	#region Public Variables

		public string kind = "Default";
		public float initialLifeTime = 1.0f;
		public float flyingSpeed = 10.0f;
		public string prefix = string.Empty;
		public string postfix = string.Empty;

	#endregion
	
	#region Private Variables

		private UILabel _label;
		private int _value;
		private Tween _tweenAlpha;
		private Tween _tweenScale;

	#endregion

	#region Getters / Setters

		public float LifeTime { get; set; }
		public bool IsVisible { get; private set; }
		public bool IsFree { get; private set; }
		public Vector3 WorldPosition { get; set; }
		
		public int Value
		{
			get => _value;
			set
			{
				_value = value;
				_label.text = $"{prefix}{_value}{postfix}";
			}
		}

		public string Caption
		{
			get => _label.text;
			set => _label.text = value;
		}
		
	#endregion

	#region Unity Calls
	
		protected override void Awake()
		{
			base.Awake();
			_label = GetComponent<UILabel>();
			IsFree = true;
		}
	
	#endregion

	#region Public Methods
		
		public void Show()
		{
			IsVisible = true;
			IsFree = false;
			LifeTime = initialLifeTime;

			_tweenAlpha?.Kill();
			_tweenAlpha = _label.DoAlpha(0.0f, 1.0f, 0.5f)
				.SetEase(Ease.OutCirc);

			_tweenScale?.Kill();
			_tweenScale = _label.DoScale(Vector3.zero, Vector3.one, 0.25f)
				.SetEase(Ease.OutBack);
		}

		public void Hide()
		{
			IsFree = true;
			_tweenAlpha?.Kill();
			_tweenAlpha = _label.DoAlpha(0.0f, 1.0f)
				.SetEase(Ease.InCubic)
				.OnComplete(() =>
				{
					gameObject.SetActive(false);
					IsVisible = false;
				});
		}
		
	#endregion
	}
}