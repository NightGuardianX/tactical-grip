namespace DatsGames
{
	using UnityEngine;
	
	public class FlyingLabelsView : UIElement
	{
		[System.Serializable]
		public struct LabelItem
		{
			public GameObject prefab;
			public string kind;
			public int quantity;
		}

		public LabelItem[] labels = new LabelItem[0];
		public UIPanel labelsContainerRef;

		#region Unity Calls
	
		// ..
	
		#endregion
	}
}