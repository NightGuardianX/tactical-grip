namespace DatsGames
{
	using UnityEngine;
	using DG.Tweening;
	using Anthill.Inject;
	using Anthill.Utils;
	using Anthill.Core;
	
	public class TouchStickView : UIElement
	{
		public delegate void TouchStickViewDelegate();

		/// <summary>
		/// Вызывается когда пользователь косается стика.
		/// </summary>
		public event TouchStickViewDelegate EventTouch;

		/// <summary>
		/// Вызывается когда пользователь отпускает стик.
		/// </summary>
		public event TouchStickViewDelegate EventRelease;

		#region Public Variables
		
		public Transform container;
		public UISprite sprStick;
		public UISprite sprBackground;
		public float maxAlpha = 0.4f;
		public float radius;

		#endregion
		#region Private Variables

		private TouchStickViewDelegate _touchCallback;
		private TouchStickViewDelegate _releaseCallback;

		private Transform _stickTransform;
		private Vector3 _origPosition;
		private Vector3 _stickPos;
		private float _dist;
		private Vector2 _axis;
		private bool _isTouched;

		private bool _isVisible;
		private bool _isEnabled = true;

		private Vector2 _deltaPos;
		private int _currentTouchID;
		private bool _isInit;
		private float _stickOffset = 0.2f;
		
		private Tween _tweenTotalAlpha;
		private Tween _tweenStick;
		private Tween _tweenBackground;
		private Tween _tweenPosition;

		#endregion
		#region Getters / Setters
		
		[Inject] public ResizeScreenHandler ResizeScreenHandler { get; set; }

		public Vector2 StickPosition { get; private set; }

		public float StickOffset
		{
			get => _stickOffset;
			set
			{
				_stickOffset = value;
				ResetJoystickPosition();
			}
		}
		
		/// <summary>
		/// Возвращает текущий вектор направления стика.
		/// </summary>
		public Vector2 Direction
		{
			get => _axis;
		}

		/// <summary>
		/// Возвращает текущий угол стика в градусах.
		/// </summary>
		public float Angle
		{
			get => AntAngle.BetweenDeg(_axis);
		}

		/// <summary>
		/// Возвращает текущую дистанцию оттягивания от центра от 0 до 1.
		/// </summary>
		public float Distance
		{
			get => _dist / radius;
		}

		/// <summary>
		/// Определяет косается ли пользователь стика или нет.
		/// </summary>
		public bool IsTouched
		{
			get => _isTouched;
		}

		/// <summary>
		/// Определяет видимость стика.
		/// </summary>
		public bool IsVisible
		{
			get => _isVisible;
		}
		
		#endregion
		#region Unity Calls
		
		private void Update()
		{
			Initialize();

			if (!_isTouched)
			{
				// Return stick to the center.
				_dist = AntMath.Lerp(_dist, 0.0f, 4.0f * Time.deltaTime);
				float angle = AntAngle.BetweenRad(
					Vector2.zero, 
					new Vector2(_stickTransform.localPosition.x, _stickTransform.localPosition.y)
				);
				
				_stickPos.x = _dist * Mathf.Cos(angle);
				_stickPos.y = _dist * Mathf.Sin(angle);

				_stickTransform.localPosition = _stickPos;
			}
		}
		
		#endregion
		#region Public Methods

		public void OnPress(bool val)
		{
			if (!_isEnabled)
			{
				return;
			}

			if (ResizeScreenHandler == null)
			{
				return;
			}
			
			if (val)
			{
				if (!_isTouched)
				{
					_tweenTotalAlpha?.Kill();
					_tweenPosition?.Kill();

					var pos = Input.mousePosition;
					pos.x = ((pos.x / Screen.width) * ResizeScreenHandler.Width);
					pos.y = ((pos.y / Screen.height) * ResizeScreenHandler.Height);
					pos.x -= ResizeScreenHandler.Width * 0.5f;
					pos.y -= ResizeScreenHandler.Height * 0.5f;
					container.localPosition = pos;

					_currentTouchID = UICamera.currentTouchID;
					_isTouched = true;
					_deltaPos = Vector2.zero;
					
					Over();
					TouchStickHandler();
				}
			}
			else if (_currentTouchID == UICamera.currentTouchID)
			{
				// On realease, reset all the values.
				_isTouched = false;
				ResetJoystickPosition();
				Out();
				ReleasedStickHandler();
			}
		}

		public void OnDrag(Vector2 delta)
		{
			if (!_isEnabled)
			{
				return;
			}
			
			// if the current touch ID
			if (_currentTouchID == UICamera.currentTouchID)
			{
				_deltaPos += delta * (ResizeScreenHandler.Width / Screen.width);
				var touchPos = new Vector3(_deltaPos.x, _deltaPos.y);
				var angle = AntAngle.BetweenRad(touchPos);
				_dist = AntMath.Distance(touchPos);
				_dist = (_dist > radius) ? radius : _dist;
				_axis.x = _dist * Mathf.Cos(angle);
				_axis.y = _dist * Mathf.Sin(angle);
				_stickTransform.localPosition = _axis;
				_axis.x /= radius;
				_axis.y /= radius;
			}
		}

		public void Over()
		{
			if (_isVisible && _isEnabled)
			{
				_tweenTotalAlpha?.Kill();
				_tweenTotalAlpha = _widget.DoAlpha(0.75f, 0.25f)
					.SetEase(Ease.OutCubic);
			}
		}

		public void Out()
		{
			if (_isVisible && _isEnabled)
			{
				_tweenTotalAlpha?.Kill();
				_tweenTotalAlpha = _widget.DoAlpha(0.1f, 0.5f)
					.SetEase(Ease.InCubic);
			}
		}

		/// <summary>
		/// Отобразает контроллер и включает его функционал.
		/// </summary>
		public void Show()
		{
			if (_isVisible)
			{
				return;
			}

			_isVisible = true;
			ResetJoystickPosition();
			
			AntDelayed.Call(0.2f, () =>
			{
				// gameObject.SetActive(true);
				_tweenTotalAlpha?.Kill();
				_tweenTotalAlpha = _widget.DoAlpha(1.0f, 0.1f)
					.SetEase(Ease.OutCubic);
			});
		}

		/// <summary>
		/// Скрывает контроллер и выключает его функционал.
		/// </summary>
		public void Hide()
		{
			if (!_isVisible)
			{
				return;
			}

			_isVisible = false;
			OnPress(false);
			_tweenTotalAlpha?.Kill();
			_tweenTotalAlpha = _widget.DoAlpha(0.0f, 0.1f)
				.SetEase(Ease.InCubic);
				// .OnComplete(() => { gameObject.SetActive(false); });
		}

		/// <summary>
		/// Включает или выключает функционал контроллера.
		/// </summary>
		/// <param name="aEnable">Вкл/выкл</param>
		public void SetEnable(bool aEnable)
		{
			_isEnabled = aEnable;

			if (!_isEnabled)
			{
				Reset();
			}
		}

		/// <summary>
		/// Устанавливает видимость контроллера не влияя на его функционал.
		/// </summary>
		/// <param name="aVisible">Скрыть/отобразить.</param>
		public void SetVisible(bool aVisible)
		{
			if (aVisible)
			{
				_tweenBackground?.Kill();
				_tweenBackground = sprBackground.DoAlpha(maxAlpha, 0.1f);

				_tweenStick?.Kill();
				_tweenStick = sprStick.DoAlpha(maxAlpha, 0.1f);
			}
			else
			{
				_tweenBackground?.Kill();
				_tweenBackground = sprBackground.DoAlpha(0.0f, 0.1f);

				_tweenStick?.Kill();
				_tweenStick = sprStick.DoAlpha(0.0f, 0.1f);
			}
		}

		/// <summary>
		/// Сбрасывает состояние стика до дефолтного, обнуляет оси и углы.
		/// </summary>
		public void Reset()
		{
			if (!_isInit)
			{
				return;
			}
			
			_origPosition = Vector3.zero;
			_stickTransform.localPosition = Vector3.zero;
			StickPosition = Vector2.zero;
			_isTouched = false;

			ResetJoystickPosition();
		}

		/// <summary>
		/// Устанавливает одноразовый обратный вызов на косание стика.
		/// </summary>
		/// <param name="aCallback">Метод для обратного взыва.</param>
		public TouchStickView OnTouch(TouchStickViewDelegate aCallback)
		{
			_touchCallback = aCallback;
			return this;
		}

		/// <summary>
		/// Устаналивает одноразовый обратный вызов на отпускание стика.
		/// </summary>
		/// <param name="aCallback">Метод для обратного взыва.</param>
		public TouchStickView OnRelease(TouchStickViewDelegate aCallback)
		{
			_releaseCallback = aCallback;
			return this;
		}

		#endregion
		#region Private Methods

		private void Initialize()
		{
			if (_isInit)
			{
				return;
			}

			_isInit = true;
			AntInject.Inject<TouchStickView>(this);
			ResizeScreenHandler.OnResizeEvent((aSize, aScale) =>
			{
				if (!_isTouched)
				{
					ResetJoystickPosition();
				}
			});
			
			sprStick.alpha = maxAlpha;
			sprBackground.alpha = maxAlpha;

			_stickTransform = sprStick.transform;
			_stickPos = new Vector3();
			
			Reset();
			ResetJoystickPosition();
		}

		private void ResetJoystickPosition()
		{
			if (!_isInit)
			{
				return;
			}

			var pos = new Vector3(
				(Screen.width * 0.5f / Screen.width) * ResizeScreenHandler.Width,
				ResizeScreenHandler.Height * _stickOffset,
				0.0f
			);

			pos.x -= ResizeScreenHandler.Width * 0.5f;
			pos.y -= ResizeScreenHandler.Height * 0.5f;
			
			_axis = Vector2.zero;

			_tweenPosition?.Kill();
			_tweenPosition = _widget.DoPosition(pos, 0.25f)
				.SetEase(Ease.OutCubic);

			_isTouched = false;
		}
		
		private void UpdateValue(Vector3 aValue)
		{
			var delta = _origPosition - aValue;
			delta.y = -delta.y;
			delta /= radius;
			StickPosition = new Vector2(-delta.x, delta.y);
		}
		
		#endregion
		#region Event Handlers

		private void TouchStickHandler()
		{
			EventTouch?.Invoke();
			_touchCallback?.Invoke();
			_touchCallback = null;
		}

		private void ReleasedStickHandler()
		{
			EventRelease?.Invoke();
			_releaseCallback?.Invoke();
			_releaseCallback = null;
		}
		
		#endregion
	}
}