namespace DatsGames
{
	using UnityEngine;
	
	public class FlyingCoinsView : UIElement
	{
		[System.Serializable]
		public struct CoinItem
		{
			public GameObject prefab;
			public int quantity;
		}

		public CoinItem[] coins = new CoinItem[0];
		public UIPanel coinsContainerRef;

		#region Unity Calls
	
		// ..
	
		#endregion
	}
}