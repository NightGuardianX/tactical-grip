namespace DatsGames
{
	using UnityEngine;
	using Anthill.Utils;
	using DG.Tweening;
	
	public class FlyingCoin : UIElement
	{
		public enum State
		{
			Delay,
			Spawn,
			Wait,
			Fly,
			Finish
		}

		public Vector2 randomScale = new Vector2(0.5f, 1.0f);
		public float spawnDelay = 0.15f;
		public float spawnRandomness = 0.1f;
		public float spawnAngle = 360.0f;
		public Vector2 spawnDistance = new Vector2(5.0f, 20.0f);
		public float spawnDuration = 1.0f;
		public AnimationCurve spawnCurve = new AnimationCurve();
		public float waitTime = 0.25f;
		public float waitTimeRandomness = 0.2f;
		public float flyingDuration = 5.0f;
		public AnimationCurve flyCurve = new AnimationCurve();

		private float _delay;
		private Vector3 _initialPos;
		private Vector3 _targetPlace;
		private Vector3 _waitPlace;
		private float _waitTime;
		private float _spawnTime;
		private float _flyTime;

		#region Getters / Setters
		
		public bool IsFree { get; private set; }
		public State CurrentState { get; set; }
		public State PrevState { get; set; }
		
		#endregion
		#region Unity Calls
	
		protected override void Awake()
		{
			base.Awake();
			Free();
		}
	
		#endregion
		#region Public Methods
		
		public void Spawn(Vector3 aPosition, Vector3 aTarget)
		{
			gameObject.SetActive(true);
			_targetPlace = aTarget;
			_initialPos = aPosition;
			_t.localPosition = aPosition;

			float rnd = AntRandom.Range(randomScale);
			_t.localScale = new Vector3(rnd, rnd, rnd);
			IsFree = false;
			_delay = spawnDelay + AntRandom.Range(-spawnRandomness, spawnRandomness);
			var ang = AntRandom.Range(0.0f, spawnAngle) * AntAngle.DegToRad;

			rnd = AntRandom.Range(spawnDistance);
			_waitPlace = new Vector2(_initialPos.x + rnd * Mathf.Cos(ang), _initialPos.y + rnd * Mathf.Sin(ang));
			_waitTime = waitTime + AntRandom.Range(-waitTimeRandomness, waitTimeRandomness);
			
			CurrentState = State.Delay;
			PrevState = State.Delay;
			_widget.alpha = 0.0f;
			_spawnTime = 0.0f;
			_flyTime = 0.0f;
		}

		public void UpdateLogic(float aDeltaTime)
		{
			switch (CurrentState)
			{
				case State.Delay :
					_delay -= aDeltaTime;
					if (_delay <= 0.0f)
					{
						CurrentState = State.Spawn;
						DOTween.To(() => _widget.alpha, x => _widget.alpha = x, 1.0f, 0.25f)
							.SetEase(Ease.OutSine);
					}
					break;

				case State.Spawn :
					_spawnTime += aDeltaTime;
					_t.localPosition = AntMath.Lerp(_initialPos, _waitPlace, 
						spawnCurve.Evaluate(_spawnTime / spawnDuration)
					);

					if (_spawnTime > spawnDuration)
					{
						CurrentState = State.Wait;
					}
					break;

				case State.Wait :
					_waitTime -= aDeltaTime;
					if (_waitTime <= 0.0f)
					{
						CurrentState = State.Fly;
					}
					break;

				case State.Fly :
					_flyTime += aDeltaTime;
					_t.localPosition = AntMath.Lerp(_waitPlace, _targetPlace, 
						flyCurve.Evaluate(_flyTime / flyingDuration)
					);

					if (_flyTime > flyingDuration)
					{
						CurrentState = State.Finish;
					}
					break;
			}
		}

		public void Free()
		{
			gameObject.SetActive(false);
			IsFree = true;
		}
		
		#endregion
	}
}