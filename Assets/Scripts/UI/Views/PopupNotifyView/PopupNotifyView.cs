namespace DatsGames
{
	using UnityEngine;
	using DG.Tweening;
	
	public class PopupNotifyView : UIElement
	{
		public delegate void PopupNotifyViewDelegate();
		public event PopupNotifyViewDelegate EventComplete;

		public UILabel description;
		public UIWidget container;

		private bool _isAnimate;

		private Tween _tweenAlpha;
		private Tween _tweenPosition;

	#region Getters / Setters
		
		// ..
		
	#endregion

	#region Unity Calls

		// protected override void Awake()
		// {
		// 	base.Awake();
		// }
	
	#endregion

	#region Public Methods
		
		public PopupNotifyView Show()
		{
			if (_isAnimate)
			{
				return this;
			}

			_tweenAlpha?.Kill();
			_tweenAlpha = container.DoAlpha(0.0f, 1.0f, 0.25f)
				.SetEase(Ease.OutSine);

			_tweenPosition?.Kill();
			_tweenPosition = container.DoPosition(
				new Vector3(0.0f, -100.0f, 0.0f),
				new Vector3(0.0f, 0.0f, 0.0f),
				1.5f
			)
				.OnComplete(() =>
				{
					_tweenPosition = container.DoPosition(new Vector3(0.0f, 25.0f, 0.0f), 0.25f)
						.SetEase(Ease.InSine);
					_tweenAlpha = container.DoAlpha(0.0f, 0.25f)
						.OnComplete(() =>
						{
							_isAnimate = false;
							EventComplete?.Invoke();
						});
				});

			_isAnimate = true;
			return this;
		}

		public PopupNotifyView Hide()
		{
			// Nothing here.
			return this;
		}

		public PopupNotifyView SetDescription(string aValue)
		{
			description.text = aValue;
			return this;
		}
		
	#endregion

	#region Private Methods

		// ..
		
	#endregion

	#region Event Handlers

		// ...
		
	#endregion
	}
}