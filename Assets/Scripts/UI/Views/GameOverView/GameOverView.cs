namespace DatsGames
{
	using UnityEngine;
	using DG.Tweening;
	using Anthill.Inject;
	
	public class GameOverView : UIElement
	{
		public delegate void GameOverViewDelegate();
		public event GameOverViewDelegate EventRestart;
		
		public UIWidget top;
		public UIWidget popup;
		public UISprite background;

		[Header("Labels")]
		public UILabel lblLevel;

		[Header("Buttons")]
		public UIButtonTrigger btnRestart;

		private Tween _tweenBgAlpha;
		private Tween _tweenTopPosition;
		private Tween _tweenTopAlpha;
		private Tween _tweenPopupAlpha;
		private Tween _tweenPopupScale;

		private int _levelValue;
		private bool _isHasHandlers;

	#region Getters / Setters
		
		[Inject] public ResizeScreenHandler ResizeScreenHandler { get; set; }

		public int LevelValue
		{
			get => _levelValue;
			set
			{
				_levelValue = value;
				lblLevel.text = $"level {_levelValue}";
			}
		}
		
	#endregion

	#region Unity Calls
	
		protected override void Awake()
		{
			base.Awake();
			AntInject.Inject<GameOverView>(this);

			// Setup the screen resize handler.
			// --------------------------------
			ResizeScreenHandler.OnResizeEvent(UpdateVisual);
			UpdateVisual(new Vector2(ResizeScreenHandler.Width, ResizeScreenHandler.Height), ResizeScreenHandler.Scale);
		}
	
	#endregion
	
	#region Public Methods
		
		public GameOverView Show()
		{
			AddHandlers();
			Active = true;
			
			btnRestart.Alpha = 1.0f;
			btnRestart.gameObject.SetActive(true);
			
			_tweenBgAlpha?.Kill();
			_tweenBgAlpha = background.DoAlpha(0.0f, 0.5f, 0.25f)
				.SetEase(Ease.OutSine);

			_tweenPopupAlpha?.Kill();
			_tweenPopupAlpha = popup.DoAlpha(0.0f, 1.0f, 0.25f)
				.SetDelay(0.5f)
				.SetEase(Ease.OutSine);

			_tweenPopupScale?.Kill();
			_tweenPopupScale = popup.DoScale(
				Vector3.one * 0.5f,
				Vector3.one,
				0.25f
			)
				.SetDelay(0.5f)
				.SetEase(Ease.OutBack);

			_tweenTopAlpha?.Kill();
			_tweenTopAlpha = top.DoAlpha(0.0f, 1.0f, 0.5f)
				.SetDelay(0.25f)
				.SetEase(Ease.OutSine);

			_tweenTopPosition?.Kill();
			_tweenTopPosition = top.DoPosition(
				new Vector3(0.0f, Widget.height * 0.5f + top.height * 0.5f, 0.0f),
				new Vector3(0.0f, Widget.height * 0.5f - top.height * 0.5f + 10.0f, 0.0f),
				0.5f
			)
				.SetEase(Ease.OutCubic);

			return this;
		}

		public GameOverView Hide()
		{
			RemoveHandlers();

			_tweenBgAlpha?.Kill();
			_tweenBgAlpha = background.DoAlpha(0.0f, 0.25f)
				.SetDelay(0.25f)
				.SetEase(Ease.InSine);

			_tweenPopupAlpha?.Kill();
			_tweenPopupAlpha = popup.DoAlpha(0.0f, 0.25f)
				.SetEase(Ease.OutSine);

			_tweenPopupScale?.Kill();
			_tweenPopupScale = popup.DoScale(Vector3.one * 0.5f,	0.25f)
				.SetEase(Ease.InSine);

			_tweenTopAlpha?.Kill();
			_tweenTopAlpha = top.DoAlpha(0.0f, 0.25f)
				.SetDelay(0.25f)
				.SetEase(Ease.InSine);

			_tweenTopPosition?.Kill();
			_tweenTopPosition = top.DoPosition(new Vector3(0.0f, Widget.height * 0.5f + top.height * 0.5f, 0.0f), 0.5f)
				.SetDelay(0.25f)
				.SetEase(Ease.InCubic)
				.OnComplete(() => Active = false);

			return this;
		}

		public void HideRestartButton()
		{
			btnRestart.DoAlpha(0.0f, 0.25f)
				.SetEase(Ease.InSine)
				.OnComplete(() => btnRestart.gameObject.SetActive(false));
		}
		
	#endregion
	
	#region Private Methods
		
		private void AddHandlers()
		{
			if (_isHasHandlers)
			{
				return;
			}

			_isHasHandlers = true;
			btnRestart.EventClick += RestartHandler;
		}

		private void RemoveHandlers()
		{
			if (!_isHasHandlers)
			{
				return;
			}

			_isHasHandlers = false;
			btnRestart.EventClick -= RestartHandler;
		}
		
	#endregion

	#region Event Handlers
		
		private void RestartHandler()
		{
			EventRestart?.Invoke();
		}

		private void UpdateVisual(Vector2 aScreenSize, float aScale)
		{
			// bottom.transform.localPosition = new Vector3(0.0f, -_widget.height * 0.5f + bottom.height * 0.5f - 20.0f, 0.0f);
			top.transform.localPosition = new Vector3(0.0f, _widget.height * 0.5f - top.height * 0.5f + 10.0f, 0.0f);
		}
		
	#endregion
	}
}