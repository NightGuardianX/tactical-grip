namespace DatsGames
{
	using UnityEngine;
	using DG.Tweening;
	using Anthill.Inject;
	using Anthill.Utils;
	
	public class DebriefingView : UIElement
	{
		public delegate void DebriefingViewDelegate();
		public event DebriefingViewDelegate EventClaim;
		
	#region Public Variables

		public UIWidget top;
		public UIWidget popup;
		public UISprite background;
		public ParticleSystem effGlow;
		public UISprite coins;

		[Header("Labels")]
		public UILabel lblReward;
		public UILabel lblLevel;

		[Header("Buttons")]
		public UIButtonTrigger btnClaim;

	#endregion

	#region Private Variables

		private bool _isHasHandlers;
		private Tween _tweenBgAlpha;
		private Tween _tweenTopPosition;
		private Tween _tweenTopAlpha;
		private Tween _tweenPopupAlpha;
		private Tween _tweenPopupScale;

		private float _rewardValue;
		private float _displayRewardValue;
		private bool _isAnimate;
		private int _levelValue;

	#endregion

	#region Getters / Setters

		[Inject] public ResizeScreenHandler ResizeScreenHandler { get; set; }
		
		public int LevelValue
		{
			get => _levelValue;
			set
			{
				_levelValue = value;
				lblLevel.text = $"level {_levelValue}";
			}
		}
		
		public int RewardValue
		{
			get => Mathf.RoundToInt(_rewardValue);
			set
			{
				_rewardValue = value;
				if (_rewardValue < 0.0f)
				{
					_rewardValue = 0.0f;
				}

				_isAnimate = true;
			}
		}
		
	#endregion

	#region Unity Calls
	
		protected override void Awake()
		{
			base.Awake();
			AntInject.Inject<DebriefingView>(this);

			// Setup the screen resize handler.
			// --------------------------------
			ResizeScreenHandler.OnResizeEvent(UpdateVisual);
			UpdateVisual(new Vector2(ResizeScreenHandler.Width, ResizeScreenHandler.Height), ResizeScreenHandler.Scale);
		}

		private void Update()
		{
			if (_isAnimate)
			{
				_displayRewardValue = AntMath.Lerp(_displayRewardValue, _rewardValue, 4.0f * Time.fixedDeltaTime);
				if (AntMath.Equal(_displayRewardValue, _rewardValue, 0.1f))
				{
					_displayRewardValue = _rewardValue;
					_isAnimate = false;
				}

				lblReward.text = _displayRewardValue.ToString("0");
			}
		}
	
	#endregion

	#region Public Methods
		
		public DebriefingView Show()
		{
			AddHandlers();
			Active = true;
			
			btnClaim.Alpha = 1.0f;
			btnClaim.gameObject.SetActive(true);
			
			_tweenBgAlpha?.Kill();
			_tweenBgAlpha = background.DoAlpha(0.0f, 0.5f, 0.25f)
				.SetEase(Ease.OutSine);

			_tweenPopupAlpha?.Kill();
			_tweenPopupAlpha = popup.DoAlpha(0.0f, 1.0f, 0.25f)
				.SetDelay(0.5f)
				.SetEase(Ease.OutSine);

			_tweenPopupScale?.Kill();
			_tweenPopupScale = popup.DoScale(
				Vector3.one * 0.5f,
				Vector3.one,
				0.25f
			)
				.SetDelay(0.5f)
				.SetEase(Ease.OutBack);

			_tweenTopAlpha?.Kill();
			_tweenTopAlpha = top.DoAlpha(0.0f, 1.0f, 0.5f)
				.SetDelay(0.25f)
				.SetEase(Ease.OutSine)
				.OnComplete(() => effGlow.gameObject.SetActive(true));

			_tweenTopPosition?.Kill();
			_tweenTopPosition = top.DoPosition(
				new Vector3(0.0f, Widget.height * 0.5f + top.height * 0.5f, 0.0f),
				new Vector3(0.0f, Widget.height * 0.5f - top.height * 0.5f + 10.0f, 0.0f),
				0.5f
			)
				.SetEase(Ease.OutCubic);

			return this;
		}

		public DebriefingView Hide()
		{
			RemoveHandlers();

			_tweenBgAlpha?.Kill();
			_tweenBgAlpha = background.DoAlpha(0.0f, 0.25f)
				.SetDelay(0.25f)
				.SetEase(Ease.InSine);

			_tweenPopupAlpha?.Kill();
			_tweenPopupAlpha = popup.DoAlpha(0.0f, 0.25f)
				.SetEase(Ease.OutSine);

			_tweenPopupScale?.Kill();
			_tweenPopupScale = popup.DoScale(Vector3.one * 0.5f, 0.25f)
				.SetEase(Ease.InSine);

			_tweenTopAlpha?.Kill();
			effGlow.gameObject.SetActive(false);
			_tweenTopAlpha = top.DoAlpha(0.0f, 0.25f)
				.SetDelay(0.25f)
				.SetEase(Ease.InSine);

			_tweenTopPosition?.Kill();
			_tweenTopPosition = top.DoPosition(
				new Vector3(0.0f, Widget.height * 0.5f + top.height * 0.5f, 0.0f),
				0.5f
			)
				.SetDelay(0.25f)
				.SetEase(Ease.InCubic)
				.OnComplete(() => Active = false);

			return this;
		}

		public void HideClaimButton()
		{
			btnClaim.DoAlpha(0.0f, 0.25f)
				.SetEase(Ease.InSine)
				.OnComplete(() => btnClaim.gameObject.SetActive(false));
		}

		public Vector3 GetCoinsIconPosition()
		{
			var p = coins.transform.parent;
			var pos = coins.transform.localPosition;
			// pos.x += 30.0f;
			while (!System.Object.ReferenceEquals(p.transform, this.transform))
			{
				pos += p.transform.localPosition;
				p = p.transform.parent;
			}
			return pos + p.transform.localPosition;
		}
		
	#endregion

	#region Private Methods
		
		private void AddHandlers()
		{
			if (_isHasHandlers)
			{
				return;
			}

			_isHasHandlers = true;
			btnClaim.EventClick += ClaimHandler;
		}

		private void RemoveHandlers()
		{
			if (!_isHasHandlers)
			{
				return;
			}

			_isHasHandlers = false;
			btnClaim.EventClick -= ClaimHandler;
		}
		
	#endregion

	#region Event Handlers
		
		private void ClaimHandler()
		{
			EventClaim?.Invoke();
		}

		private void UpdateVisual(Vector2 aScreenSize, float aScale)
		{
			// bottom.transform.localPosition = new Vector3(0.0f, -_widget.height * 0.5f + bottom.height * 0.5f - 20.0f, 0.0f);
			top.transform.localPosition = new Vector3(0.0f, _widget.height * 0.5f - top.height * 0.5f + 10.0f, 0.0f);
		}
		
	#endregion
	}
}