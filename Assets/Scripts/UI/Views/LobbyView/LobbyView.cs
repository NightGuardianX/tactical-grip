namespace DatsGames
{
	using UnityEngine;
	using Anthill.Inject;
	using DG.Tweening;

	public class LobbyView : UIElement
	{
		public delegate void LobbyViewDelegate();
		public LobbyViewDelegate EventStartGame;

		public UIWidget top;
		public UIWidget bottom;
		public UILabel levelValue;

		private int _currentLevel;

		private Tween _tweenTop;
		private Tween _tweenBottom;
		private Tween _tweenAlpha;

		private bool _isEnableInput;

	#region Getters / Setters

		[Inject] public ResizeScreenHandler ResizeScreenHandler { get; set; }
		
		public int CurrentLevel
		{
			get => _currentLevel;
			set
			{
				_currentLevel = value;
				levelValue.text = $"Level {value}";
			}
		}
		
	#endregion
	
	#region Unity Calls
		
		protected override void Awake()
		{
			base.Awake();
			AntInject.Inject<LobbyView>(this);

			// Setup the screen resize handler.
			// --------------------------------
			ResizeScreenHandler.OnResizeEvent(UpdateVisual);
			UpdateVisual(new Vector2(ResizeScreenHandler.Width, ResizeScreenHandler.Height), ResizeScreenHandler.Scale);
		}

        private void Update()
        {
            if (!_isEnableInput)
            {
                return;
            }

            if (Application.isEditor)
            {
                if (Input.GetMouseButtonDown(0))
                {
                    EventStartGame?.Invoke();
                }
            }

            Touch touch;
            for (int i = 0, n = Input.touches.Length; i < n; i++)
            {
                touch = Input.touches[i];
                if (touch.phase == TouchPhase.Began)
                {
                    EventStartGame?.Invoke();
                }
            }
        }

        #endregion

        #region Public Methods

        public LobbyView Show()
		{
			 _isEnableInput = true;
			Active = true;

			_tweenBottom?.Kill();
			_tweenBottom = bottom.DoPosition(
				new Vector3(0.0f, -Widget.height * 0.5f - bottom.height * 0.5f, 0.0f),
				new Vector3(0.0f, -Widget.height * 0.5f + bottom.height * 0.5f - 20.0f, 0.0f),
				0.5f
			)
				.SetEase(Ease.OutCubic);
			
			_tweenTop?.Kill();
			_tweenTop = top.DoPosition(
				new Vector3(0.0f, Widget.height * 0.5f + top.height * 0.5f, 0.0f),
				new Vector3(0.0f, Widget.height * 0.5f - top.height * 0.5f + 10.0f, 0.0f),
				0.5f
			)
				.SetEase(Ease.OutCubic);

			_tweenAlpha?.Kill();
			_tweenAlpha = _widget.DoAlpha(0.0f, 1.0f, 0.5f)
				.SetEase(Ease.OutSine);

			return this;
		}

		public LobbyView Hide()
		{
			_isEnableInput = false;
			
			_tweenBottom?.Kill();
			_tweenBottom = bottom.DoPosition(new Vector3(0.0f, -Widget.height * 0.5f - bottom.height * 0.5f, 0.0f), 0.5f)
				.SetEase(Ease.OutCubic);
			
			_tweenTop?.Kill();
			_tweenTop = top.DoPosition(new Vector3(0.0f, Widget.height * 0.5f + top.height * 0.5f, 0.0f), 0.5f)
				.SetEase(Ease.OutCubic);

			_tweenAlpha?.Kill();
			_tweenAlpha = _widget.DoAlpha(0.0f, 0.5f)
				.SetEase(Ease.OutCubic)
				.OnComplete(() => Active = false);

			return this;
		}
		
	#endregion	

	#region Event Handlers
	
		private void UpdateVisual(Vector2 aScreenSize, float aScale)
		{
			bottom.transform.localPosition = new Vector3(0.0f, -_widget.height * 0.5f + bottom.height * 0.5f - 20.0f, 0.0f);
			top.transform.localPosition = new Vector3(0.0f, _widget.height * 0.5f - top.height * 0.5f + 10.0f, 0.0f);
		}
	
	#endregion	
	}
}