namespace DatsGames
{
	using UnityEngine;
	
	public class TutorialDragToMove : UIElement
	{
		#region Public Variables

		public Transform handPivot;
		public float maxVelocity = 12.0f;
		public float maxForce = 12.0f;
		public float maxSpeed = 12.0f;
		public float arriveDistance = 50.0f;
		public float mass = 2.0f;
		public Transform[] wayPoints = new Transform[0];

		#endregion
		#region Private Variables

		private int _index;
		private Vector3 _currentPoint = Vector3.zero;
		private Vector3 _desiredVelocity;
		private Vector3 _steering;
		private Vector3 _velocity;
		
		#endregion
		#region Unity Calls
	
		protected override void Awake()
		{
			base.Awake();

			_currentPoint = wayPoints[0].localPosition;
			_index = 0;
		}

		private void Update()
		{
			_desiredVelocity = Vector3.Normalize(_currentPoint - handPivot.localPosition) * maxVelocity;
			_steering = _desiredVelocity - _velocity;

			_steering = Truncate(_steering, maxForce);
			_steering /= mass;

			_velocity = Truncate(_velocity + _steering * Time.deltaTime, maxSpeed);
			handPivot.localPosition += _velocity * Time.deltaTime;

			if (Vector3.Distance(handPivot.localPosition, _currentPoint) <= arriveDistance)
			{
				_index++;
				if (_index >= wayPoints.Length)
				{
					_index = 0;
				}

				_currentPoint = wayPoints[_index].localPosition;
			}
		}
	
		#endregion
		#region Private Methods
		
		private Vector3 Truncate(Vector3 aVector, float aMaxValue)
		{
			aVector.x = (aVector.x > aMaxValue) ? aMaxValue : aVector.x;
			aVector.y = (aVector.y > aMaxValue) ? aMaxValue : aVector.y;
			aVector.z = (aVector.z > aMaxValue) ? aMaxValue : aVector.z;
			return aVector;
		}
		
		#endregion
	}
}