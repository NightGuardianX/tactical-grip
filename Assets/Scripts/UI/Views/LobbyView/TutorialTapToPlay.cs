using System;

namespace DatsGames
{
	using UnityEngine;
	using DG.Tweening;
	using Anthill.Tasks;
	using Anthill.Utils;

	public class TutorialTapToPlay : UIElement
	{
		[Header("Hand Animation")]
		public Transform handPivot;
		public float rotationDuration;
		public Vector3 fromRotation;
		public Vector3 toRotation;
		public AnimationCurve rotationCurve = new AnimationCurve();

		[Header("Wave Animation")]
		public UISprite handWave;
		public float waveDuration;
		public Vector3 fromScale;
		public Vector3 toScale;
		public AnimationCurve waveCurve = new AnimationCurve();

		#region Private Variables

		// private AntTaskManager _tm;
		private Tween _tweenWaveScale;
		private Tween _tweenWaveAlpha;

		private float _time;

		#endregion
		#region Getters / Setters
		
		// ..
		
		#endregion
		#region Unity Calls

		// private void Update()
		// {
		// 	_tm?.Execute();
		// 	_tmAttention?.Execute();
		// }

		private void Start()
		{
			// Tutorial animation.
			AntTaskManager.Do(true)
				.Task(DoRotateHand)
				.Task(DoResetRotateAndScale)
				.Delay(0.5f);
		}

		#endregion
		#region Private Methods

		private bool DoRotateHand()
		{
			_time += Time.deltaTime;
			handPivot.transform.localRotation = Quaternion.LerpUnclamped(
				Quaternion.Euler(fromRotation),
				Quaternion.Euler(toRotation), 
				rotationCurve.Evaluate(_time / rotationDuration)
			);

			if (_time >= rotationDuration)
			{
				_time = 0.0f;
				return true;
			}
			return false;
		}

		private bool DoResetRotateAndScale()
		{
			_time += Time.deltaTime;
			var d = rotationCurve.Evaluate(_time / rotationDuration);
			handPivot.transform.localRotation = Quaternion.LerpUnclamped(
				Quaternion.Euler(toRotation),
				Quaternion.Euler(fromRotation),
				d
			);
			
			handWave.transform.localScale = Vector3.LerpUnclamped(fromScale, toScale, d);
			handWave.alpha = AntMath.Lerp(1.0f, 0.0f, d);

			if (_time >= rotationDuration)
			{
				_time = 0.0f;
				return true;
			}
			
			return false;
		}
		
		#endregion
	}
}