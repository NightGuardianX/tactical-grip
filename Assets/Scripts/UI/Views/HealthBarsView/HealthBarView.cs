namespace DatsGames
{
	using UnityEngine;
	using System.Collections.Generic;
	using DG.Tweening;
	using Anthill.Utils;

	public class HealthBarView : UIElement
	{
		public GameObject healthBarItemPrefab;
		public Transform container;

		private List<HealthBarItem> _healthBars = new List<HealthBarItem>();
		private Tween _tweenAlpha;

	#region Unity Calls

		protected override void Awake()
		{
			base.Awake();
		}

	#endregion

	#region Public Methods

		public void Show()
		{
			Active = true;

			_tweenAlpha?.Kill();
			_tweenAlpha = _widget.DoAlpha(0.0f, 1.0f, 0.25f);
		}

		public void Hide()
		{
			_tweenAlpha?.Kill();
			_tweenAlpha = _widget.DoAlpha(0.0f, 0.25f)
				.OnComplete(() => Active = false);
		}

		public int RequestBar(HealthBarKind aKind)
		{
			for (int i = 0, n = _healthBars.Count; i < n; i++)
			{
				if (!_healthBars[i].Active)
				{
					_healthBars[i].Active = true;
					_healthBars[i].SetKind(aKind);
					return i;
				}
			}

			int index = MakeBar();
			_healthBars[index].SetKind(aKind);
			return index;
		}

		public void UpdateBar(int aBarIndex, float aValue, Vector3 aWorldPosition)
		{
			if (aBarIndex >= 0 && aBarIndex < _healthBars.Count)
			{
				var bar = _healthBars[aBarIndex];
				bar.Position = aWorldPosition;

				if (!AntMath.Equals(bar.Value, aValue))
				{
					// Обновляем значение только если значения разные
					// чтобы каждый раз не стартовать анимацию.
					bar.SetValue(aValue);
				}
			}
		}

		public void SetPosition(int aBarIndex, Vector3 aPosition)
		{
			if (aBarIndex >= 0 && aBarIndex < _healthBars.Count)
			{
				_healthBars[aBarIndex].transform.localPosition = aPosition;
			}
		}

		public void SetValue(int aBarIndex, float aValue)
		{
			if (aBarIndex >= 0 && aBarIndex < _healthBars.Count)
			{
				var bar = _healthBars[aBarIndex];
				if (!AntMath.Equals(bar.Value, aValue))
				{
					// Обновляем значение только если значения разные
					// чтобы каждый раз не стартовать анимацию.
					bar.SetValue(aValue);
				}
			}
		}

		public void SetMaxValue(int aBarIndex, float aValue)
		{
			if (aBarIndex >= 0 && aBarIndex < _healthBars.Count)
			{
				_healthBars[aBarIndex].MaxValue = aValue;
			}
		}

		public void ReleaseBar(int aIndex)
		{
			if (aIndex >= 0 && aIndex < _healthBars.Count && _healthBars[aIndex] != null)
			{
				_healthBars[aIndex].Active = false;
			}
		}

	#endregion

	#region Private Methods
		
		private int MakeBar()
		{
			var go = Instantiate(healthBarItemPrefab);
			go.transform.SetParent(container, false);
			var hb = go.GetComponent<HealthBarItem>();
			_healthBars.Add(hb);
			return _healthBars.Count - 1;
		}

	#endregion
	}
}