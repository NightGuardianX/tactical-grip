namespace DatsGames
{
	using UnityEngine;
	using Anthill.Utils;
	
	public class HealthBarItem : UIElement
	{
		[System.Serializable]
		public struct VisualItem
		{
			public HealthBarKind kind;
			public string spriteName;
		}

		public int offset = 16;
		public float width = 168.0f;

		public UISprite background;
		public UISprite fill;
		public UISprite previousFill;

		public float fillDuration = 0.25f;
		public AnimationCurve fillCurve = new AnimationCurve();

		[Header("Blinker Colors")]
		public float activateBlinkPercent = 0.2f;
		public float blinkInterval = 0.25f;
		public Color defaultColor;
		public Color[] blinkColors = new Color[0];

		[Header("Visual of Bars")]
		public VisualItem[] visuals = new VisualItem[0];

	#region Private Variables

		private HealthBarKind _kind;
		private float _value = 1.0f;
		private float _visualValue = 1.0f;
		private float _prevValue = 1.0f;
		private float _maxValue = 1.0f;
		private float _time = 0.0f;
		private bool _isAnimate;

		private float _blinkInterval;
		private int _blinkIndex;
		private bool _isBlink;

	#endregion

	#region Getters / Setters

		public Vector3 Position
		{
			get => _t.localPosition;
			set => _t.localPosition = value;
		}
		
		public float MaxValue
		{
			get => _maxValue;
			set
			{
				_maxValue = value;
				UpdateVisual();
			}
		}

		public float Value
		{
			get => _value;
			set
			{
				_value = value;
				_visualValue = value;
				_isBlink = (_kind == HealthBarKind.Player && _value / MaxValue < activateBlinkPercent);
				UpdateVisual();
			}
		}
		
	#endregion

	#region Unity Calls
		
		private void Update()
		{
			if (_isAnimate)
			{
				_time += Time.deltaTime;
				_visualValue = AntMath.Lerp(_prevValue, _value, fillCurve.Evaluate(_time / fillDuration));
				if (_time >= fillDuration)
				{
					_isAnimate = false;
				}

				UpdateVisual();
			}

			if (_isBlink)
			{
				_blinkInterval += Time.deltaTime;
				if (_blinkInterval > blinkInterval)
				{
					_blinkIndex++;
					_blinkIndex = (_blinkIndex >= blinkColors.Length) ? 0 : _blinkIndex;
					background.color = blinkColors[_blinkIndex];
					_blinkInterval = 0.0f;
				}
			}
		}
		
	#endregion

	#region Public Methods

		public void SetKind(HealthBarKind aKind)
		{
			_kind = aKind;
			int index = System.Array.FindIndex(visuals, x => x.kind == aKind);
			if (index >= 0 && index < visuals.Length)
			{
				background.color = defaultColor;
				fill.spriteName = visuals[index].spriteName;
			}
		}
		
		public void SetValue(float aValue)
		{
			_isBlink = (_kind == HealthBarKind.Player && aValue / MaxValue < activateBlinkPercent);
			_prevValue = _value;
			_value = aValue;
			_isAnimate = true;
			_time = 0.0f;
			UpdateVisual();
		}
		
	#endregion

	#region Private Methods
		
		private void UpdateVisual()
		{
			fill.width = offset + Mathf.RoundToInt(width * (_value / _maxValue));
			previousFill.width = offset + Mathf.RoundToInt(width * (_visualValue / _maxValue));

			if (!_isBlink)
			{
				background.color = defaultColor;
			}
		}
		
	#endregion
	}
}