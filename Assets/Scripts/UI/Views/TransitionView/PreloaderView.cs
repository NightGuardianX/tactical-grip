namespace DatsGames
{
	using UnityEngine;
	using DG.Tweening;
	
	public class PreloaderView : UIElement
	{
		private struct LoaderItem
		{
			public UISprite sprite;
			public float time;
		}

		#region Public Variables

		[Tooltip("Prefab of the dot for spawning.")]
		public GameObject dotPrefab;

		[Tooltip("Radius of the dot's spawning.")]
		public float radius = 100;

		[Tooltip("Count of the dots.")]
		public int count = 8;

		public float fadeTime = 0.25f;
		public float fadeSpeed = 0.25f;

		#endregion
		#region Private Variables

		private LoaderItem[] _items;
		private int _nextIndex = 0;
		private float _interval;
		private bool _isInitialized;

		private Tween _tweenAlpha;
		
		#endregion
		#region Unity Calls
	
		private void Start()
		{
			Initialize();
		}

		private void Update()
		{
			for (int i = 0, n = _items.Length; i < n; i++)
			{
				_items[i].sprite.alpha = _items[i].time / fadeTime + 0.1f;
				_items[i].time -= fadeSpeed * Time.deltaTime;
				if (_items[i].time <= 0.0f)
				{
					_items[i].time = 0.0f;
				}
			}

			_interval -= Time.deltaTime;
			if (_interval < 0.0f)
			{
				_interval = fadeTime * 0.5f;
				_nextIndex--;
				_nextIndex = (_nextIndex < 0) ? _items.Length - 1 : _nextIndex;
				_items[_nextIndex].time = fadeTime;
			}
		}
	
		#endregion
		#region Public Methods
		
		public void Show(bool aAnimate = true)
		{
			gameObject.SetActive(true);
			if (aAnimate)
			{
				_tweenAlpha?.Kill();
				_tweenAlpha = Widget.DoAlpha(0.0f, 1.0f, 0.25f)
					.SetEase(Ease.OutSine);
			}
			else
			{
				Alpha = 1.0f;
			}
		}
		
		public void Hide(bool aAnimate = true)
		{
			if (aAnimate)
			{
				_tweenAlpha?.Kill();
				_tweenAlpha = Widget.DoAlpha(0.0f, 0.25f)
					.SetEase(Ease.InSine)
					.OnComplete(() => gameObject.SetActive(false));
			}
			else
			{
				gameObject.SetActive(false);
			}
		}

		#endregion
		#region Private Methods
		
		private void Initialize()
		{
			if (_isInitialized)
			{
				return;
			}

			_isInitialized = true;
			float step = 360.0f / count;
			float current = 0.0f;
			_nextIndex = 0;
			_interval = fadeTime * 0.5f;

			_items = new LoaderItem[count];
			for (int i = 0; i < count; i++)
			{
				var go = Instantiate(dotPrefab);
				go.transform.SetParent(_t, false);
				go.transform.localRotation = Quaternion.Euler(0.0f, 0.0f, current);
				go.transform.localPosition = new Vector3(
					radius * Mathf.Cos(current * Mathf.Deg2Rad), 
					radius * Mathf.Sin(current * Mathf.Deg2Rad), 
					0.0f
				);

				var sprite = go.GetComponent<UISprite>();
				sprite.alpha = fadeTime / (i + 1);
				_items[i] = new LoaderItem {
					sprite = sprite,
					time = sprite.alpha
				};
				current += step;
			}
		}
		
		#endregion
	}
}