namespace DatsGames
{
	using DG.Tweening;

	public class TransitionView : UIElement
	{
		public delegate void TransitionDelegate();
		public event TransitionDelegate EventShowComplete;
		public event TransitionDelegate EventHideComplete;

		#region Public Methods
		
		public PreloaderView preloader;
		
		#endregion
		#region Private Methods

		private TransitionDelegate _showFinishCallback;
		private TransitionDelegate _hideFinishCallback;

		private Tween _tweenAlpha;

		#endregion
		#region Unity Calls
		
		// ...
		
		#endregion
		#region Public Methods
		
		public TransitionView Show(float aDelay = 0.0f)
		{
			Active = true;

			_tweenAlpha?.Kill();
			_tweenAlpha = _widget.DoAlpha(0.0f, 1.0f, 0.25f)
				.SetDelay(aDelay)
				.SetEase(Ease.OutCubic)
				.OnComplete(() => 
				{
					EventShowComplete?.Invoke();
					_showFinishCallback?.Invoke();
					_showFinishCallback = null;
				});
			
			return this;
		}

		public TransitionView Hide()
		{
			_tweenAlpha?.Kill();
			_tweenAlpha = _widget.DoAlpha(0.0f, 0.25f)
				.SetEase(Ease.OutCubic)
				.OnComplete(() =>
				{
					Active = false;
					EventHideComplete?.Invoke();
					_hideFinishCallback?.Invoke();
					_hideFinishCallback = null;
				});
			
			return this;
		}

		public TransitionView OnShowComplete(TransitionDelegate aCallback)
		{
			_showFinishCallback = aCallback;
			return this;
		}

		public TransitionView OnHideComplete(TransitionDelegate aCallback)
		{
			_hideFinishCallback = aCallback;
			return this;
		}
		
		#endregion
	}
}