namespace DatsGames
{
	using UnityEngine;
	
	/// <summary>
	/// Данный класс отслеживает изменение размеров экрана и отправляет сообщение если
	/// произошло изменение разрешения экрана.
	/// 
	/// Используется для корректного выравнивания игровых интерфейсов при инициализации
	/// игры на устройствах с разными разрешениями экрана.
	/// </summary>
	public class ResizeScreenHandler : MonoBehaviour
	{
		public delegate void ResizeScreenHandlerDelegate(Vector2 aScreenSize, float aScale);
		public event ResizeScreenHandlerDelegate EventResized;

	#region Public Variables

		public int defaultScreenWidth = 828;
		public int defaultScreenHeight = 1792;

	#endregion

	#region Private Variables
		
		private Transform _t;
		private UIRoot _uiRoot;
		private int _prevWidth;
		private int _prevHeight;
		private float _width;
		private float _height;
		private float _scale;

	#endregion

	#region Getters / Setters
		
		public float Width { get => _width; }
		public float Height { get => _height; }
		public float Scale { get => _scale; }
		
	#endregion

	#region Unity Calls
	
		private void Awake()
		{
			_uiRoot = GameObject.FindObjectOfType<UIRoot>();
			A.Assert(_uiRoot == null, "Can't find UIRoot on the scene!");
			_t = GetComponent<Transform>();

			_width = defaultScreenWidth;
			_height = defaultScreenHeight;
			_prevWidth = -1;
			_prevHeight = -1;
		}
	
		private void LateUpdate()
		{
			if (_prevWidth != Screen.width || _prevHeight != Screen.height)
			{
				float ratio = (float) _uiRoot.activeHeight / Screen.height;
				_width = Mathf.Ceil(Screen.width * ratio);
				_height = Mathf.Ceil(Screen.height * ratio);
				_scale = _width / defaultScreenWidth;

				EventResized?.Invoke(new Vector2(_width, _height), _scale);

				_prevWidth = Screen.width;
				_prevHeight = Screen.height;
			}
		}
	
	#endregion

	#region Public Methods
		
		public void OnResizeEvent(ResizeScreenHandlerDelegate aCallback)
		{
			EventResized += aCallback;
		}
		
	#endregion
	}
}