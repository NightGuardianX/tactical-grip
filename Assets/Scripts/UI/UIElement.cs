namespace DatsGames
{
	using UnityEngine;

	public class UIElement : MonoBehaviour
	{
		protected UIWidget _widget;
		protected Transform _t;
		protected bool _active = true;

		private bool _isInitialized;

	#region Getters / Setters
		
		public UIWidget Widget
		{
			get
			{
				Initialize();
				return _widget;
			}
		}

		public float Alpha
		{
			get
			{
				Initialize();
				return _widget.alpha;
			}
			set
			{
				Initialize();
				_widget.alpha = value;
			}
		}

		public virtual bool Active
		{
			get => _active;
			set
			{
				_active = value;
				gameObject.SetActive(value);
			}
		}

	#endregion

	#region Unity Calls
		
		protected virtual void Awake()
		{
			Initialize();
		}
		
	#endregion

	#region Public Methods

		public void SetAnchor(GameObject aObject, Vector2 aPosition, 
			float aLeft = 0.0f, float aTop = 0.0f, float aRight = 0.0f, float aBottom = 0.0f)
		{
			var topLeft = new Vector2(aPosition.x - _widget.width * 0.5f, aPosition.y - _widget.height * 0.5f);
			var bottomRight = new Vector2(aPosition.x + _widget.width * 0.5f, aPosition.y + _widget.height * 0.5f);
			_widget.SetAnchor(aObject, 
				aLeft, Mathf.RoundToInt(topLeft.x),      // Left
				aBottom, Mathf.RoundToInt(topLeft.y),    // Bottom
				aRight, Mathf.RoundToInt(bottomRight.x), // Right
				aTop, Mathf.RoundToInt(bottomRight.y)    // Top
			);

			// TopLeft -> 0.0, 1.0, 0.0, 1.0
		}

		public void SetAnchor(GameObject aObject, float aLeft = -10.0f, float aTop = 10.0f, 
			float aRight = 10.0f, float aBottom = -10.0f)
		{
			_widget.SetAnchor(aObject, 
				0.0f, Mathf.RoundToInt(aLeft),
				0.0f, Mathf.RoundToInt(aBottom),
				1.0f, Mathf.RoundToInt(aRight),
				1.0f, Mathf.RoundToInt(aTop)
			);
		}
		
	#endregion

	#region Private Methods
		
		private void Initialize()
		{
			if (_isInitialized)
			{
				return;
			}

			_isInitialized = true;
			_t = GetComponent<Transform>();
			_widget = GetComponent<UIWidget>();
		}
		
	#endregion
	}
}